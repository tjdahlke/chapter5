#!/usr/bin/env python
import subprocess
import commands
import re
import os
import sys

y=re.compile(":(\d+)->22/tcp")
z=commands.getoutput("docker ps")
m=y.search(z)



cwd=os.getcwd()
base="dockerhomeDIR/"
basedir="%s/%s"%(cwd,base)

if not os.path.isdir(basedir):
  print commands.getoutput("mkdir %s"%basedir)

arg="--volume=%s:/home/user/project"%(basedir)
versionname="tjdahlke/shaping-decon-docker:static"
print arg


# Check to see if the image has already been built
qq = z.split()
alreadybuilt=False
if (versionname in qq):
    alreadybuilt=True
    print("It looks like the docker build you are trying to make already exists. Check that you have the right versionname set")


if (alreadybuilt):
    sshcmd1 = "ssh -Y -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p %s user@localhost" % (m.group(1))
    print("================================================================\n %s" % sshcmd1)
    print "Password = newpass"
    subprocess.call(sshcmd1,shell=True)
else:
    build = "docker build -t %s ." % (versionname)
    print("================================================================\n %s" % build)
    a=subprocess.call(build,shell=True)
    run = "docker run -d -p 22 %s -it %s" % (arg, versionname)
    print(run)
    # run = "docker run -d -p 22 -it %s" % (versionname)
    x=subprocess.call(run,shell=True)
    print("================================================================\n %s" % run)

    z=commands.getoutput("docker ps")
    m=y.search(z)
    if m:
        sshcmd2 = "ssh -Y -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  -p %s user@localhost" % (m.group(1))
        print("================================================================\n %s" % sshcmd2)
        print "Password = newpass"
        subprocess.call(sshcmd2,shell=True)
    else:
        print "trouble starting docker. If if doesn't show up when you type 'docker ps', then it wasn't built correctly."

