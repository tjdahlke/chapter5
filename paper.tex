
\chapter{Application to 3D Gulf of Mexico field data}
\label{chap:chap5}

\par In order to demonstrate the theory and algorithms introduced in previous chapters on a convincing example, I apply level set inversion to a 3D Gulf of Mexico ocean bottom node (OBN) data set provided by Shell. I begin this chapter by describing the dataset and its context, and explore the RTM imagery provided to me for clues related to the actual earth model. Next, I demonstrate how I identify a subset of nodes to use for my inversion (based on my own RTM images) to improve computation turnaround. After, I discuss the necessity of matching amplitudes with the observed data, and discuss how I modify the objective function by weighting the residuals and incorporate a kinematic virtual source operator into the gradient calculation. Following, I discuss the inversion workflow used and the resulting inverted model. To conclude, I show the improvements in the migrated image that was created using the new (inverted) velocity model.

\section{Data set overview}

\par The 3D dataset that I use was acquired in the Gulf of Mexico and provided by Shell Exploration Inc. using Fairfield Z3000 ocean-bottom nodes (Figure \ref{fig:Z3000-node}). The data was collected in 2010 to improve imaging around the salt by leveraging wide-azimuth acquisition with ocean bottom node technology. I was provided with the pressure, horizontal, and vertical components of the data. The field sits in the Garden Banks region about 362km south-west of New Orleans, Louisiana (Figure \ref{fig:overview-map}) in approximately 830 m of water, with an airgun shooting footprint covering an area of about 48x48km. The reservoir itself sits beneath thick layers of salt more than 6 km below the sea floor, and production began on the field in 2014. In the region near the salt diapir, the node footprint coverage is just enough to cover the salt (see Figure \ref{fig:salt-acquisition-mapT}). The area of production is centered around a salt protrusion that nearly reaches the water bottom (Figure \ref{fig:cardamom-salt-diagram}).

% Image of node used in survey
\sideplot{Z3000-node}{width=3in}{Fairfield Z3000 ocean bottom node used to record data. \NR}

% Overview map of project location in GOM
\plot{overview-map}{width=5in}{Map showing approximate project location in the Garden Banks region of the Gulf of Mexico. \NR}

% 3D Shell salt diagram
\plot{cardamom-salt-diagram}{width=5in}{Oblique view of the salt body that is most prominent in the dataset I investigate. \NR}
% https://www.offshore-technology.com/wp-content/uploads/sites/6/2017/09/2-diagram.jpg

% Overview map of entire acquisition
\plot{full-acquisition-map}{width=5in}{Map showing the boundary of the extent of the airgun shooting (green line) and the OBN positions (black points).\NR}

\plot{salt-acquisition-mapT}{width=5in}{Map showing acquisition geometry in the greater region surrounding the zone where inversion and imaging were performed (depth slice at 1,800m). Red is the salt body with approximate inclusion location, black dots are node positions, and grey dots are shot positions. One easily can see where the boats divert around a production platform at 211,700m inline position. \NR}


% Figure of area of interest overlaid on the velocity model

\par Along with the data, Shell provided a Reverse Time Migration (RTM) image cube produced from the downgoing pressure data. The approach they used is called mirror imaging, since all downgoing data recorded on the OBN units (besides the direct arrival) is a result of the mirror-like reflection of subsurface events off the ocean surface \citep{grion2007mirror}. When looking at this mirror-image, one can see an inclusion in the salt at about 1,700m deep, with an inline position of 214,800m and crossline position of 49,600m (see Figures \ref{fig:original-shell-rtm-FRONT}, \ref{fig:original-shell-rtm-SIDE}, \ref{fig:original-shell-rtm-TOP}). When comparing against the velocity model provided (Figures \ref{fig:migvel-top}, \ref{fig:migvel-side}), one can see that the inclusion observed was not included in the velocity model. I intend to show that my level set FWI inversion workflow can help recover a velocity model that takes this inclusion into account, and correspondingly produces a more accurate RTM image.

% Region of interest
\plot{original-shell-rtm-FRONT}{width=5in}{Original mirror image RTM provided by Shell; front view. \NR}
\plot{original-shell-rtm-SIDE}{width=5in}{Original mirror image RTM provided by Shell; side view. \NR}
\plot{original-shell-rtm-TOP}{width=5in}{Original mirror image RTM provided by Shell; top view. \NR}

% VEL MODEL USED FOR MIGRATION
\multiplot{2}{migvel-top,migvel-side}{width=2.8in}{Velocity model used for migration with 10m grid spacing. Figure (b) at 1,700m deep; Figure (a) at inline position 214,800m.}


\section{Optimizing inclusion illumination}
\par Because of limited computational resources, I am interested in minimizing the number of wavefield propagations needed to perform inversion. By taking advantage of reciprocity between the nodes and sources for the hydrophone component, one can change the modeling to use node positions for the sources, and shot positions at the ocean surface as recording locations \citep{knopoff1959seismic}. While this does assume the signature of shots in the field experiment are consistent with each other, I find that this assumption is valid, and exploit the advantages of reciprocity in this dataset.

\par While I limit the propagation domain to a $\sim$7.5x7.5km region centered around the inclusion, I ignore nodes that do not readily contribute to illumination in the inclusion area of interest (AOI) (Figures \ref{fig:ideal-aoi-top} and \ref{fig:ideal-aoi-side}). I do this by performing RTM with all the nodes and shots in my model domain, and use the downgoing component of the hydrophone data. Next, I select with a masking function the AOI (in our case, immediately around the salt inclusion). After, I cross-correlate the RTM image produced by each node-gather with the area of interest from the full-stack RTM image. This gives me information about which nodes correlate most strongly with the full-stack image. I then sort the RTM node-gather images by this correlation statistic, and select those that have the highest correlation while ignoring those that do not. 


% AREA OF INTEREST USED FOR CORRELATIONS
\plot{ideal-aoi-top}{width=6in}{Area of interest (cyan) used in cross-correlation overlaid on full-stack mirror RTM image. Slice at depth 1,770m. }
\plot{ideal-aoi-side}{width=6in}{Area of interest (cyan) used in cross-correlation overlaid on full-stack mirror RTM image. Slice at inline position 214,800m.}



\par By comparing the RTM images using higher numbers of nodes (Figures \ref{fig:selective-RTMdown-side-stack-50}, \ref{fig:selective-RTMdown-side-stack-35}, \ref{fig:selective-RTMdown-top-stack-50}, \ref{fig:selective-RTMdown-top-stack-35}) with those using fewer nodes (Figures \ref{fig:selective-RTMdown-side-stack-25}, \ref{fig:selective-RTMdown-top-stack-25}, \ref{fig:selective-RTMdown-side-stack-15}, \ref{fig:selective-RTMdown-top-stack-15}), it is easy to see how image quality decreases as nodes are removed. The goal is to choose the lowest number of nodes that still correspond to a reasonably good RTM image. I ultimately chose to use the node subset that comprises the top 35 percent of (positively correlated) illumination contribution to the region around the inclusion, which amounts to 78 of the 288 original nodes being used.


% DOWNGOING ONLY
\plot{selective-RTMdown-side-stack-50}{width=4.5in}{RTM image from stacking top 50 percent of positively correlated node-gather images (214,800m inline position).}
\plot{selective-RTMdown-side-stack-35}{width=4.5in}{RTM image from stacking top 35 percent of positively correlated node-gather images (214,800m inline position).}
\plot{selective-RTMdown-side-stack-25}{width=4.5in}{RTM image from stacking top 25 percent of positively correlated node-gather images (214,800m inline position).}
\plot{selective-RTMdown-side-stack-15}{width=4.5in}{RTM image from stacking top 15 percent of positively correlated node-gather images (214,800m inline position).}

\plot{selective-RTMdown-top-stack-50}{width=4.5in}{RTM image from stacking top 50 percent of positively correlated node-gather images (1,770m inline position).}
\plot{selective-RTMdown-top-stack-35}{width=4.5in}{RTM image from stacking top 35 percent of positively correlated node-gather images (1,770m inline position).}
\plot{selective-RTMdown-top-stack-25}{width=4.5in}{RTM image from stacking top 25 percent of positively correlated node-gather images (1,770m inline position).}
\plot{selective-RTMdown-top-stack-15}{width=4.5in}{RTM image from stacking top 15 percent of positively correlated node-gather images (1,770m inline position).}



% Phase only objective function and its implications
\section{Phase only objective function and its implications}

\par Convergence using the level set objective function as I have formulated it will be partly determined by the ability of the modeling operator to match the phase and amplitudes in the observed data. The Green's function of the earth produces an elastic response, but the modeling operator I use assumes an acoustic response. I chose to model acoustically to avoid the added computational expense of modeling elastic waves. This means that my operator will have difficulty matching the amplitudes found in the observed data, even if I have the exact earth model. 

\par For this reason, I modify the objective function to make it agnostic to amplitude information and become primarily based on minimizing kinematic errors. I follow the work done in \cite{xukai_phase} and adopt an objective function ($\psi$) that includes trace-by-trace normalization of the data:

\begin{align}
	\psi(m) &=  \frac{1}{2} \sum_{s,g} {  \left\|  r_{d}(s,g)  \right\|^{2} } \\
	\psi(m) &=  \frac{1}{2} \sum_{s,g} {  \left\|  \frac{F(m,s,g) }{ \sqrt { F(m,s,g)^{T} F(m,s,g) } } -  \frac{ d_{obs}(s,g) }{ d_{obs}(s,g)^{T} d_{obs}(s,g)} \right\|^{2} }, 
\label{eq:phaseObj}
\end{align}

\noindent where $r_{d}$ is the normalized residual, $m$ is the velocity model, $d_{obs}$ is the field data, $F(m,s,g)$ is the acoustic modeling operator, and $s$ and $r$ are the shot and receiver indices respectively. I take the derivative of this new objective function to find a new gradient calculation:

\begin{align}
	J_{f}(m) &= r_{d}^{T}(s,g)   \frac{ \partial \left( \frac{F(m,s,g)}{\sqrt{F(m,s,g)^{T}F(m,s,g)} } \right) }{ \partial m } \\
	J_{f}(m) &= r_{d}^{T}(s,g)   \frac{ \partial \left( \frac{F(m,s,g)}{\sqrt{F(m,s,g)^{T}F(m,s,g)} } \right) }{ \partial F(m,s,g) } \frac{ \partial F(m,s,g)}{\partial m}.
\label{eq:phaseDer}
\end{align}

\noindent I represent this as a series of operators:
\begin{equation}
	J^{T}_{f}(m) = B^{T} P^{T} r_{d}(s,g),
\label{eq:phaseJac}
\end{equation}

\noindent where $B$ is the born operator, and $P$ is a new operator to account for the modifications made to the objective function. For this reason, the Gauss-Newton Hessian computation is also altered to account for $P$:

\begin{equation}
	H_{GN} =  P B B^{T} P^{T}.
\label{eq:phaseHess}
\end{equation}


\section{Inversion implementation}

\par Using the nodes selected as described in the previous section, I ran an inversion using a starting velocity model very similar to the smooth one used in migration (Figures \ref{fig:initvel-side}, \ref{fig:initvel-top}). This model extends into negative depth (above the water surface) since mirror wave propagation was used in the inversion. The algorithm used alternates between updating the salt boundary (level set) and the background velocity for each non-linear (outer-loop) iteration. Within each outer-loop iteration is an iterative inversion to find the search direction in either the level set or background velocity space using the Gauss-Newton Hessian. After this, a line search finds the optimal scaling parameter to apply to the search direction and then update the model. The full workflow used is described in Algorithm \ref{alg:3dhessInv}. For the data itself, I performed designature of the hydrophone and vertical components, PZ-summation to create the downgoing separated data, and a shaping filter to remove the bubble. These pre-processing steps are described in detail in Appendix \ref{append:A} and \ref{append:B}.

\multiplot{2}{initvel-side,initvel-top}{width=5in}{Inversion starting velocity model side view (a) and top view (b).}

\par Since the RTM images I created (Figures \ref{fig:ideal-aoi-top}, \ref{fig:ideal-aoi-side}) and those from Shell (Figures \ref{fig:original-shell-rtm-FRONT}, \ref{fig:original-shell-rtm-SIDE},\ref{fig:original-shell-rtm-TOP}) agree on the presence of some kind of inclusion in the salt, I begin the inversion by preferentially initializing the implicit surface in the region where I believe it to be (Figures \ref{fig:phi-side-it-0}, \ref{fig:phi-top-it-0}). For the first non-linear iteration, I also use guidance to extend the gradient footprint as described in chapter \ref{chap:chap4} (see Figure \ref{fig:saltmask-side-it-0}). Because of this guidance, the first inversion iteration is capable of pushing the implicit surface value below zero to create a level set marking the inclusion. However, for following iterations, I do not use interpreter guidance to extend the gradient. In the case where an inclusion takes shape because of the first update, then the standard level set gradient is active around the inclusion edge and is sufficient to adjust it. In following iterations, the gradient can close or open the inclusion further without the need for an extended gradient (see Figure \ref{fig:saltmask-side-it-2}). It is only the first iteration where that extension is necessary. In the case where the first update does not begin an inclusion, I assume that the data does not support the existence of one after all. 

\par Because the nodes chosen for inversion are focused on illuminating the inclusion area, I limit the extent of the level set updating to the inclusion area of interest (see Figure \ref{fig:phi-side-it-35}). By doing this, I reduce spurious updating of the salt boundary in regions that receive poor illumination by the chosen acquisition geometry, which would likely be driven by artifacts in the gradient.


\plot{phi-side-it-0}{width=5in}{Initial implicit surface (214,800m inline position).}
\plot{phi-side-it-35}{width=5in}{Implicit surface after 35 iterations of inversion (214,800m inline position).}
\plot{phi-top-it-0}{width=6in}{Initial implicit surface (1,700m depth position).}
\multiplot{2}{saltmask-side-it-0,saltmask-side-it-2}{width=5in}{Masking used for first level set iteration (a) and for second level set iteration (b). Masking in (a) is based off of $\delta(\phi)$, while masking in (b) is based off of $\delta(\phi,G)$.}



\begin{algorithm}
    \caption{Alternating Gauss-Newton Hessian updating algorithm}
    \label{alg:3dhessInv}
    \begin{algorithmic}[1]
        \Procedure{LevelSetInversion-order2}{ $d_{\text{obs}}$,$\phi_{0}$,$b_{0}$ }

        \For {$i$ in (1,$N$)}

			\State $d_{\text{syn}}(i) = \text{F}(\phi_{i},b_{i-1}) $

			\State $\triangle d_{i} = d_{\text{obs}} - d_{\text{syn}}(i) $

			\State $g_{i} = D^{T} B^{T} \triangle d_{i} $


        	\If{EvenNumberedIteration}

	            \State $\triangle \lambda_{i} = \textbf{CGGNHessianInvSalt}(g_{i}) $

    			\State $\triangle \phi_{i} = \text{D}( \triangle \lambda_{i}) $

	            \State $\triangle b_{i} = 0$

	            \State $\alpha = \textbf{linesearch}(\triangle \phi_{i}) $

	            \State $\beta = 0 $

            \Else
	            \State $\triangle \phi_{i} = 0$

	            \State $\triangle b_{i} = \textbf{CGGNHessianInvBack}(g_{i}) $

	            \State $\alpha = 0 $

	            \State $\beta = \textbf{linesearch}(\triangle b_{i}) $
	            
            \EndIf
            \State $  \phi_{i} =  \phi_{i-1} - \alpha \cdot \triangle \phi_{i} $
            \State $  b_{i} =  b_{i-1}  - \beta \cdot \triangle b_{i}$

        \EndFor
        \State Return $m(\lambda{N},b_{N})$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}


\par After running this alternating inversion algorithm for 35 iterations, I find that an inclusion was created in the model, and that the background velocity model (which includes the inclusion velocity) has been updated in a manner that is more geologically consistent (Figures \ref{fig:velmodel-side-it-35}, \ref{fig:velmodel-top-it-35}). Taking the difference between the beginning and final models gives a better view of the updates made, highlighting a shift in the ocean bottom interface, and adding some higher velocity zones near the top of salt (Figures \ref{fig:velmodel-diff-side-35}, \ref{fig:velmodel-diff-top-35}). By looking at the difference between the starting and final background velocity model, we can see that the inclusion velocity has decreased about 130[m/s] (from a starting guess velocity of 4250[m/s]) (Figures \ref{fig:velback-diff-top-35}, \ref{fig:velback-diff-side-35}). The objective function value shows a steady decrease over non-linear iterations, with the predominant decreases occurring from updates in the background velocity, which have a more significant impact on events in the data space (compare Figure \ref{fig:inv-full-objfunc} and \ref{fig:inv-salt-objfunc}).

\plot{velmodel-side-it-35}{width=6in}{Velocity model after 35 iterations (214,800m inline position).}
\plot{velmodel-top-it-35}{width=6in}{Velocity model after 35 iterations (1,700m depth position).}
\plot{velmodel-diff-side-35}{width=6in}{Full velocity model difference between model at 35 iterations and starting model (214,800m inline position).}
\plot{velmodel-diff-top-35}{width=6in}{Full velocity model difference between model at 35 iterations and starting model (1,700m depth position).}
\plot{velback-diff-top-35}{width=6in}{Background velocity model difference between model at 35 iterations and starting model (1,700m depth position).}
\plot{velback-diff-side-35}{width=6in}{Background velocity model difference between model at 35 iterations and starting model (214,800m inline position).}

\plot{inv-full-objfunc}{width=6in}{Objective function over non-linear (outer-loop) iterations.}
\plot{inv-salt-objfunc}{width=6in}{Contribution of level set updating to the objective function decrease over non-linear (outer-loop) iterations.}

\par In order to validate an actual improvement in the velocity model, I perform RTM on both the initial model (unsmoothed) and the final model after inversion and compare the two images. The area where one would expect to see the inclusion make the most difference would be the area directly below it. I find that coherency of some of the sediment layers is improved in the region where they meet the salt flank (compare Figures \ref{fig:inc-zone-before-214800} and \ref{fig:inc-zone-after-214800}). In order to determine how much influence the inclusion alone has on the image, I performed RTM with a model using the salt and inclusion updates, but with the original background velocity for areas outside the salt (Figure \ref{fig:inc-zone-after2-214800}). I find that the image coherency is still improved on a number of reflectors below the inclusion. None of this level of detail is present in the original Shell RTM image (Figure \ref{fig:inc-zone-shell-214800}). Similar improvements in the RTM image can be found below the inclusion at inline position 214,950m (compare Figures \ref{fig:inc-zone-before-214950}, \ref{fig:inc-zone-after-214950}, and \ref{fig:inc-zone-after2-214950}).

\plot{inc-zone-vel-diff-214800}{width=5in}{Difference between final velocity model and starting velocity model (214,800m inline position).}

% \plot{inc-zone-before-214800}{width=5in}{Migrated RTM image in inclusion zone using starting velocity model (214,800m inline position).}
% \plot{inc-zone-after-214800}{width=5in}{Migrated RTM image in inclusion zone using final velocity model after 35 iterations (214,800m inline position).}

\multiplot{2}{inc-zone-before-214800,inc-zone-after-214800}{width=5in}{Migrated RTM image in inclusion zone at 214,800m inline position using starting velocity model (a) and with velocity model after 35 iterations (b).}


\plot{inc-zone-after2-214800}{width=5in}{Migrated RTM image in inclusion zone using final salt model after 35 iterations (214,800m inline position), but using the original background velocity.}
\plot{inc-zone-shell-214800}{width=5in}{Migrated RTM image in inclusion zone provided by Shell (214,800m inline position).}

\clearpage
\plot{inc-zone-vel-diff-214950}{width=5in}{Difference between final velocity model and starting velocity model (214,950m inline position).}
% \plot{inc-zone-before-214950}{width=5in}{Migrated RTM image in inclusion zone using starting velocity model (214,950m inline position).}
% \plot{inc-zone-after-214950}{width=5in}{Migrated RTM image in inclusion zone using final velocity model after 35 iterations (214,950m inline position).}

\multiplot{2}{inc-zone-before-214950,inc-zone-after-214950}{width=5in}{Migrated RTM image in inclusion zone at 214,950m inline position using starting velocity model (a) and with velocity model after 35 iterations (b).}

\plot{inc-zone-after2-214950}{width=5in}{Migrated RTM image in inclusion zone using final salt model after 35 iterations (214,950m inline position), but using the original background velocity.}



\par The area above the salt diapir is mostly improved by the background velocity model updating, but also seems to show a fault feature (see Figure \ref{fig:top-zone-after-214820}) that is not evident in the Shell RTM (Figure \ref{fig:top-zone-shell-214820}) or in the initial RTM image that I created (Figure \ref{fig:top-zone-before-214820}). A fault at this position could be produced from the upward stresses created by the salt diapir below.

\plot{top-zone-vel-diff-214820}{width=5in}{Difference between final velocity model and starting velocity model (214,820m inline position).}
\plot{top-zone-shell-214820}{width=5in}{Migrated RTM image in top of salt zone provided by Shell (214,820m inline position).}
\multiplot{2}{top-zone-before-214820,top-zone-after-214820}{width=5in}{Migrated RTM image in top of salt zone at 214,820m inline position using starting velocity model (a) and using velocity model after 35 iterations (b). (A) is the position of an artifact visible in Figure \ref{fig:top-zone-before-214820}. (B) is the position of a potential fault, visible in (b).}

% \plot{top-zone-after-214820}{width=5in}{Migrated RTM image in top of salt zone using final velocity model after 35 iterations (214,820m inline position). (A) is the position of an artifact visible in Figure \ref{fig:top-zone-before-214820}. (B) is the position of a potential fault, visible in Figure \ref{fig:top-zone-after-214820}.}




% CONCLUSIONS
\section{Conclusions}
\par In order to evaluate the effectiveness of shape optimization with level sets on a industry level data, I applied my method on a Gulf of Mexico OBN dataset from Shell. The data suggests an inclusion in the salt model, which makes it an ideal example for demonstrating the effectiveness of interpreter guidance as well. By intelligently selecting nodes that most effectively illuminate the inclusion area, I can temper the computational expense of the inversion. I explain how the phase-only objective function can alleviate the problems arising from amplitude differences between the field data and acoustically modeled data. I then invert for the level set shape and background velocity model using a Gauss-Newton Hessian inversion algorithm, and find that it recovers an inclusion shape consistent with the RTM images. When I repeat the RTM images using the new velocity model, I find that I gain more coherency in the sediment layers directly below the inclusion that terminate against the salt flank, as well as other improvements near the top of salt and elsewhere. This demonstrates the efficacy of the method for application to field datasets.




