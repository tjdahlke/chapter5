[general_parameters]

n1=418
// overall nz before any BC padding, which includes the airpad value. (nzs=nz-airpad)
airpad=105
n2=661
n3=623

d1=10.0
d2=10.0
d3=10.0

// RBF zone parameters
n1r=159
n2r=301
n3r=301
f1r=163
f2r=410
f3r=135

// o1 equals dz*(airpad - 5). 5 is assuming the laplacian stencil in the forward operator has a length of 5.
o1=-1375
o2=45980.0 
// 		y, crossline: 	36569.8 is limit based on intersection of salt and background model
o3=211780.0
// 		x, inline:		204804  is limit based on intersection of salt and background model


nt=9360.0
dt=0.00075
bs=25
// bs needs to be smaller than min(nz,nx,ny) I think

// Used for the exponential dampening
alpha=150.0

randpad=45
pad=90
pctG=0.9
seed=2017
kappa=0.05

# RBF parameters
beta=0.06   #0.25 # Higher is spiky, low is smooth
trad=14 	#9   # Radius of the RBF table # trad = SQRT(-LOG(threshold)/(beta**2))

