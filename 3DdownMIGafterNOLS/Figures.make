

##########################################################################################
# FIGURES

ER:
	# No ER Figures in 3DdownMIGafterNOLS

CR: CRv ${R}/inc-zone-afterNOLS-214800.pdf ${R}/top-zone-afterNOLS-214820.pdf \
${R}/inc-zone-afterNOLS-214950.pdf


NR:
	# No NR Figures in 3DdownMIGafterNOLS


CRv: ${R}/inc-zone-afterNOLS-214800.v ${R}/top-zone-afterNOLS-214820.v \
${R}/inc-zone-afterNOLS-214950.v

##########################################################################################

sliceparINC=n3=1 min1=1000.0 max1=2795 min2=49000 max2=50000
sliceparTOP=n3=1 min1=500.0 max1=1500 min2=49000 max2=50000

# Vel model used in migration
${R}/FWI-inc-zone-afterNOLS-%.v: RTMimageDOWN.H
	Tpow tpow=3 < $< | ../keepBin/laplacian.x | Window3d ${sliceparINC} min3=$* | Grey o2num=49100 d2num=200 title=" " label1='Depth [m]'  label2='Crossline [m]' label3=' ' ${FIG}$@notext
	vp_annotate batch=y < $@notext text=../Anno/inc-zone-$*.txt > $@
	rm $@notext

${R}/FWI-top-zone-afterNOLS-%.v: RTMimageDOWN.H
	Tpow tpow=3 < $< | ../keepBin/laplacian.x | Window3d ${sliceparTOP} min3=$* | Grey o2num=49100 d2num=200 title=" " label1='Depth [m]'  label2='Crossline [m]' label3=' ' ${FIG}$@notext
	vp_annotate batch=y < $@notext text=../Anno/top-zone-$*.txt > $@
	rm $@notext

${R}/FWI-top-zone-vel-afterNOLS.v:
	Window3d ${sliceparTOP} min3=214820 < migvel.H | Grey newclip=1 color=jc o2num=49100 d2num=200 title=" " label1='Depth [m]'  label2='Crossline [m]' label3=' ' ${FIG}$@


${R}/FWI-inc-zone-vel-afterNOLS.v:
	Window3d ${sliceparINC} min3=214800 < migvel.H | Grey newclip=1 color=jc o2num=49100 d2num=200 title=" " label1='Depth [m]'  label2='Crossline [m]' label3=' ' ${FIG}$@

##########################################################################################
# Convert to pdfs
%.pdf: %.v
	pstexpen $< $*.ps color=y fat=y
	ps2pdf -dEPSCrop -dAutoFilterColorImages=false  -dColorImageFilter=/FlateEncode  -dAutoFilterGrayImages=false  -dGrayImageFilter=/FlateEncode  -dAutoFilterMonoImages=false  -dMonoImageFilter=/CCITTFaxEncode $*.ps $@
	rm $*.ps
	





