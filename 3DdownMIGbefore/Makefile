include ${SEPINC}/SEP.top
main=$(shell dirname `pwd`)
project=$(shell pwd)
projbase=$(shell pwd | xargs basename)
INVname=3DdownMIG.before
genpar=${P}/cardamom.before.p
gradtag=RTM3d.before
include ${main}/ParamMakefile
include ${main}/CompileMakefile
include Figures.make

# cees-mazama
ifeq ($(HOSTNAME), cees-mazama)
	rawdataDN=/data/biondo/tjdahlke/pz-summation/DOWN_final.H
endif

# cees-rcf
ifeq ($(HOSTNAME), cees-rcf.stanford.edu)
	rawdataDN=/data/sep/tjdahlke/cardamom_downgoing/DOWN_final.H
endif

setdatapaths:
	echo "datapath=${project}/scratch/" > ${path_out}/.datapath
	echo "datapath=${project}/scratch/" > ${project}/.datapath


#################################################################################
# Make the initial velocity models

itf=0
# BEFORE RTM IMAGE MODEL
${migvel}:
	make --directory=../3dFWIdown25 extended_vel${itf}.H
	Cp ../3dFWIdown25/extended_vel${itf}.H $@

${abcs}: ${migvel} ${B}/ABCweights3d.x 
	${SINGRUN} ${B}/ABCweights3d.x alpha=0.20 padT=0.0 padB=${pad} padY=${pad} padX=${pad} < $< > $@

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# Other stuff

make_all_randomABCs: ${recpos}.DOWN ${migvel} ${B}/randomB.x
	python ${SCRIPTDIR}/PBS_randomABCs.py ${recpos}.DOWN ${genpar} ${migvel} ${project}/randomABCs ${SCRIPTDIR}/PBStemplate_M1.sh ${SCRIPTDIR} ${B} ${pad} ${randpad} 1

${wavelet}: 
	Wavelet phase=minimum fund=30.0 wavelet=ricker0 d1=${dt} n1=${nt} | Scale rscale=0.001 > $@

#################################################################################
# Make the initial synthetic data based on the initial model

make_all_syndata_for_mig_inc ${project}/wrk/SYN_DOWN_MIG-100.H: ${recpos}.DOWN ${B}/main3d.x ${wavelet} ${B}/Window_key ${migvel}  ${pathtempinv}/obsdata_DOWN.H make_all_randomABCs
	python ${SCRIPTDIR}/make_all_syndata.py ${PBS_common} velmodel=${migvel} inversionname=$@ \
	${inversion_PBS} recpos=${recpos}.DOWN souhead=${pathtempinv}/obsdata_DOWN.H nfiles=$(shell less ${recpos}.DOWN | wc -l)  prefix=SYN_DOWN_MIG flag='mig' rand_path=${project}/randomABCs

#################################################################################
#################################################################################
# Process the DOWNGOING observed data

small_model_data.DOWN:
	Window_key synch=1 \
	key1='sy' mink1=46000  maxk1=52560 \
	key2='sx' mink2=211800 maxk2=217800 \
	key3='ry' mink3=46000  maxk3=52560 \
	key4='rx' mink4=211800 maxk4=217800 \
	< ${rawdataDN} > $@ hff=${project}/$@@@


# Process the DOWNGOING observed data. Injecting sources at their relative water surface location. Injecting nodes at the mirror locations.
${souhead}.DOWN ${pathtempinv}/obsdata_DOWN.H: small_model_data.DOWN
	Headermath maxsize=100000 \
	key1=nodeId eqn1='nodeid' \
	key2=SZ eqn2='@INT(0.5+((${randpad}*${dz})-${oz}+sdepth)/${dz})' \
	key3=SY eqn3='@INT(0.5+(sy-${oy}+${dy}*${randpad})/${dy})' \
	key4=SX eqn4='@INT(0.5+(sx-${ox}+${dx}*${randpad})/${dx})' \
	key5=GZ eqn5='@INT(0.5+((${randpad}*${dz})-${oz}-gwdep)/${dz})' \
	key6=GY eqn6='@INT(0.5+(ry-${oy}+${dy}*${randpad})/${dy})' \
	key7=GX eqn7='@INT(0.5+(rx-${ox}+${dx}*${randpad})/${dx})' \
	key8=OFFSET eqn8='offset' \
	delete_keys=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60 \
	< $< hff=${souhead}.DOWN > ${pathtempinv}/obsdata_DOWN.H

${recpos}.DOWN: ${souhead}.DOWN
	python ${SCRIPTDIR}/find_all_nodeIds.py infile=$< outfile=$@

#################################################################################
${project}/hydr_%.H: ${pathtempinv}/obsdata_%.H
	Interp d1out=${dt} n1out=${nt} < $< > $@

${recpos}.%: ${souhead}.%
	python ${SCRIPTDIR}/find_all_nodeIds.py infile=$< outfile=$@

#################################################################################

# Make RTM images with data
RTMimageDOWN.H: ${migvel} ${wavelet} ${souhead}.DOWN ${project}/hydr_DOWN.H \
	${B}/BORN3d.x ${B}/ADD.x ${project}/wrk/SYN_DOWN_MIG-100.H
	python ${SCRIPTDIR}/RTM_imaging3d.py ${inversion_PBS} ${Hessian_PBS} gradtag=${gradtag} \
	migvel=${migvel} obs_path=${project}/hydr_DOWN.H inversionname=$@ \
	rand_path=${project}/randomABCs nfiles=$(shell less ${recpos}.DOWN | wc -l) \
	recpos=${recpos}.DOWN nt=${nt} souhead=${project}/hydr_DOWN.H \
	writewave=0 queues=sep queues_cap=250 phaseOnly=0

#################################################################################

include ${SEPINC}/SEP.bottom

