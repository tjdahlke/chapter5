[general_parameters]
n1=375
// overall nz before any BC padding, which includes the airpad value. (nzs=nz-airpad)
airpad=105
n2=751
n3=723

d1=10.0
d2=10.0
d3=10.0

// RBF zone parameters
n1r=159
n2r=301
n3r=301
f1r=163
f2r=410
f3r=135


// 		Origins are based on the REGION of interest, not the REGION of propagation!!
o1=-1000
o2=44990.0 
// 		y, crossline: 	36569.8 is limit based on intersection of salt and background model
o3=211790.0
// 		x, inline:		204804  is limit based on intersection of salt and background model

nt=9360.0
//4666.0
// Could totally do 500 for testing
dt=0.00075
//0.002
bs=25
// 15 works well for small models
// 190 works well for a 200x999x1000 model
// bs needs to be smaller than min(nz,nx,ny) I think

// Used for the exponential dampening
alpha=150.0

randpad=35
pad=90
pctG=0.9
seed=2017
kappa=0.05

# RBF parameters
beta=0.06   #0.25 # Higher is spiky, low is smooth
trad=14 	#9   # Radius of the RBF table # trad = SQRT(-LOG(threshold)/(beta**2))

