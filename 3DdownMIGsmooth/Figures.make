
##########################################################################################
# FIGURES

ER: ${R}/mig-wavelet.pdf ${R}/mig-wavelet-spectra.pdf ${R}/migvel-side.pdf ${R}/migvel-top.pdf


CR: CRv ${R}/ideal-aoi-side.pdf ${R}/ideal-aoi-top.pdf ${R}/downgoing-full-side.pdf ${R}/downgoing-full-top.pdf\
${R}/selective-RTMdown-top-stack-15.pdf ${R}/selective-RTMdown-top-stack-25.pdf ${R}/selective-RTMdown-top-stack-35.pdf ${R}/selective-RTMdown-top-stack-50.pdf \
${R}/selective-RTMdown-side-stack-50.pdf ${R}/selective-RTMdown-side-stack-35.pdf ${R}/selective-RTMdown-side-stack-25.pdf ${R}/selective-RTMdown-side-stack-15.pdf \


NR:
	# No NR Figures in 3DdownMIGsmooth

CRv: ${R}/selective-RTMdown-top-stack-15.v ${R}/selective-RTMdown-top-stack-25.v ${R}/selective-RTMdown-top-stack-35.v ${R}/selective-RTMdown-top-stack-50.v \
${R}/selective-RTMdown-side-stack-50.v ${R}/selective-RTMdown-side-stack-35.v ${R}/selective-RTMdown-side-stack-25.v ${R}/selective-RTMdown-side-stack-15.v \

##########################################################################################

slicepar=min3=214800 n3=1 min1=400.0 max1=2300
sliceparT= min1=1770.0 n1=1 
sliceparF=min2=49620 n2=1 min1=400.0 max1=2300 min3=212000 max3=218000

# Vel model used in migration
${R}/migvel-side.v: ${migvel}
	Window3d ${slicepar} < $<  | Grey o1num=500 o2num=46000 d2num=2000 color=jc wantscalebar=y newclip=1 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Velocity [m/s]' ${FIG}$@

${R}/migvel-top.v: ${migvel}
	Window3d ${sliceparT} < $< | Transp plane=12 > $@1
	Grey o1num=212000 d1num=2000 o2num=46000 color=jc newclip=1 title=" " label1='Inline [m]' label2='Crossline [m]' label3='Velocity [m/s]' < $@1 ${FIG}$@
	rm $@1

# Spectra of source wavelet
${R}/mig-wavelet-spectra.v: ${wavelet}
	Spectra < ${wavelet} | Scale | Graph label1='Freq [Hz]' label2='Relative Magnitude' max1=75.0 title=" " ${FIG}$@

# Plot of source wavelet
${R}/mig-wavelet.v: ${wavelet}
	< ${wavelet} Graph label1='Time [s]' label2='Amplitude' max1=0.25 title=" " ${FIG}$@

# Area of interest figures

${R}/ideal-aoi-top.v: ${outputsRMS}/ideal_AOI.H RTMimageDOWN.H 
	Window3d ${sliceparT} < RTMimageDOWN.H | Transp plane=12 | Grey title=" " label2='Crossline [m]' label1='Inline [m]' ${FIG}$@1
	Window3d ${sliceparT} < ${project}/inclusion_AOI.H | Transp plane=12 > $@2
	Contour plotfat=10 nc=1 c0=0.5 plotcol=2 title=" " < $@2 ${FIG}$@3
	vp_Overlay $@1 $@3 > $@
	rm $@1 $@2 $@3

${R}/ideal-aoi-side.v: ${outputsRMS}/ideal_AOI.H RTMimageDOWN.H 
	Window3d ${slicepar} < RTMimageDOWN.H | Tpow tpow=4 | Grey o2num=46000 d2num=2000 title=" " label1='Depth [m]' label2='Crossline [m]' ${FIG}$@1
	Window3d ${slicepar} < ${project}/inclusion_AOI.H > $@2
	Contour plotfat=10 nc=1 c0=0.5 plotcol=2 title=" " o2num=46000 d2num=2000 < $@2 ${FIG}$@3
	vp_Overlay $@1 $@3 > $@
	rm $@1 $@2 $@3

# Full RTM figures
${R}/downgoing-full-side.v: RTMimageDOWN.H
	Window3d ${slicepar} < $< | Tpow tpow=3 | ../keepBin/laplacian.x > $@1
	Tpow tpow=4 < $@1 | Grey o2num=46000 d2num=2000 pclip= title=" " label1='Depth [m]' label2='Crossline [m]' ${FIG}$@
	rm $@1

${R}/downgoing-full-top.v: RTMimageDOWN.H
	Window3d ${sliceparT} < $< | Tpow tpow=3 | ../keepBin/laplacian.x | Transp plane=12 > $@1
	Grey title=" " label1='Inline [m]' label2='Crossline [m]' < $@1 ${FIG}$@
	rm $@1

${R}/downgoing-full-front.v: RTMimageDOWN.H
	Window3d ${sliceparF} < $< | Tpow tpow=3 | ../keepBin/laplacian.x > $@1
	Grey o2num=213000 d2num=1000 title=" " label1='Depth [m]' label2='Inline [m]' < $@1 ${FIG}$@
	rm $@1

# Select RTM figures
${R}/selective-RTMdown-side-stack-%.v: stacked_RTM_posDOT_%.H
	Window3d ${slicepar} < $< | Tpow tpow=3 | Grey o2num=46000 d2num=2000 pclip= title=" " label1='Depth [m]' label2='Crossline [m]' ${FIG}$@

${R}/selective-RTMdown-top-stack-%.v: stacked_RTM_posDOT_%.H
	Window3d ${sliceparT} < $< | Transp plane=12 | Grey title=" " label1='Inline [m]' label2='Crossline [m]' ${FIG}$@


##########################################################################################

# Convert to pdfs
%.pdf: %.v
	pstexpen $< $*.ps color=y fat=y
	ps2pdf -dEPSCrop -dAutoFilterColorImages=false  -dColorImageFilter=/FlateEncode  -dAutoFilterGrayImages=false  -dGrayImageFilter=/FlateEncode  -dAutoFilterMonoImages=false  -dMonoImageFilter=/CCITTFaxEncode $*.ps $@
	rm $*.ps
	




