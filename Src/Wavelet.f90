program Himage

  use sep

  implicit none

  include "fftw3.f"

  integer                              :: n1, w1, w2, w3, w4, nshift, i
  real                                 :: d1, dw, w, pi, tdelay
  real, dimension(:), allocatable      :: data

  integer*8                            :: planf, planb
  complex, dimension(:), allocatable   :: in, out

  call sep_init()

  call from_param("n1", n1)
  call from_param("d1", d1)
  call from_param("w1", w1)
  call from_param("w2", w2)
  call from_param("w3", w3)
  call from_param("w4", w4)
  call from_param("tdelay", tdelay, 0.)

  if(w4 >= 1./(2.*d1)) call erexit("w4 is too large")

  allocate(data(n1))
  allocate(in(n1), out(n1))

  data = 0.
  in = 0.
  out = 0.

  dw = 1./(n1*d1)
  pi = 3.14159265358979323846
  nshift = tdelay/d1

  call sfftw_plan_dft_1d(planf, n1, in, out, FFTW_FORWARD, FFTW_MEASURE)
  call sfftw_plan_dft_1d(planb, n1, out, in, FFTW_BACKWARD, FFTW_MEASURE)

  do i=1,n1/2
    w = (i-1)*dw
    if(w < w1) then
      out(i) = 0.
    elseif(w >= w1 .and. w < w2) then
      out(i) = cos(pi/2.*(w2-w)/(w2-w1))*cos(pi/2.*(w2-w)/(w2-w1))
    elseif(w >= w2 .and. w < w3) then
      out(i) = 1.
    elseif(w >= w3 .and. w < w4) then
      out(i) = cos(pi/2.*(w-w3)/(w4-w3))*cos(pi/2.*(w-w3)/(w4-w3))
    else
      out(i) = 0.
    endif

  enddo

  call sfftw_execute_dft(planb, out, in)
  in = 2*in/sqrt(1.*n1)

  do i=1,nshift
    data(i) = real(in(i+n1-nshift))
  enddo
  do i=nshift+1,n1
    data(i) = real(in(i-nshift))
  enddo

  call to_history("n1", n1)
  call to_history("o1", 0.)
  call to_history("d1", d1)
!  call to_history("d1", dw)

  call sep_write(data)
!  call sep_write(real(out))

  call sep_close()
end program
