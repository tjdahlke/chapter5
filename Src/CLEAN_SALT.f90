!===============================================================================
!		Isolates the outside of the salt
!		MODEL:  NZxNYxNX	(input salt)
!		DATA:   NZxNYxNX	(clean output salt)
!
!===============================================================================

program CLEAN_SALT
	use sep
	implicit none

	real,dimension(:,:,:),allocatable	:: input,output
	logical								:: verbose
	integer,dimension(8)               	:: timer0,timer1
	real								:: timer_sum=0.,threshold,val
	integer								:: nz,ny,nx,iz,iy,ix,izR,iyR,ixR

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	call from_param("verbose",verbose,.TRUE.)
	if (verbose) write(0,*) "==================== Read in initial parameters"
	call from_param("threshold",threshold,0.0)
	call from_param("val",val,1.0)
	call from_history("n1",nz)
	call from_history("n2",ny)
	call from_history("n3",nx)

	IF(verbose) WRITE(0,*) "========= Allocate ==================================="
	allocate(input(nz,ny,nx))
	allocate(output(nz,ny,nx))

	IF(verbose) WRITE(0,*) "========= Read in input =================="
	CALL sep_read(input)

	output=1.0
!##########################################################################################

	! Z down
	do ix=1,nx
		do iy=1,ny
			do iz=1,nz
				if (input(iz,iy,ix)<threshold) then
					output(iz,iy,ix)=output(iz,iy,ix)*0.0
				else
					EXIT
				end if
			end do
		end do
	end do

	! Z up
	do ix=1,nx
		do iy=1,ny
			do iz=1,nz
				izR=nz-iz+1
				if (input(izR,iy,ix)<threshold) then
					output(izR,iy,ix)=output(izR,iy,ix)*0.0
				else
					EXIT
				end if
			end do
		end do
	end do

	! X left
	do iy=1,ny
		do iz=1,nz
			do ix=1,nx
				if (input(iz,iy,ix)<threshold) then
					output(iz,iy,ix)=output(iz,iy,ix)*0.0
				else
					EXIT
				end if
			end do
		end do
	end do

	! X right
	do iy=1,ny
		do iz=1,nz
			do ix=1,nx
				ixR=nx-ix+1
				if (input(iz,iy,ixR)<threshold) then
					output(iz,iy,ixR)=output(iz,iy,ixR)*0.0
				else
					EXIT
				end if
			end do
		end do
	end do

	! Y left
	do ix=1,nx
		do iz=1,nz
			do iy=1,ny
				if (input(iz,iy,ix)<threshold) then
					output(iz,iy,ix)=output(iz,iy,ix)*0.0
				else
					EXIT
				end if
			end do
		end do
	end do

	! Y right
	do ix=1,nx
		do iz=1,nz
			do iy=1,ny
				iyR=ny-iy+1
				if (input(iz,iyR,ix)<threshold) then
					output(iz,iyR,ix)=output(iz,iyR,ix)*0.0
				else
					EXIT
				end if
			end do
		end do
	end do

!##########################################################################################

	if(verbose) write(0,*) "==================== Write output file"
	call to_history("n1",nz)
	call to_history("n2",ny)
	call to_history("n3",nx)
	call to_history("label1",'z [m]')
	call to_history("label2",'y [m]')
	call to_history("label2",'x [m]')
	call sep_write(output)

	if (verbose) write(0,*) "Program complete"

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec)",timer_sum/1000
	write(0,*) "timer sum (min)",timer_sum/1000/60
	write(0,*) "DONE!"
	call sep_close()

end program