extern "C" {
void laplacianFOR3d_noABC(int n, int n1, int n12, float *coef,float *v2dt, float *prev,float *cur,float *next,float *nextF);
}

extern "C" {
void waveADJ_noABC(int n, int n1, int n12, float *coef,float *prev,float *curT, float *cur,float *next,float *nextF);
}

extern "C" {
void comboBornFOR(int n, float *v2dt, float *model1, float *prev,float *curr, float *next, float *out, float dt2);
}

extern "C" {
void comboBornADJ(int n, float *v2dt, float *prev,float *curr, float *next, float *out, float dt2);
}

extern "C" {
void simple_mult( int n,  float *input,  float *output, float h);
}

extern "C" {
void sumABCs( int n,  float *curZ,  float *curY,  float *curX,  float *output);
}

extern "C" {
void first_deriv2oZ( int n, int n1, int n12, float *input, float *output, float h, float scale);
}

extern "C" {
void first_deriv2oX(int n, int n1, int n12, float *input, float *output, float h, float scale);
}

extern "C" {
void first_deriv2oY(int n, int n1, int n12, float *input, float *output, float h, float scale);
}

extern "C" {
void addABC( int n, float dt, float *abcW, float *vel, float *cur, float *curD, float *newW, float *newABC);
}







extern "C" {
void laplacianFOR3d(int n, int n1, int n12, float *coef,float *v2dt,float *abc, float *prev,float *cur,float *next,float *nextF);
}

extern "C" {
void waveADJ(int n, int n1, int n12, float *coef,float *v2dt,float *abc, float *prev,float *curT,float *cur,float *next,float *nextF);
}

extern "C" {
void scatFOR3d(int n, float *wvfld,float *data, float *refl);
}

extern "C" {
void scatADJ3d(int n, float *wvfld,float *data, float *refl);
}




extern "C" {
void scatFOR3dExt(int n, float *wvfld,float *data, float *refl);
}

extern "C" {
void scatADJ3dExt(int n, float *wvfld,float *data, float *refl);
}




extern "C" {
void scale3d(int n, float *model,float *data, float *coef);
}

extern "C" {
void secder3d(int n, float *prev,float *curr, float *next, float *out, float dt2);
}

extern "C" {
void laplacianADJ3d(int n, int n1, int n12, float *coef,float *v2dt,float *abc, float *prev,float *cur,float *next,float *nextF);
}