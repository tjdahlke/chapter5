
!===============================================================================
!		Places an elipsoid of given dimensions on a file
!		MODEL:  Nz x Ny x Nx
!		DATA:  	Nz x Ny x Nx
!===============================================================================

program CIRCLE3d
	use sep
	implicit none

	integer								:: nz,nx,iz,ix,iy,ny
	real,dimension(:,:,:),allocatable	:: data
	real								:: replaceval,radx,radz,rady,centx,centz,centy
	real 								:: oy,dy,ox,dx,oz,dz,x,z,y,test
	logical								:: verbose

	call sep_init()

	call from_param("verbose",verbose,.TRUE.)
	if(verbose) write(0,*) "==================== Read in initial parameters"
	call from_param("centx",centx)
	call from_param("centz",centz)
	call from_param("centy",centy)
	call from_param("radx",radx)
	call from_param("radz",radz)
	call from_param("rady",rady)
	call from_param("replaceval",replaceval,-1.0)
	call from_history("n1",nz)
	call from_history("n2",ny)
	call from_history("n3",nx)
	call from_history("d1",dz)
	call from_history("d2",dy)
	call from_history("d3",dx)
	call from_history("o1",oz)
	call from_history("o2",oy)
	call from_history("o3",ox)

	allocate(data(nz,ny,nx))
	data=0.0

	do ix=1,nx
		x = ox + (ix-1)*dx -centx
		do iy=1,ny
			y = oy + (iy-1)*dy -centy
			do iz=1,nz
				z = oz + (iz-1)*dz - centz
				test = (x/radx)**2 + (z/radz)**2 + (y/rady)**2
				if ( test<=1.0 ) then
					data(iz,iy,ix)=replaceval
				end if
			end do
		end do
	end do

!##########################################################################################


	if(verbose) write(0,*) "==================== Write out RBF file (implicit surface)"
	call sep_write(data)

	if (verbose) write(0,*) "APPLY_RBF program complete"
	deallocate(data)


end program
