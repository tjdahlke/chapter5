!===============================================================================
!
!
!   MODEL:  
!   DATA:   
!===============================================================================

program RBF_BUILD_TABLE
  use sep
  use rbf_mod3d
  implicit none

  integer                               :: nrbf, nz, nx, ny, trad
  real                                  :: beta, threshold, timer_sum=0.
  real, dimension(:,:,:), allocatable   :: rbftable
  logical                               :: verbose
  integer, dimension(8)                 :: timer0, timer1

  call sep_init()
  call DATE_AND_TIME(values=timer0)

  if(verbose) write(0,*) "================ Read in initial parameters /allocate =================="
  call from_param("n1", nz)
  call from_param("n2", nx)
  call from_param("n3", ny)
  call from_param("threshold", threshold, 0.01)
  call from_param("verbose", verbose, .TRUE.)
  call from_param("trad", trad, 15)
  call from_param("beta", beta, 0.15)
  allocate(rbftable(2*trad+1, 2*trad+1, 2*trad+1))

!##########################################################################################
  if(verbose) write(0,*) "========= Initialize the submodules ====================================="
  call rbf_init_buildtable(trad,threshold,beta)
  call rbftable_build(rbftable)
!##########################################################################################

  if(verbose) write(0,*) "========= Write out RBF table ================="
  call to_history("n1",2*trad+1)
  call to_history("n2",2*trad+1)
  call to_history("n3",2*trad+1)
  call to_history("label1","x")
  call to_history("label2","z")
  call to_history("label3","y")
  call sep_write(rbftable)

  !-----------------------------------------------------------------------------
  call DATE_AND_TIME(values=timer1)
  timer0 = timer1-timer0
  timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
  write(0,*) "timer sum (sec): ", timer_sum/1000
  if (verbose) write(0,*) "RBF_BUIL_TABLE program complete"
  deallocate(rbftable)
end program