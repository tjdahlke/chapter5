!===============================================================================
!		Applies the Radial Basis function
!		MODEL:  Nrbf
!		DATA:  	Nz x Ny X Nx
!===============================================================================

program APPLY_RBF3d
	use sep
	use rbf_mod3d
	implicit none

	integer								:: nrbf,nz,ny,nx,trad
	real,dimension(:,:,:),allocatable	:: data,rbftable
	real,dimension(:,:),allocatable		:: rbfcoord
	real,dimension(:),allocatable		:: model
	logical								:: verbose,adjoint
	integer,dimension(8)               	:: timer0,timer1
	real								:: timer_sum=0.

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	call from_param("verbose",verbose,.TRUE.)
	if(verbose) write(0,*) "==================== Read in initial parameters"
	! call from_param("n1",nz)
	! call from_param("n2",ny)
	! call from_param("n3",nx,1)
	call from_param("n1r",nz)
	call from_param("n2r",ny)
	call from_param("n3r",nx,1)
	call from_param("adjoint",adjoint,.FALSE.)
	call from_param("trad",trad,15)
	call from_aux("rbfcoord","n1",nrbf)

	allocate(model(nrbf))
	allocate(data(nz,ny,nx))
	allocate(rbfcoord(nrbf,3))
	allocate(rbftable((2*trad+1),(2*trad+1),(2*trad+1)))

	if (adjoint) then
		if (verbose) write(0,*) "==================== Reading in RBF surface"
		call sep_read(data)
	else
		if (verbose) write(0,*) "==================== Reading in RBF weights"
		call sep_read(model)
	endif
	if (verbose) write(0,*) "==================== Reading in RBF coord and table"
	call sep_read(rbfcoord,"rbfcoord")
	call sep_read(rbftable,"rbftable")


!##########################################################################################
	if (verbose) write(0,*) "==================== Initializing RBF"
	call rbf_init(nz,ny,nx,nrbf,trad,rbfcoord,rbftable)
	if (verbose) write(0,*) "==================== Running RBF"
	call rbf(adjoint,.False.,model,data)
	if (verbose) write(0,*) "==================== sum(model) = ",sum(model)
	if (verbose) write(0,*) "==================== sum(data) = ",sum(data)
!##########################################################################################

	if (adjoint) then
		if(verbose) write(0,*) "==================== Write out RBF file (weights)"
		call to_history("n1",nrbf)
		call to_history("n2",1)
		call to_history("n3",1)
		call to_history("o2",0.0)
		call to_history("o3",0.0)
		call to_history("d2",1.0)
		call to_history("d3",1.0)
		call to_history("label1",'# sparse rbf centers')
		call sep_write(model)
	else
		if(verbose) write(0,*) "==================== Write out RBF file (implicit surface)"
		call to_history("n1",nz)
		call to_history("n2",ny)
		call to_history("n3",nx)
		call to_history("label1",'z [km]')
		call to_history("label2",'y [km]')
		call to_history("label3",'x [km]')
		call sep_write(data)
	end if

	if (verbose) write(0,*) "APPLY_RBF program complete"
	deallocate(data,model)

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec)",timer_sum/1000
	write(0,*) "timer sum (min)",timer_sum/1000/60
	write(0,*) "DONE!"

end program