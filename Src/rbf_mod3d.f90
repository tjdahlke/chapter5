!==============================================================
!	RADIAL BASIS FUNCTION MODULE 3D
!
!	7/7/2017,taylor@sep.stanford.edu
! 
! This code needs to be compiled AND linked with -qopenmp!!
! Otherwise it wont run correctly!!
!==============================================================


module rbf_mod3d
	use sep
	use mem_mod
	implicit none
	real,private								:: beta,eps,threshold
	integer,private								:: nz,nx,ny,nrbf,trad
	real,allocatable,dimension(:,:,:),private	:: temp1,temp3,m0,Hterm,phirbf
	REAL,PARAMETER 								:: Pi = 3.1415927
	real,dimension(:,:),pointer,private 		:: rbfcoord
	real,dimension(:,:,:),pointer,private 		:: masksalt,rbftable
	contains
	!==============================================================


	subroutine rbf_init(nz_in,nx_in,ny_in,nrbf_in,trad_in,rbfcoord_in,rbftable_in)
		integer							:: nz_in,nx_in,ny_in,nrbf_in,trad_in
		real,dimension(:,:),target		:: rbfcoord_in
		real,dimension(:,:,:),target	:: rbftable_in
		rbfcoord => rbfcoord_in
		rbftable => rbftable_in
		nrbf = nrbf_in
		nz = nz_in
		nx = nx_in
		ny = ny_in
		trad = trad_in
		allocate(temp1(nz,nx,ny))
		allocate(temp3(nz,nx,ny))
		call sep_init()
	end subroutine


	subroutine rbf_init_buildtable(trad_in,threshold_in,beta_in)
		integer				:: trad_in
		real				:: threshold_in,beta_in
		trad = trad_in
		threshold = threshold_in
		beta = beta_in
	end subroutine


	subroutine rbf_init_mask(masksalt_in)
		real,dimension(:,:,:),target	:: masksalt_in
		masksalt => masksalt_in
	end subroutine



	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Build the RBF gaussian curve template ---------------------
	subroutine rbftable_build(rbftable)
		real,dimension(:,:,:)	:: rbftable
		real					:: radius,yy,xx,zz
		integer					:: ix,iz,iy
		!!!race_cond_present!$omp parallel do
		do iy=1,(2*trad+1)
			yy = (-trad+iy-1)**2 
			do ix=1,(2*trad+1)
				xx = (-trad+ix-1)**2 
				do iz=1,(2*trad+1)
					zz = (-trad+iz-1)**2
					radius = SQRT((zz + xx + yy))
					rbftable(iz,ix,iy) = EXP(-((beta*radius)**2))
				end do
			end do
		end do
		!!!race_cond_present!$omp end parallel do
	end subroutine





	subroutine rbf_kernel(cz,cx,cy,left1,left2,right1,right2,near1,near2,top1,top2,bot1,bot2,far1,far2)
	    implicit none
		integer,intent(in)	:: cz,cx,cy
		integer,intent(out)	:: left1,left2,right1,right2,near1,near2
		integer,intent(out)	:: top1,top2,bot1,bot2,far1,far2

		left1 = MAXVAL((/1,cx-trad/))
		right1 = MINVAL((/nx,cx+trad/))
		top1 = MAXVAL((/1,cz-trad/))
		bot1 = MINVAL((/nz,cz+trad/))
		near1 = MAXVAL((/1,cy-trad/))
		far1 = MINVAL((/ny,cy+trad/))

		left2 = MAXVAL((/1,trad+2-cx/))
		right2 = MINVAL((/2*trad+1,nx-cx+trad+1/))
		top2 = MAXVAL((/1,trad+2-cz/))
		bot2 = MINVAL((/2*trad+1,nz-cz+trad+1/))
		near2 = MAXVAL((/1,trad+2-cy/))
		far2 = MINVAL((/2*trad+1,ny-cy+trad+1/))

	end subroutine






	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Make full RBF (apply weights to kernels) --------------------------------
	subroutine rbf(adj,add,model,data)
		real,dimension(:)		:: model
		real,dimension(:,:,:)	:: data
		integer					:: cz,cx,cy,ixx,izz,iyy,ir,qq
		logical					:: adj,add
		integer					:: left1,left2,right1,right2,near1,near2
		integer					:: top1,top2,bot1,bot2,far1,far2		
		! integer 				:: NTHREADS, TID, OMP_GET_NUM_THREADS, OMP_GET_THREAD_NUM


		temp1=0.0
		if (.not. add) then
			if (adj) then
				model=0.0
			else
				data=0.0
			endif
		endif


		if (adj) then
			!!!race_cond_present!$omp parallel do private(cz,cx,cy,left1,left2,right1,right2,top1,top2,bot1,bot2,near1,near2,far1,far2,iyy,ixx,izz)
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cx = rbfcoord(ir,2)
				cy = rbfcoord(ir,3)

				call rbf_kernel(cz,cx,cy,left1,left2,right1,right2,near1,near2,top1,top2,bot1,bot2,far1,far2)
				temp1(top1:bot1,left1:right1,near1:far1) = rbftable(top2:bot2,left2:right2,near2:far2)

! For efficiency maybe make two rbcoord lists; one with rbfs that are close to the edges; one with the rbfs from the inner area.
! UPDATE ON THE ABOVE IDEA:	Prototyped it a little and found that it ran SLOWER (46 sec vs ~20 sec!)
! Also, print statements add a lot of seconds to the runtime!
! Also, keeping the triple loop below in its present form seems to be the fastest option. (Tried SUM(); was slower)

				do iyy=near1,far1
					do ixx=left1,right1
						do izz=top1,bot1
							model(ir) = model(ir) + data(izz,ixx,iyy)*temp1(izz,ixx,iyy)
						end do
					end do
				end do

			end do
			!!!race_cond_present!$omp end parallel do
		else
			!!!race_cond_present!$omp parallel do private(cz,cx,cy,left1,left2,right1,right2,top1,top2,bot1,bot2,near1,near2,far1,far2,iyy,ixx,izz)
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cx = rbfcoord(ir,2)
				cy = rbfcoord(ir,3)

				call rbf_kernel(cz,cx,cy,left1,left2,right1,right2,near1,near2,top1,top2,bot1,bot2,far1,far2)
				temp1(top1:bot1,left1:right1,near1:far1) = rbftable(top2:bot2,left2:right2,near2:far2)

				do iyy=near1,far1
					do ixx=left1,right1
						do izz=top1,bot1
							data(izz,ixx,iyy) = data(izz,ixx,iyy) + model(ir)*temp1(izz,ixx,iyy)
						end do
					end do
				end do

			end do
			!!!race_cond_present!$omp end parallel do
		end if


	end subroutine





	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Apply Linearized derivative of Heaviside of RBF  ------------------------
	subroutine L_Hrbf(adj,add,model,data)
		real,dimension(:)		:: model
		real,dimension(:,:,:)	:: data
		integer					:: cz,cx,cy,ixx,izz,iyy,ir
		logical					:: adj,add
		integer,dimension(8)	:: timer0,timer1,timer2,val
		real 					:: timer_sum1=0.,timer_sum2=0.
		integer					:: left1,left2,right1,right2,near1,near2
		integer					:: top1,top2,bot1,bot2,far1,far2

		temp1=0.0
		if (.not. add) then
			if (adj) then
				model=0.0
			else
				data=0.0
			endif
		endif

! Possible to loop over the sparse mask space and make quicker?

		if (adj) then
			!!!race_cond_present!$omp parallel do private(cz,cx,cy,left1,left2,right1,right2,top1,top2,bot1,bot2,near1,near2,far1,far2,iyy,ixx,izz)
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cx = rbfcoord(ir,2)
				cy = rbfcoord(ir,3)

				call rbf_kernel(cz,cx,cy,left1,left2,right1,right2,near1,near2,top1,top2,bot1,bot2,far1,far2)
				temp1(top1:bot1,left1:right1,near1:far1) = rbftable(top2:bot2,left2:right2,near2:far2)

				do iyy=near1,far1
					do ixx=left1,right1
						do izz=top1,bot1
							model(ir) = model(ir) + data(izz,ixx,iyy)*temp1(izz,ixx,iyy)*masksalt(izz,ixx,iyy)
						end do
					end do
				end do

			end do
			!!!race_cond_present!$omp end parallel do
		else
			!!!race_cond_present!$omp parallel do private(cz,cx,cy,left1,left2,right1,right2,top1,top2,bot1,bot2,near1,near2,far1,far2,iyy,ixx,izz)
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cx = rbfcoord(ir,2)
				cy = rbfcoord(ir,3)

				call rbf_kernel(cz,cx,cy,left1,left2,right1,right2,near1,near2,top1,top2,bot1,bot2,far1,far2)
				temp1(top1:bot1,left1:right1,near1:far1) = rbftable(top2:bot2,left2:right2,near2:far2)

				do iyy=near1,far1
					do ixx=left1,right1
						do izz=top1,bot1
							data(izz,ixx,iyy) = data(izz,ixx,iyy) + model(ir)*temp1(izz,ixx,iyy)*masksalt(izz,ixx,iyy)
						end do
					end do
				end do

			end do
			!!!race_cond_present!$omp end parallel do
		end if

	end subroutine





	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Apply Heaviside to RBF (Non-linear forward) -----------------------------
	subroutine Hrbf(model,data)
		real,dimension(:)		:: model
		real,dimension(:,:,:)	:: data
		integer					:: izz, ixx, iyy
		call rbf(.False.,.False.,model,temp3)

		data=0.0
		! For case where nothing is salt (start of NL inversion). 
		! Otherwise there is no salt "seed" for the linearization step
		if (sum(temp3)==0.0) then
			write(0,*) "~~~~~~~~~~~~~  Using >=0.0  ~~~~~~~~~~~~~~~~"
			data = 1.0
		else
			write(0,*) "~~~~~~~~~~~~~  Using >0.0  ~~~~~~~~~~~~~~~~"
			data=0.0
			!!!race_cond_present!$omp parallel do
			do iyy=1,ny
				! write(0,*) "percent done = ", 100.0*iyy/ny
				do ixx=1,nx
					do izz=1,nz
						if (temp3(izz,ixx,iyy)>0.0) data(izz,ixx,iyy) = 1.0
					end do
				end do
			end do
			!!!race_cond_present!$omp end parallel do	
		endif

		if (exist_file('test')) then
			call to_history("n1",nz,'test')
			call to_history("o1",0,'test')
			call to_history("d1",1,'test')
			call to_history("n2",nx,'test')
			call to_history("o2",0,'test')
			call to_history("d2",1,'test')
			call to_history("n3",ny,'test')
			call to_history("o3",0,'test')
			call to_history("d3",1,'test')
			call sep_write(temp3,'test')
		endif

	end subroutine



end module