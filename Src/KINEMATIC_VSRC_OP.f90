program KINEMATIC_VSRC_OP

  ! Program for calculating the kinematic virtual source as described by Xukai Shen in his thesis from 2015.
  !
  ! Inputs:
  !   1) history file of input data (same size as syntraces); standard input
  !   2) history file of syntraces

  ! Outputs:
  !   1) history file of virtual source data; standard output
  !
  ! Taylor Dahlke, October 2018
  !
  !VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV


  !=================================
  ! Standard declarations
  !=================================
  use sep
  implicit none


  !=================================
  ! Declare variables
  !=================================
  ! Program variables
  integer                                       :: ii,ntraces,nt
  double precision                              :: synnorm, min_synnorm
  double precision, dimension(:),allocatable    :: syntraceOD,modelD,dataD

  real                                          :: ot,threshold
  real, dimension(:),allocatable                :: syntraceO,model,data
  logical                                       :: adjoint,verbose
  call sep_init()

  ! History file parameters
  call from_history('n1',nt)
  call from_history('n2',ntraces)

  call from_aux("syntraces","o1",ot)
  call from_param("adjoint",adjoint,.False.)
  call from_param("verbose",verbose,.False.)
  call to_history('o1',ot)

  ! Make arrays
  allocate(syntraceO(nt))
  allocate(model(nt))
  allocate(data(nt))
  allocate(syntraceOD(nt))
  allocate(modelD(nt))
  allocate(dataD(nt))

!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

! THIS IS HARD-CODED!!!!!

! THIS NEEDS TO BE ADJUSTED BASED ON THE INPUT DATA!!!!
  threshold=0.000000000001
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

min_synnorm=1000000000
  ! Make virtual source traces
  if (adjoint) then
      do ii=0,ntraces

          ! Zero the traces
          data=0.0
          dataD=0.0
          model=0.0
          modelD=0.0
          syntraceOD=0.0

          ! Read the traces
          if (verbose) write(0,*) "percent done: ", 100*ii/ntraces
          call sep_read(syntraceO,"syntraces")
          call sep_read(data)
          dataD=data
          syntraceOD=syntraceO

          ! Find trace norms
          synnorm=SQRT(DOT_PRODUCT(syntraceOD,syntraceOD))


          ! ! USE THIS TO FIND A ROUGH IDEA OF WHAT THRESHOLD SHOULD BE
          ! if ((synnorm**3) < min_synnorm) then
          !   min_synnorm = synnorm**3
          ! end if
          ! write(0,*) "min_synnorm = ", min_synnorm
          ! write(0,*) "-----------------------------------------"



          if ((synnorm**3)<threshold) then
              model=0.0
              call sep_write(model)
              ! Dont bother doing the rest in this case
              cycle
          end if

          if (verbose) then
            if (SQRT(DOT_PRODUCT(data,data))/=0.0) then
                write(0,*) "========================================================="
                write(0,*) "trace: ", ii
                ! write(0,*) "maxval(dataD) = ", maxval(dataD)
                ! write(0,*) "minval(dataD) = ", minval(dataD)
                write(0,*) "maxval(syntraceOD) = ", maxval(syntraceOD)
                write(0,*) "minval(syntraceOD) = ", minval(syntraceOD)
                write(0,*) "(synnorm**3) = ", (synnorm**3)
                write(0,*) "dataNorm =", SQRT(DOT_PRODUCT(data,data))
                write(0,*) "DOT_PRODUCT(dataD,syntraceOD) = ", DOT_PRODUCT(dataD,syntraceOD)
                write(0,*) "(DOT_PRODUCT(dataD,syntraceOD)/(synnorm**3)) = ", (DOT_PRODUCT(dataD,syntraceOD)/(synnorm**3))
            end if
          end if

          ! Calculate the virtual source
          modelD = modelD - (DOT_PRODUCT(dataD,syntraceOD)/(synnorm**3))*syntraceOD
          modelD = modelD + (dataD/synnorm) 

          ! Write the output trace
          model=modelD
          call sep_write(model)

      end do
  else
      do ii=0,ntraces

          ! Read the traces
          data=0.0
          dataD=0.0
          model=0.0
          modelD=0.0
          syntraceOD=0.0
          if (verbose) write(0,*) "percent done: ", 100*ii/ntraces
          call sep_read(syntraceO,"syntraces")
          call sep_read(model)
          modelD=model
          syntraceOD=syntraceO

          ! Find trace norms
          synnorm=SQRT(DOT_PRODUCT(syntraceOD,syntraceOD))

          if ((synnorm**3)<threshold) then
              data=0.0
              call sep_write(data)
              ! Dont bother doing the rest in this case
              cycle
          end if

          if (verbose) then
            if (SQRT(DOT_PRODUCT(model,model))/=0.0) then
                write(0,*) "========================================================="
                write(0,*) "trace: ", ii
                ! write(0,*) "maxval(dataD) = ", maxval(dataD)
                ! write(0,*) "minval(dataD) = ", minval(dataD)
                write(0,*) "maxval(syntraceOD) = ", maxval(syntraceOD)
                write(0,*) "minval(syntraceOD) = ", minval(syntraceOD)
                write(0,*) "(synnorm**3) = ", (synnorm**3)
                write(0,*) "modelNorm =", SQRT(DOT_PRODUCT(model,model))
                write(0,*) "DOT_PRODUCT(modelD,syntraceOD) = ", DOT_PRODUCT(modelD,syntraceOD)
                write(0,*) "(DOT_PRODUCT(modelD,syntraceOD)/(synnorm**3)) = ", (DOT_PRODUCT(modelD,syntraceOD)/(synnorm**3))
            end if
          end if

          ! Calculate the virtual source
          dataD = dataD + (modelD/synnorm) 
          dataD = dataD - (DOT_PRODUCT(modelD,syntraceOD)/(synnorm**3))*syntraceOD

          ! Write the output trace
          data=dataD
          call sep_write(data)

      end do
  end if



  write(0,*) " DONE!"

end program




