
!===============================================================================
!		Applies the clipping
!===============================================================================

program CLIP
use sep
implicit none

	integer										:: n1, n2, n3
	logical										:: verbose, replacelessthan, range
	real										:: clipval,replaceval, lowR, highR
	real, dimension(:,:), allocatable			:: input2d
	real, dimension(:,:,:), allocatable			:: input3d

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("clipval",clipval,1.5)
	call from_param("replacelessthan",replacelessthan,.true.)
	call from_param("replaceval",replaceval,clipval)
	call from_param("range",range,.false.)

	call from_history("n1",n1)
	call from_history("n2",n2)
	call from_history("n3",n3,1)

	if(verbose) write(0,*) "replaceval = ", replaceval
	if(verbose) write(0,*) "clipval = ", clipval
	if(verbose) write(0,*) "replacelessthan = ", replacelessthan
	if(verbose) write(0,*) "range = ", range

	if (range) then
		call from_param("lowR",lowR)
		call from_param("highR",highR)
		if(verbose) write(0,*) "lowR  = ", lowR
		if(verbose) write(0,*) "highR = ", highR
	end if



	if (n3==1) then
		if(verbose) write(0,*) "========= Detected 2D input ==============="
		allocate(input2d(n1, n2))
		call sep_read(input2d)

		if (range) then
			WHERE ((lowR<input2d) .and. (input2d<highR)) input2d = replaceval
		else
			if (replacelessthan) then
				WHERE (input2d <=clipval) input2d = replaceval
			else
				WHERE (input2d >clipval) input2d = replaceval
			end if
		end if
		if(verbose) write(0,*) "maxval(input2d) = ", maxval(input2d)
		call sep_write(input2d)
	else
		if(verbose) write(0,*) "========= Detected 3D input ==============="
		allocate(input3d(n1, n2, n3))
		call sep_read(input3d)

		if (range) then
			WHERE ((lowR<input3d) .and. (input3d<highR)) input3d = replaceval
		else
			if (replacelessthan) then
				if(verbose) write(0,*) "got here 1"
				if(verbose) write(0,*) "replaceval = ", replaceval
				if(verbose) write(0,*) "b4: maxval(input3d) = ", maxval(input3d)
				if(verbose) write(0,*) "b4: minval(input3d) = ", minval(input3d)
				WHERE (input3d < clipval) input3d = replaceval
			else
				if(verbose) write(0,*) "got here 2"
				if(verbose) write(0,*) "replaceval = ", replaceval
				if(verbose) write(0,*) "b4: maxval(input3d) = ", maxval(input3d)
				if(verbose) write(0,*) "b4: minval(input3d) = ", minval(input3d)
				WHERE (input3d > clipval) input3d = replaceval
			end if
		end if
		if(verbose) write(0,*) "maxval(input3d) = ", maxval(input3d)
		if(verbose) write(0,*) "minval(input3d) = ", minval(input3d)
		call sep_write(input3d)
	end if




end program
