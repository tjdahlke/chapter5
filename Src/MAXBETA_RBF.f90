program MAXBETA_RBF
use sep
use mem_mod
implicit none

real                               		:: upperlim, maxbetaval
integer									:: nz, ny, nx, nz2, ny2, nx2
logical									:: verbose
real, dimension(:,:,:), allocatable		:: phi_grad, phi

call sep_init()
call from_aux("phi","n1",nz)
call from_aux("phi","n2",ny)
call from_aux("phi","n3",nx,1)

call from_aux("phigrad","n1",nz2)
call from_aux("phigrad","n2",ny2)
call from_aux("phigrad","n3",nx2,1)

if ((nz.ne.nz2) .or. (ny.ne.ny2) .or. (nx.ne.nx2)) then
	write(0,*) "Dimensions of 'phi' and 'phigrad' do not match."
end if

call from_param("upperlim", upperlim)
call from_param("verbose", verbose,.false.)

!---------------- Allocate ------------------------------
! if (verbose) write(0,*) " MAXBETA: Allocating"
allocate(phi(nz, ny, nx))
allocate(phi_grad(nz2, ny2, nx2))

!------------ Read in values ------------------------------
! if (verbose) write(0,*) " MAXBETA: Reading in arrays"
call sep_read(phi, "phi")
call sep_read(phi_grad, "phigrad")
maxbetaval=upperlim*MAXVAL(ABS(phi))/(MAXVAL(ABS(phi_grad)))
if (verbose) write(0,*) "(MAXVAL(ABS(phi_grad))) = ", (MAXVAL(ABS(phi_grad)))
if (verbose) write(0,*) "(MAXVAL(ABS(phi))) = ", (MAXVAL(ABS(phi)))
if (verbose) write(0,*) "maxbetaval = ", maxbetaval

! !(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((
!-----------  Write outputs ------------------------------------------------
OPEN(UNIT=1,FILE="maxbetaval.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
WRITE(UNIT=1, FMT=*) maxbetaval
CLOSE(UNIT=1)

end program
