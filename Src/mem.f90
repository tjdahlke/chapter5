module mem_mod

  implicit none
  integer,private                        :: gb0=0, gb1=0
  double precision,private               :: memsize=0.d0

  contains

!!---------------------------------------------------------- 

  subroutine mem_print()
    write(0,*) "Total CPU memory allocated in GB", real(memsize/1024./1024./1024.*4.)
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_add(n1)
    integer                               :: n1
    memsize = memsize + 1.d0*n1
    gb1 = int(memsize/1024./1024./1024.*4.)
    if(gb1 > gb0) call mem_print()
    gb0 = gb1
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_alloc1d(array, n1)
    integer                               :: n1
    real, dimension(:), allocatable       :: array
    if(.not. allocated(array)) then
      allocate(array(n1))
      memsize = memsize + 1.d0*n1
      gb1 = int(memsize/1024./1024./1024.*4.)
      if(gb1 > gb0) call mem_print()
      gb0 = gb1
    end if
    array = 0.
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_alloc2d(array, n1, n2)
    integer                            :: n1, n2
    real, dimension(:,:), allocatable  :: array
    if(.not. allocated(array)) then
      allocate(array(n1,n2))
      memsize = memsize + 1.d0*n1*n2
      gb1 = int(memsize/1024./1024./1024.*4.)
      if(gb1 > gb0) call mem_print()
      gb0 = gb1
    end if
    array = 0.
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_alloc3d(array, n1, n2, n3)
    integer                              :: n1, n2, n3
    real, dimension(:,:,:), allocatable  :: array
    if(.not. allocated(array)) then
      allocate(array(n1,n2,n3))
      memsize = memsize + 1.d0*n1*n2*n3
      gb1 = int(memsize/1024./1024./1024.*4.)
      if(gb1 > gb0) call mem_print()
      gb0 = gb1
    end if
    array = 0.
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_alloc4d(array, n1, n2, n3, n4)
    integer                                :: n1, n2, n3, n4
    real, dimension(:,:,:,:), allocatable  :: array
    if(.not. allocated(array)) then
      allocate(array(n1,n2,n3,n4))
      memsize = memsize + 1.d0*n1*n2*n3*n4
      gb1 = int(memsize/1024./1024./1024.*4.)
      if(gb1 > gb0) call mem_print()
      gb0 = gb1
    end if
    array = 0.
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_alloc5d(array, n1, n2, n3, n4, n5)
    integer                                  :: n1, n2, n3, n4, n5
    real, dimension(:,:,:,:,:), allocatable  :: array
    if(.not. allocated(array)) then
      allocate(array(n1,n2,n3,n4,n5))
      memsize = memsize + 1.d0*n1*n2*n3*n4*n5
      gb1 = int(memsize/1024./1024./1024.*4.)
      if(gb1 > gb0) call mem_print()
      gb0 = gb1
    end if
    array = 0.
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_alloc6d(array, n1, n2, n3, n4, n5, n6)
    integer                                    :: n1, n2, n3, n4, n5, n6
    real, dimension(:,:,:,:,:,:), allocatable  :: array
    if(.not. allocated(array)) then
      allocate(array(n1,n2,n3,n4,n5,n6))
      memsize = memsize + 1.d0*n1*n2*n3*n4*n5*n6
      gb1 = int(memsize/1024./1024./1024.*4.)
      if(gb1 > gb0) call mem_print()
      gb0 = gb1
    end if
    array = 0.
  end subroutine

!!---------------------------------------------------------- 

  subroutine mem_alloc7d(array, n1, n2, n3, n4, n5, n6, n7)
    integer                                      :: n1, n2, n3, n4, n5, n6, n7
    real, dimension(:,:,:,:,:,:,:), allocatable  :: array
    if(.not. allocated(array)) then
      allocate(array(n1,n2,n3,n4,n5,n6,n7))
      memsize = memsize + 1.d0*n1*n2*n3*n4*n5*n6*n7
      gb1 = int(memsize/1024./1024./1024.*4.)
      if(gb1 > gb0) call mem_print()
      gb0 = gb1
    end if
    array = 0.
  end subroutine

!!---------------------------------------------------------- 

end module
