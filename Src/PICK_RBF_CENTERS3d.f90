!###############################################################################
!
!	PICK Radial Basis Fucntion Centers PROGRAM. This is done by choosing a
!	random cloud of Gaussian centers,with more clustering around the areas
!	of the boundaries.
!
!	input:  	implicit surface phi slope NZxNXxNY
!	msb:  		RBF center locations NZxNXxNY
!	centers:  	probability map used to place centers NZxNXxNY
!	output:		rbf center coordinates (in 3D, or 2D if nx==1)
!###############################################################################

program PICK_RBF_CENTERS
	use sep
	implicit none

	!===========================================================================
	!	Allocate and Initialize
	!---------------------------------------------------------------------------
	integer                             :: nz,nx,ny,ix,iz,iy
	integer								:: i,ic,ncenters
	real								:: dz,dx,oz,ox,maxprob,minprob,rr
	real,dimension(:,:),allocatable		:: rbfcenters
	real,dimension(:,:,:),allocatable	:: centers, msb
	integer,parameter 					:: seed = 86456
	logical								:: verbose
	integer,dimension(8)               	:: timer0,timer1,timeseg
	real                                :: timer_sum=0.
	!===============================================================================

	call sep_init()
	call DATE_AND_TIME(values=timer0)
	call srand(seed)
	call from_history('n1',nz)
	call from_history('n2',ny)
	call from_history('n3',nx,1)

	call from_param('maxprob',maxprob,0.075)
	call from_param('minprob',minprob,0.005)
	call from_param('verbose',verbose,.False.)

	! Allocate
	allocate(msb(nz,ny,nx))
	allocate(centers(nz,ny,nx))
	centers=0.0

	! Initialize
	call sep_read(msb)


	call DATE_AND_TIME(values=timer1)
	timer_sum=0.0
	timeseg = timer1-timer0
	timer_sum = timer_sum + (((timeseg(3)*24+timeseg(5))*60+timeseg(6))*60+timeseg(7))*1000+timeseg(8)
	write(0,*) "Initialization time: ",timer_sum/1000
	!---------------------------------------------

	! Normalize upper and lower bounds to some max/min probabilities
	msb = (msb - minval(msb)) 
	msb = msb/maxval(msb)*(maxprob-minprob) + minprob

	! Randomly place RBF centers
	!$omp parallel do
	do ix=1,nx
		do iy=1,ny
			do iz=1,nz
				CALL RANDOM_NUMBER(rr)
				if(rr<msb(iz,iy,ix)) then
					centers(iz,iy,ix)=1
				endif
			enddo
		enddo
	enddo
	!$omp end parallel do
	ncenters=sum(centers)
	allocate(rbfcenters(ncenters,3))


	call DATE_AND_TIME(values=timer1)
	timer_sum=0.0
	timeseg = timer1-timer0
	timer_sum = timer_sum + (((timeseg(3)*24+timeseg(5))*60+timeseg(6))*60+timeseg(7))*1000+timeseg(8)
	write(0,*) "RBF centers time: ",timer_sum/1000

	! Build sparse rbf center array
	ic=1
	do ix=1,nx
		do iy=1,ny
			do iz=1,nz
				if(centers(iz,iy,ix)>0.0) then
					rbfcenters(ic,1) = iz
					rbfcenters(ic,2) = iy
					rbfcenters(ic,3) = ix
					ic = ic + 1
				endif
			enddo
		enddo
	enddo

	call DATE_AND_TIME(values=timer1)
	timer_sum=0.0
	timeseg = timer1-timer0
	timer_sum = timer_sum + (((timeseg(3)*24+timeseg(5))*60+timeseg(6))*60+timeseg(7))*1000+timeseg(8)
	write(0,*) "RBF sparse time: ",timer_sum/1000



	if (nx==1) then
		! Write outputs for 2D case
		if (exist_file('msb')) then
			call to_history("n1"    ,nz			,'msb')
			call to_history("n2"    ,ny			,'msb')
			call to_history("label1",'z [km]'	,'msb')
			call to_history("label2",'y [km]'	,'msb')
			call sep_write(msb(:,:,1),"msb")
		end if

		if (exist_file('centers')) then
			call to_history("n1"    ,nz			,'centers')
			call to_history("n2"    ,ny			,'centers')
			call to_history("label1",'z [km]'	,'centers')
			call to_history("label2",'y [km]'	,'centers')
			call sep_write(centers(:,:,1),"centers")
		end if

		call to_history("n1"    ,ncenters)
		call to_history("n2"    ,2)
		call to_history("n3"    ,1)
		call to_history("o1"    ,0)
		call to_history("o2"    ,0)
		call to_history("o3"    ,0)
		call to_history("d1"    ,1)
		call to_history("d2"    ,1)
		call to_history("d3"    ,1)
		call to_history("label1",'# rbf centers')
		call to_history("label2",'z,y coord')
		call sep_write(rbfcenters)
	else
		! Write outputs for 3D case
		if (exist_file('msb')) then
			call to_history("n1"    ,nz			,'msb')
			call to_history("n2"    ,ny			,'msb')
			call to_history("n3"    ,nx			,'msb')
			call to_history("label1",'z [km]'	,'msb')
			call to_history("label2",'y [km]'	,'msb')
			call to_history("label3",'x [km]'	,'msb')
			call sep_write(msb,"msb")
		end if

		if (exist_file('centers')) then
			call to_history("n1"    ,nz			,'centers')
			call to_history("n2"    ,ny			,'centers')
			call to_history("n3"    ,nx			,'centers')
			call to_history("label1",'z [km]'	,'centers')
			call to_history("label2",'y [km]'	,'centers')
			call to_history("label3",'x [km]'	,'centers')
			call sep_write(centers,"centers")
		end if

		call to_history("n1"    ,ncenters)
		call to_history("n2"    ,3)
		call to_history("n3"    ,1)
		call to_history("o1"    ,0)
		call to_history("o2"    ,0)
		call to_history("o3"    ,0)
		call to_history("d1"    ,1)
		call to_history("d2"    ,1)
		call to_history("d3"    ,1)
		call to_history("label1",'# rbf centers')
		call to_history("label2",'z,y,x coord')
		call sep_write(rbfcenters)
	end if

	call DATE_AND_TIME(values=timer1)
	timer_sum=0.0
	timeseg = timer1-timer0
	timer_sum = timer_sum + (((timeseg(3)*24+timeseg(5))*60+timeseg(6))*60+timeseg(7))*1000+timeseg(8)
	write(0,*) "write outputs time: ",timer_sum/1000


	if(verbose) write(0,*) "============================================"
	if(verbose) write(0,*) " Percent reduction = ",100.-(100.*ncenters/(nz*nx*ny))
	if(verbose) write(0,*) "============================================"

end program