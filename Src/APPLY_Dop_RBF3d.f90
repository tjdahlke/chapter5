!===============================================================================
!		Applies the derivative with respect to the velocity model: M(phi)
!		MODEL:  NP	(perturbations in phi, sparsely represented)
!		DATA:   NZxNYxNX	(velocity model perturbation)
!
!		(Just uses phi parameter, not b!)
!===============================================================================

program APPLY_Dop_RBF3d
	use sep
	use rbf_mod3d
	implicit none

	integer								:: nrbf,nz,nx,ny,nz2,nx2,ny2,trad
	real,dimension(:,:,:),allocatable	:: data,rbftable,scaling,masksalt,temp
	real,dimension(:,:),allocatable		:: rbfcoord
	real,dimension(:),allocatable		:: model
	logical								:: verbose,adjoint,add
	integer,dimension(8)               	:: timer0,timer1
	real								:: timer_sum=0.

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	call from_param("verbose",verbose,.TRUE.)
	if (verbose) write(0,*) "==================== Read in initial parameters"
	call from_param("n1r",nz)
	call from_param("n2r",nx)
	call from_param("n3r",ny,1)
	! call from_param("n1",nz)
	! call from_param("n2",nx)
	! call from_param("n3",ny,1)
	call from_param("adjoint",adjoint,.FALSE.)
	call from_param("trad",trad,15)
	call from_aux("rbfcoord","n1",nrbf)
	CALL from_param("add",add,.FALSE.)

	IF(verbose) WRITE(0,*) "========= Allocate ==================================="
	allocate(model(nrbf))
	allocate(data(nz,nx,ny))
	allocate(rbfcoord(nrbf,3))
	allocate(rbftable((2*trad+1),(2*trad+1),(2*trad+1)))
	ALLOCATE(scaling(nz,nx,ny))
	ALLOCATE(masksalt(nz,nx,ny))
	ALLOCATE(temp(nz,nx,ny))

	IF(verbose) WRITE(0,*) "========= Read in masking, scaling, RBF coords =================="
	CALL sep_read(scaling,"scaling")
	CALL sep_read(masksalt,"masksalt")
	CALL sep_read(rbfcoord,"rbfcoord")
	CALL sep_read(rbftable,"rbftable")

	! Reading the inputs
	if (adjoint) then
		if (verbose) write(0,*) "==================== Reading in RBF surface"
		call from_history("n1",nz2)
		call from_history("n2",nx2)
		call from_history("n3",ny2)
		if ((nz2.ne.nz).or.(nx2.ne.nx).or.(ny2.ne.ny)) then
			write(0,*) "========================================================================================="
			write(0,*) "The input model dimensions dont match what the param file says. Did you change the size / pad the model for wave propagation?"
			stop
		end if
		call sep_read(data)
	else
		if (verbose) write(0,*) "==================== Reading in RBF weights"
		call sep_read(model)
	endif
	if (verbose) write(0,*) "==================== Reading in RBF coord and table"


!##########################################################################################
	if (verbose) write(0,*) "==================== Initializing RBF"
	call rbf_init(nz,nx,ny,nrbf,trad,rbfcoord,rbftable)
	CALL rbf_init_mask(masksalt)

	IF (adjoint) THEN
		!	deltaM -----> deltaPhiRBF (sparse)
		IF (.NOT. add) model=0.0
		temp = temp + data*scaling
		CALL L_Hrbf(.TRUE.,.FALSE.,model,temp)
	ELSE
		!	deltaPhiRBF (sparse) -----> deltaM
		IF (.NOT. add) data=0.0
		CALL L_Hrbf(.FALSE.,.FALSE.,model,temp)
		data = data + temp*scaling
	ENDIF


!##########################################################################################

	if (adjoint) then
		if(verbose) write(0,*) "==================== Write out RBF file (weights)"
		call to_history("n1",nrbf)
		call to_history("n2",1)
		call to_history("n3",1)
		call to_history("o2",0.0)
		call to_history("o3",0.0)
		call to_history("d2",1.0)
		call to_history("d3",1.0)
		call to_history("label1",'# sparse rbf centers')
		call sep_write(model)
	else
		if(verbose) write(0,*) "==================== Write out RBF file (implicit surface)"
		call to_history("n1",nz)
		call to_history("n2",nx)
		call to_history("n3",ny)
		call to_history("label1",'z [km]')
		call to_history("label2",'x [km]')
		call to_history("label2",'y [km]')
		call sep_write(data)
	end if

	if (verbose) write(0,*) "APPLY_RBF program complete"
	deallocate(data,model,masksalt,scaling,temp)

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec)",timer_sum/1000
	write(0,*) "timer sum (min)",timer_sum/1000/60
	write(0,*) "DONE!"
	call sep_close()

end program