!====================================================================================
!		Non-linear Application of the Heaviside function to a Radial Basis function
!
!		H(RBF(z))
!
!		MODEL:  Nrbf x 1
!		DATA:  	Nz x Nx X Ny
!====================================================================================

program NONLINEAR_RBF_PHI3d
	use sep
	use rbf_mod3d
	implicit none

	integer								:: nz,nx,ny,nrbf,trad
	real								:: timer_sum=0.
	real,dimension(:),allocatable		:: model
	real,dimension(:,:),allocatable		:: rbfcoord
	real,dimension(:,:,:),allocatable	:: data,rbftable
	logical								:: verbose
	integer,dimension(8)               	:: timer0,timer1

	call sep_init()
	call DATE_AND_TIME(values=timer0)
	call from_param("verbose",verbose,.TRUE.)
	call from_param("trad",trad,15)
	call from_aux("rbfcoord","n1",nrbf)

	if(verbose) write(0,*) "==================== Read in initial parameters"
	call from_param("n1",nz)
	call from_param("n2",nx)
	call from_param("n3",ny)

	allocate(model(nrbf))
	allocate(data(nz,nx,ny))
	allocate(rbfcoord(nrbf,3))
	allocate(rbftable((2*trad+1),(2*trad+1),(2*trad+1)))

	if (verbose) write(0,*) "==================== Reading in RBF weights perturbation"
	call sep_read(model)

	if (verbose) write(0,*) "==================== Reading in RBF center coordinates"
	call sep_read(rbfcoord,"rbfcoord")

	if (verbose) write(0,*) "==================== Reading in RBF table"
	call sep_read(rbftable,"rbftable")

	!##########################################################################################
	call rbf_init(nz,nx,ny,nrbf,trad,rbfcoord,rbftable)
	if (verbose) write(0,*) "==================== Applying Heaviside function"
	call Hrbf(model,data)
	data=2*data-1 ! To create a implicit surafce that is +1 in salt and -1 outside salt
	!##########################################################################################

	if(verbose) write(0,*) "==================== Write out RBF file (surface perturbation)"
	call to_history("n1",nz)
	call to_history("n2",nx)
	call to_history("n3",ny)
	call to_history("label1",'z [km]')
	call to_history("label2",'x [km]')
	call to_history("label3",'y [km]')
	call sep_write(data)

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec): ",timer_sum/1000
	if (verbose) write(0,*) "LINEARIZED_RBF program complete"
	deallocate(data,model)

end program