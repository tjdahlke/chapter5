

#include "boost/multi_array.hpp"
#include "timer.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;



int main(int argc, char **argv){

        timer x=timer("TIME TO BUILD ABC WEIGHTS:");
        x.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> inp=io->getRegFile("in",usageIn);
        std::shared_ptr<hypercube> hyperVel=inp->getHyper();

        // Get modelspace parameters
        int padX=par->getInt("padX");
        int padY=par->getInt("padY");
        int padT=par->getInt("padT");
        int padB=par->getInt("padB");

        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];//+padT+padB;
        long long ny=velN[1];//+2*padY;
        long long nx=velN[2];//+2*padX;
        float dz = hyperVel->getAxis(1).d;
        float dy = hyperVel->getAxis(2).d;
        float dx = hyperVel->getAxis(3).d;
        float oz = hyperVel->getAxis(1).o;
        float oy = hyperVel->getAxis(2).o;
        float ox = hyperVel->getAxis(3).o;
        float ds = dx;

        // Get ABC parameters
        int bcX=padX;
        int bcY=padY;
        int bcZT=padT;
        int bcZB=padB;
        float alpha=par->getFloat("alpha",0.15);
        float dt=par->getFloat("dt",0.002);

        fprintf(stderr, "ds=%f\n",ds);
        fprintf(stderr, "dt=%f\n",dt);
        fprintf(stderr, "alpha=%f\n",alpha);

        // ABC DAMPENING OUTPUT
        SEP::axis axZ(nz,oz,dz);
        SEP::axis axY(ny,oy,dy);
        SEP::axis axX(nx,ox,dx);
        std::shared_ptr<hypercube> hyperOut3dABC(new hypercube(axZ,axY,axX));
        std::shared_ptr<float3DReg> abc(new float3DReg(hyperOut3dABC));
        std::shared_ptr<genericRegFile> outABC=io->getRegFile("out",usageOut);
        outABC->setHyper(hyperOut3dABC);
        outABC->writeDescription();
        fprintf(stderr,">>>>> Done setting up the ABC dampening output \n");


        //========  MAKE ABC WEIGHTS ========================================
        inp->readFloatStream(abc->getVals(),hyperVel->getN123());

        float xx,zz,yy,rr;
        // Assign the exponential weights to the ABC dampening
        for(int iy=0; iy < ny; iy++) {
                // Y dampening boundaries
                yy = 0.0;
                if (iy <= bcY){
                        yy = (iy-bcY)/(float(bcY));
                }
                else if ((ny-bcY) <= iy){
                        yy = (iy-ny+bcY)/(float(bcY));
                }
                for(int ix=0; ix < nx; ix++) {
                        // X dampening boundaries
                        if (bcX!=0.0){
                                xx = 0.0;
                                if (ix <= bcX){
                                        xx = (ix-bcX)/(float(bcX));
                                }
                                else if ((nx-bcX) <= ix){
                                        xx = (ix-nx+bcX)/(float(bcX));
                                }
                        }
                        for(int iz=0; iz < nz; iz++) {
                                // Z dampening boundaries
                                zz = 0.0;
                                if(bcZT!=0.0){
                                        if (iz <= bcZT){
                                                zz = (iz-bcZT)/(float(bcZT));
                                        }
                                }
                                if ((nz-bcZB) <= iz){
                                        zz = (iz-nz+bcZB)/(float(bcZB));
                                }
                                // Calculate the radius
                                rr=sqrt(zz*zz+xx*xx+yy*yy);
                                (*abc->_mat)[ix][iy][iz] = (*abc->_mat)[ix][iy][iz]*alpha*dt*rr/ds;
                        }
                }
        }


        // =============================================================================
        // Write the outputs to SEP files
        outABC->writeFloatStream(abc->getVals(),hyperOut3dABC->getN123());  // ABC weights

        x.stop();
        x.print();

}
