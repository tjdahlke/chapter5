
//==============================================================================================
//
//        3D WAVE PROPAGATION CODE
//        Taylor Dahlke, taylor@sep.stanford.edu, 10/23/2017
//
//      This program solves the acoustic wave equation with constant density. It uses 
//      TBB vectorization for the inner loop to speed things up. I follow the formulation
//      described in the paper:
//
//      "Accurate implementation of two-way wave-equation operators", Ali Almomim
//
//      Absorbing boundary conditions are simple dampening in a padding region around the model.
//
//      FORWARD OPERATOR:  
//         wavelet (NT x 1) ---->>  shot gather (NT x NREC)
//
//      ADJOINT OPERATOR:  
//         shot gather (NT x NREC) ---->>  wavelet (NT x 1)
//
//==============================================================================================

#include "boost/multi_array.hpp"
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "timer.h"
#include "laplacianOP3d.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <float1DReg.h>
#include <ioModes.h>
#include <math.h>
using namespace SEP;
using namespace giee;
using namespace std;

typedef boost::multi_array<float, 3> float3D;

class block {
public:
block(int b1,int e1,int b2,int e2,int b3,int e3) : _b1(b1),_e1(e1),_b2(b2),_e2(e2),_b3(b3),_e3(e3){
};
int _b1,_e1,_b2,_e2,_b3,_e3;
};



int main(int argc, char **argv){

        if (argc==1){
                cout << "==========================================================================\n\n" << endl;
                cout << "==========================================================================\n\n" << endl;
                cout << "NAME:   main3d.x  --- 3D acoustic wave propagation operator" << endl; 
                cout << "SYNOPSIS:   main3d.x  par=  abcs=abcs.H  < in.H > out.H" << endl;
                cout << "DESCRIPTION:  This program runs 3D acoustic wave propagation with constant density. It uses TBB vectorization for the inner loop to speed things up. I follow the formulation described in the paper: 'Accurate implementation of two-way wave-equation operators', Ali Almomim. Absorbing boundary conditions are simple dampening in a padding region around the model.\n\n" << endl;
                cout << "FORWARD OPERATOR: "<< endl;
                cout << "       wavelet (NT x 1) ---->>  shot gather (NT x NREC)"<< endl;
                cout << "ADJOINT OPERATOR: "<< endl;
                cout << "       shot gather (NT x NREC) ---->>  wavelet (NT x 1)\n\n"<< endl;


                cout << "INPUT FILES: \n" << endl;                           
                cout << "vel: Three dimensional .H file (nz x nx x ny) that has the acosutic wavespeed in [m/s]. \n" << endl;                           
                cout << "rec: Two dimensional .H file of (8 x ntraces) that has header info about each trace in the shot (or receiver) gather. \n" << endl;                           
                cout << "abc: Three dimensional .H file (nz x nx x ny) that has the weights for the absorbing boundary conditions. \n" << endl;                           
                
                cout << "OPTIONAL OUTPUT FILES: \n" << endl;                           
                cout << "curW: Three dimensional .H file (nz x nx x ny) of the wavefield at time nt. \n" << endl;                           
                cout << "oldW: Three dimensional .H file (nz x nx x ny) of the wavefield at time nt-1. \n\n" << endl;                           

                cout << "INPUT PARAMETERS: \n" << endl;                           
                cout << "adj: [int] (1 or 0) Indicates whether the operator is being run as forward or adjoint.\n" << endl;                           
                cout << "souX: [int] (0 to nx-1) Indicates X index position of the source. \n" << endl;                           
                cout << "souY: [int] (0 to ny-1) Indicates Y index position of the source. \n" << endl;                           
                cout << "souZ: [int] (0 to nz-1) Indicates Z index position of the source. \n" << endl;                           
                cout << "d1: [float] Spatial sampling of each grid cell in meters. This code assumes dx = dz = dy. \n" << endl;                           
                cout << "dt: [float] Time sampling. \n" << endl;                           
                cout << "nt: [int] Number of time samples.\n" << endl;                           
                cout << "cfl: [int] (1 or 0) Indicates whether or not to check the CFL condition before running.\n" << endl;                           
                cout << "wavefield: [int] (1 or 0) Indicates whether or not to write out the receiver wavefield.\n" << endl;                           
                cout << "bs: [int] (1 to min(nx,nz,ny)) Block side used for TBB thread blocking.\n" << endl;                           
                cout << "rtm: [int] (0 to nx-1) Indicates whether to output the oldW and curW files. \n" << endl;                           
                cout << "useabcs: [int] (0 to nx-1) Indicates whether to use absorbing boundary conditions or not. \n\n\n" << endl;                           

                exit(0);
        }


        timer x=timer("FULL RUN");
        timer x1=timer("INITIALIZATION");
        x.start();
        x1.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> velin=io->getRegFile("vel",usageIn);
        std::shared_ptr<genericRegFile> rec=io->getRegFile("rec",usageIn);
        std::shared_ptr<hypercube> hyperVel=velin->getHyper();
        std::shared_ptr<hypercube> hyperRec=rec->getHyper();

        // Get runtime parameters
        int rtm=par->getInt("rtm",1);
        int cfl=par->getInt("cfl",1);
        int pad=par->getInt("pad",65);

        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long ny=velN[1];
        long long nx=velN[2];
        float dz = hyperVel->getAxis(1).d;
        float dy = hyperVel->getAxis(2).d;
        float dx = hyperVel->getAxis(3).d;
        float oz = hyperVel->getAxis(1).o;
        float oy = hyperVel->getAxis(2).o;
        float ox = hyperVel->getAxis(3).o;

        fprintf(stderr, "ox=%f\n", ox);
        fprintf(stderr, "oy=%f\n", oy);

        // Get acquisition parameters
        std::vector<int> recN=hyperRec->getNs();
        int Nkeys=recN[0];
        int Nrec=recN[1];

        fprintf(stderr, "Nkeys =%d\n", Nkeys);
        fprintf(stderr, "Nrec =%d\n", Nrec);

        // Get propagation parameters
        int nt=par->getInt("nt",100);
        float dsamp=par->getFloat("d1",25.0);
        float dt=par->getFloat("dt",0.002);
        SEP::axis axT(nt,0.0,dt);
        SEP::axis ax1(1,0.0,1.0);
        fprintf(stderr, "nt =%d\n", nt);
        fprintf(stderr, "dt input =%f\n", dt);
        fprintf(stderr, "dsamp =%f\n", dsamp);
        fprintf(stderr, "dt used =%f\n", dt);

        x1.stop();
        x1.print();
        x1.start();

        //==========================================================================
        int adj=par->getInt("adj",0);
        std::shared_ptr<genericRegFile> wav;
        std::shared_ptr<hypercube> hyperWav1(new hypercube(axT));
        std::shared_ptr<hypercube> hyperWav2(new hypercube(axT,ax1));
        std::shared_ptr<float1DReg> wavelet(new float1DReg(hyperWav1));
        if(adj){
                fprintf(stderr, "WAVELET OUTPUT\n" );
                // WAVELET OUTPUT
                wav=io->getRegFile("out",usageOut);
                wav->setHyper(hyperWav2);
                wav->writeDescription();}
        else{   // WAVELET INPUT
                fprintf(stderr, "WAVELET INPUT\n" );
                wav=io->getRegFile("in",usageIn);
                wav->readFloatStream(wavelet->getVals(),hyperWav1->getN123());    
        }
        x1.stop();
        x1.print();
        x1.start();

        //==========================================================================
        // Shot gather data
        SEP::axis axR(Nrec,0.0,1.0);
        std::shared_ptr<hypercube> hyperOut2d(new hypercube(axT,axR));
        std::shared_ptr<float2DReg> gather(new float2DReg(hyperOut2d));
        std::shared_ptr<genericRegFile> outSG;
        // Shot gather 3D header
        SEP::axis axK(Nkeys,0.0,1.0);
        std::shared_ptr<hypercube> hyperOut2dH(new hypercube(axK,axR));
        std::shared_ptr<float2DReg> gatherH(new float2DReg(hyperOut2dH));
        std::shared_ptr<genericRegFile> outSGH;
        if(adj){// SHOT GATHER INPUT
                fprintf(stderr, "SHOT GATHER INPUT\n" );
                outSG=io->getRegFile("in",usageIn);
                outSG->readFloatStream(gather->getVals(),hyperOut2d->getN123());    
        }
        else{   // SHOT GATHER OUTPUT
                fprintf(stderr, "SHOT GATHER OUTPUT\n" );
                outSG=io->getRegFile("out",usageOut);
                outSG->setHyper(hyperOut2d);
                outSG->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather output \n");
                fprintf(stderr, "SHOT GATHER 3D HEADER OUTPUT\n" );
                outSGH=io->getRegFile("header",usageOut);
                outSGH->setHyper(hyperOut2dH);
                outSGH->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather 3D spatial header output \n");
        }
        //==========================================================================
        x1.stop();
        x1.print();
        x1.start();

        // Setup the velocity model input
        SEP::axis ax1D(nx*ny*nz,0.0,1.0);
        std::shared_ptr<hypercube> hyperVel1d(new hypercube(ax1D));
        std::shared_ptr<float1DReg> velModel(new float1DReg(hyperVel1d));
        velin->readFloatStream(velModel->getVals(),hyperVel->getN123());

        // Setup the reciever position coordinates input
        std::shared_ptr<float2DReg> reccoor(new float2DReg(hyperRec));
        rec->readFloatStream(reccoor->getVals(),hyperRec->getN123());

        // Setup the absorbing boundary condition weights input
        std::shared_ptr<float3D> abcW2(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> abcW3(new float3D(boost::extents[nx][ny][nz]));
        int useabcs=par->getInt("useabcs",1);
        if(useabcs){
                fprintf(stderr, "Using ABC dampening \n");
                std::shared_ptr<genericRegFile> abc=io->getRegFile("abc",usageIn);
                std::shared_ptr<float1DReg> abcW(new float1DReg(hyperVel1d));
                abc->readFloatStream(abcW->getVals(),hyperVel1d->getN123());
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        abcW2->data()[ii] = (*abcW->_mat)[ii];
                        abcW3->data()[ii] = 1.0;
                }
        }
        else{
                fprintf(stderr, "Using no ABC dampening \n");
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        abcW3->data()[ii] = 1.0;
                }
        }
        fprintf(stderr,">>>>> Done reading ABC's \n");

        x1.stop();
        x1.print();
        x1.start();

        // Read in the velocity model
        std::shared_ptr<float3D> v2dt(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> vel(new float3D(boost::extents[nx][ny][nz]));
        if(cfl){
                float maxvel=(*velModel->_mat)[0];
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        vel->data()[ii] = ((*velModel->_mat)[ii]);
                        v2dt->data()[ii] = ((*velModel->_mat)[ii])*((*velModel->_mat)[ii])*dt*dt;
                        if ( (*velModel->_mat)[ii] > maxvel){
                                maxvel = (*velModel->_mat)[ii];
                        }
                }
                // Check the CFL condition
                float cfl=maxvel*dt/dsamp;
                float compval=sqrt(3.0)/3.0;
                if (cfl>=compval){
                        fprintf(stderr,">>>>> ERROR: %f > %f \n",cfl,compval);
                        fprintf(stderr,">>>>> maxvel=%f  dt=%f  dsamp=%f \n",maxvel,dt,dsamp);
                        fprintf(stderr,"Doesn't pass the CFL condition. Either decrease dt or increase dsamp\n");
                        exit (EXIT_FAILURE);
                }
                fprintf(stderr,">>>>> PASSED the CFL condition \n");
                fprintf(stderr,">>>>> maxvel = %f : CFL = %f \n", maxvel, cfl);
        }
        else{
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        v2dt->data()[ii] = ((*velModel->_mat)[ii])*((*velModel->_mat)[ii])*dt*dt;
                }
                fprintf(stderr,">>>>> Did not check the CFL condition \n");
        }
        fprintf(stderr,">>>>> Done reading velocity model \n");

        x1.stop();
        x1.print();
        x1.start();


        // WAVEFIELD OUTPUT
        int wavefield=par->getInt("wavefield",0);
        SEP::axis axZ(nz,oz,dz);
        SEP::axis axX(nx,ox,dx);
        SEP::axis axY(ny,oy,dy);
        std::shared_ptr<hypercube> hyperOut3d(new hypercube(axZ,axY,axT));
        std::shared_ptr<float3DReg> wave(new float3DReg(hyperOut3d));
        std::shared_ptr<genericRegFile> outWV;
        if (wavefield){
                outWV=io->getRegFile("wave",usageOut);
                outWV->setHyper(hyperOut3d);
                outWV->writeDescription();
                fprintf(stderr,">>>>> Done setting up the wavefield output \n");
        }



        std::shared_ptr<genericRegFile> outPRE;
        std::shared_ptr<genericRegFile> outCUR;
        std::shared_ptr<hypercube> hyperOutFRAME(new hypercube(axZ,axY,axX));
        if(rtm){
                // WAVEFIELD PREVIOUS FRAME OUTPUT
                outPRE=io->getRegFile("oldW",usageOut);
                outPRE->setHyper(hyperOutFRAME);
                outPRE->writeDescription();
                fprintf(stderr,">>>>> Done setting up the previous wavefield frame output \n");

                // WAVEFIELD CURRENT FRAME OUTPUT
                outCUR=io->getRegFile("curW",usageOut);
                outCUR->setHyper(hyperOutFRAME);
                outCUR->writeDescription();
                fprintf(stderr,">>>>> Done setting up the current wavefield frame output \n");
        }

        x1.stop();
        x1.print();
        x1.start();

        // ===============================================================================
        // Initialize 10th order spatial Laplacian FD coefficients and pressure fields
        fprintf(stderr,">>>>> Beginning other initializations \n");
        float coef[6];
        float sc=1.0/(dsamp*dsamp);

        coef[1]=sc*42000/(25200.0);
        coef[2]=-1.0*sc*6000/(25200.0);
        coef[3]=sc*1000/(25200.0);
        coef[4]=-1.0*sc*125/(25200.0);
        coef[5]=sc*8/(25200.0);
        coef[0]=-6*(coef[1]+coef[2]+coef[3]+coef[4]+coef[5]);

        std::shared_ptr<float3D> oldW(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curW(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> newW(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> newF(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> tmp(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curTemp1(new float3D(boost::extents[nx][ny][nz]));

        std::shared_ptr<float3D> curWX(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curWZ(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curWY(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curD(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> finalP(new float3D(boost::extents[nx][ny][nz]));

        x1.stop();
        x1.print();
        x1.start();


        fprintf(stderr,">>>>> Zeroing out the arrays \n");
        std::fill( oldW->data(), oldW->data() + oldW->num_elements(), 0 ); 
        std::fill( curW->data(), curW->data() + curW->num_elements(), 0 ); 
        std::fill( curTemp1->data(), curTemp1->data() + curTemp1->num_elements(), 0 ); 
        std::fill( newW->data(), newW->data() + newW->num_elements(), 0 ); 
        std::fill( newF->data(), newF->data() + newF->num_elements(), 0 ); 
        std::fill( tmp->data(), tmp->data() + tmp->num_elements(), 0 ); 

        x1.stop();
        x1.print();
        x1.start();

        // =====================================================================
        // Set blocksize
        int bs=par->getInt("bs",25);
        int bs1,bs2,bs3;
        bs1=bs;
        bs2=bs;
        bs3=bs;

        vector<int> b1(1,5),e1(1,5+bs1);
        int nleft=nz-10-bs1,i=0;
        while(nleft>0) {
                int bs=min(nleft,bs1);
                b1.push_back(e1[i]);
                e1.push_back(e1[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b2(1,5),e2(1,5+bs2);
        nleft=ny-10-bs2;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs2);
                b2.push_back(e2[i]);
                e2.push_back(e2[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b3(1,5),e3(1,5+bs3);
        nleft=nx-10-bs3;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs3);
                b3.push_back(e3[i]);
                e3.push_back(e3[i]+bs);
                ++i;
                nleft-=bs;
        }

        // Make the full propagation regions blocks
        vector<block> blocks;
        for(int i3=0; i3<b3.size(); ++i3) {
                for(int i2=0; i2<b2.size(); ++i2) {
                        for(int i1=0; i1<b1.size(); ++i1) {
                                blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                        }
                }
        }

        // Make ABC block regions
        vector<block> Z1blocks;
        vector<block> Z2blocks;
        vector<block> X1blocks;
        vector<block> X2blocks;
        vector<block> Y1blocks;
        vector<block> Y2blocks;
        for(int i3=0; i3<b3.size(); ++i3){
                for(int i2=0; i2<b2.size(); ++i2){
                        for(int i1=0; i1<b1.size(); ++i1){
                                // Z dimension blocks
                                if (b1[i1]<pad){
                                        Z1blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));                 
                                }
                                if (e1[i1]>(nz-pad)){
                                        Z2blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                                }
                                // Y dimension blocks (no overlap with Z blocks)
                                if ( (b2[i1]<pad)      and (b1[i1]>=pad) and (e1[i1]<=(nz-pad)) ){
                                        Y1blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));                 
                                }
                                if ( (e2[i1]>(ny-pad)) and (b1[i1]>=pad) and (e1[i1]<=(nz-pad)) ){
                                        Y2blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                                }
                                // X dimension blocks (no overlap with Z blocks)
                                if ( (b3[i1]<pad)      and (b1[i1]>=pad) and (e1[i1]<=(nz-pad)) ){
                                        X1blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));                 
                                }
                                if ( (e3[i1]>(nx-pad)) and (b1[i1]>=pad) and (e1[i1]<=(nz-pad)) ){
                                        X2blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                                }
                        }
                }
        }

        // Specify source position
        int isou=0;
        int souz=par->getInt("souZ");
        int souy=par->getInt("souY");
        int soux=par->getInt("souX");

        fprintf(stderr, "souz=%d\n",souz );
        fprintf(stderr, "souy=%d\n",souy );
        fprintf(stderr, "soux=%d\n",soux );

        // Print initialization time
        x1.stop();
        x1.print();

        //========  MAIN WAVEFIELD LOOP ========================================
        timer x2=timer("MAIN WAVEFIELD LOOP");
        x2.start();

        // int souyxz=souz+soux*nz+souy*nx*nz;
        int souyxz=souz+souy*nz+soux*ny*nz;
        float v2dti=-1.0*((*velModel->_mat)[souyxz])*((*velModel->_mat)[souyxz])*dt*dt;
        int recz,recx,recy;

        if (adj){ // ADJOINT   
                fprintf(stderr, "ADJOINT OPERATOR \n");
                for(int it=0; it < nt; it++) {
                        fprintf(stderr,"Percent done ADJOINT: %f \n",100*float(it)/float(nt));
                        // Inject reciever recordings (reverse time)
                        std::fill( newF->data(), newF->data() + newF->num_elements(), 0 ); 
                        for(int irec=0; irec < Nrec; irec++) {
                                recz=(*reccoor->_mat)[irec][1];
                                recy=(*reccoor->_mat)[irec][2];
                                recx=(*reccoor->_mat)[irec][3];
                                (*newF)[recx][recy][recz]=(*newF)[recx][recy][recz]+(*gather->_mat)[irec][nt-1-it];
                        }

                        // NEW FASTER WAVEFIELD GENERATOR
                        // Scale current wavefield
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny*nx),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        scatFOR3d(nz,v2dt->data()+i3*nz,curTemp1->data()+i3*nz,curW->data()+i3*nz);
                                }
                        });
                        // Do LaPlacian and time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        waveADJ(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i, abcW3->data()+i, oldW->data()+i,curTemp1->data()+i,curW->data()+i,newW->data()+i,newF->data()+i);
                                                }
                                        }
                                }
                        });


                        if (useabcs){
                                // fprintf(stderr,"using ABCS \n");

                                // Make first-derivatives Z1
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z1blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Z1blocks[ib]._b3; i3 < Z1blocks[ib]._e3; i3++) {
                                                        for(int i2=Z1blocks[ib]._b2; i2 < Z1blocks[ib]._e2; i2++) {
                                                                int i=Z1blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oZ(Z1blocks[ib]._e1-Z1blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWZ->data()+i,dz,-1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives Z2
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z2blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Z2blocks[ib]._b3; i3 < Z2blocks[ib]._e3; i3++) {
                                                        for(int i2=Z2blocks[ib]._b2; i2 < Z2blocks[ib]._e2; i2++) {
                                                                int i=Z2blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oZ(Z2blocks[ib]._e1-Z2blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWZ->data()+i,dz,1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives Y1
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y1blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Y1blocks[ib]._b3; i3 < Y1blocks[ib]._e3; i3++) {
                                                        for(int i2=Y1blocks[ib]._b2; i2 < Y1blocks[ib]._e2; i2++) {
                                                                int i=Y1blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oY(Y1blocks[ib]._e1-Y1blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWY->data()+i,dz,-1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives Y2
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y2blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Y2blocks[ib]._b3; i3 < Y2blocks[ib]._e3; i3++) {
                                                        for(int i2=Y2blocks[ib]._b2; i2 < Y2blocks[ib]._e2; i2++) {
                                                                int i=Y2blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oY(Y2blocks[ib]._e1-Y2blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWY->data()+i,dz,1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives X1
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)X1blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=X1blocks[ib]._b3; i3 < X1blocks[ib]._e3; i3++) {
                                                        for(int i2=X1blocks[ib]._b2; i2 < X1blocks[ib]._e2; i2++) {
                                                                int i=X1blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oX(X1blocks[ib]._e1-X1blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWX->data()+i,dz,-1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives X2
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)X2blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=X2blocks[ib]._b3; i3 < X2blocks[ib]._e3; i3++) {
                                                        for(int i2=X2blocks[ib]._b2; i2 < X2blocks[ib]._e2; i2++) {
                                                                int i=X2blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oX(X2blocks[ib]._e1-X2blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWX->data()+i,dz,1.0);
                                                        }
                                                }
                                        }
                                });

                                // ADD IN THE ALI-STYLE ABCS
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny*nx),[&](const tbb::blocked_range<int>&r){
                                        for(int i3=r.begin(); i3!=r.end(); ++i3){
                                                sumABCs(nz,curWZ->data()+i3*nz,curWY->data()+i3*nz,curWX->data()+i3*nz,curD->data()+i3*nz);
                                                addABC(nz,dt,abcW2->data()+i3*nz,vel->data()+i3*nz,curW->data()+i3*nz,curD->data()+i3*nz,newW->data()+i3*nz,finalP->data()+i3*nz);
                                                simple_mult(nz,finalP->data()+i3*nz,newW->data()+i3*nz,1.0);
                                        }
                                });
                        }



                        // Scale and extract source (reverse time)
                        (*wavelet->_mat)[nt-1-it] = v2dti*(*newW)[soux][souy][souz];

                        // Update the wavefields
                        tmp=oldW; oldW=curW; curW=newW; newW=tmp;

                        // Extract the wavefield for debugging
                        if (wavefield){
                                (*wave->_mat)[it] = (*newW)[souy];
                        }

                }
        }
        else{ // FORWARD
                fprintf(stderr, "FORWARD OPERATOR \n");

                for(int it=0; it < nt; it++) {
                        fprintf(stderr,"Percent done FORWARD : %f \n",100*float(it)/float(nt));

                        // Scale and inject source
                        (*newF)[soux][souy][souz] = v2dti*(*wavelet->_mat)[it];

                        // Do time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW3->data()+i,oldW->data()+i,curW->data()+i,newW->data()+i,newF->data()+i);
                                                }
                                        }
                                }
                        });

                        if (useabcs){
                                // fprintf(stderr,"using ABCS \n");

                                // Make first-derivatives Z1
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z1blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Z1blocks[ib]._b3; i3 < Z1blocks[ib]._e3; i3++) {
                                                        for(int i2=Z1blocks[ib]._b2; i2 < Z1blocks[ib]._e2; i2++) {
                                                                int i=Z1blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oZ(Z1blocks[ib]._e1-Z1blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWZ->data()+i,dz,-1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives Z2
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z2blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Z2blocks[ib]._b3; i3 < Z2blocks[ib]._e3; i3++) {
                                                        for(int i2=Z2blocks[ib]._b2; i2 < Z2blocks[ib]._e2; i2++) {
                                                                int i=Z2blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oZ(Z2blocks[ib]._e1-Z2blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWZ->data()+i,dz,1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives Y1
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y1blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Y1blocks[ib]._b3; i3 < Y1blocks[ib]._e3; i3++) {
                                                        for(int i2=Y1blocks[ib]._b2; i2 < Y1blocks[ib]._e2; i2++) {
                                                                int i=Y1blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oY(Y1blocks[ib]._e1-Y1blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWY->data()+i,dz,-1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives Y2
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y2blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=Y2blocks[ib]._b3; i3 < Y2blocks[ib]._e3; i3++) {
                                                        for(int i2=Y2blocks[ib]._b2; i2 < Y2blocks[ib]._e2; i2++) {
                                                                int i=Y2blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oY(Y2blocks[ib]._e1-Y2blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWY->data()+i,dz,1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives X1
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)X1blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=X1blocks[ib]._b3; i3 < X1blocks[ib]._e3; i3++) {
                                                        for(int i2=X1blocks[ib]._b2; i2 < X1blocks[ib]._e2; i2++) {
                                                                int i=X1blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oX(X1blocks[ib]._e1-X1blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWX->data()+i,dz,-1.0);
                                                        }
                                                }
                                        }
                                });

                                // Make first-derivatives X2
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)X2blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=X2blocks[ib]._b3; i3 < X2blocks[ib]._e3; i3++) {
                                                        for(int i2=X2blocks[ib]._b2; i2 < X2blocks[ib]._e2; i2++) {
                                                                int i=X2blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                first_deriv2oX(X2blocks[ib]._e1-X2blocks[ib]._b1,nz,nz*ny,curW->data()+i,curWX->data()+i,dz,1.0);
                                                        }
                                                }
                                        }
                                });

                                // ADD IN THE ALI-STYLE ABCS
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny*nx),[&](const tbb::blocked_range<int>&r){
                                        for(int i3=r.begin(); i3!=r.end(); ++i3){
                                                sumABCs(nz,curWZ->data()+i3*nz,curWY->data()+i3*nz,curWX->data()+i3*nz,curD->data()+i3*nz);
                                                addABC(nz,dt,abcW2->data()+i3*nz,vel->data()+i3*nz,curW->data()+i3*nz,curD->data()+i3*nz,newW->data()+i3*nz,finalP->data()+i3*nz);
                                                simple_mult(nz,finalP->data()+i3*nz,newW->data()+i3*nz,1.0);
                                        }
                                });
                        }

                        for(int irec=0; irec < Nrec; irec++) {
                                // Extract reciever recordings
                                int recz=(*reccoor->_mat)[irec][1];
                                int recy=(*reccoor->_mat)[irec][2];
                                int recx=(*reccoor->_mat)[irec][3];
                                (*gather->_mat)[irec][it]=(*newW)[recx][recy][recz];
                        }

                        // Extract the wavefield for debugging
                        if (wavefield){
                                (*wave->_mat)[it] = (*newW)[soux];
                        }


                        // Update the wavefields
                        tmp=oldW; oldW=curW; curW=newW; newW=tmp;
                }

                fprintf(stderr, "Finished time stepping \n" );

                for(int irec=0; irec < Nrec; irec++){
                        // Write header info for each extracted trace
                        int nodeId=(*reccoor->_mat)[irec][0];
                        int recz=(*reccoor->_mat)[irec][1];
                        int recy=(*reccoor->_mat)[irec][2];
                        int recx=(*reccoor->_mat)[irec][3];
                        float offset=(*reccoor->_mat)[irec][7];
                        float gz=float(recz);
                        float gy=float(recy);
                        float gx=float(recx);
                        float sz=float(souz);
                        float sy=float(souy);
                        float sx=float(soux);
                        (*gatherH->_mat)[irec][0]=nodeId;
                        (*gatherH->_mat)[irec][1]=gz;
                        (*gatherH->_mat)[irec][2]=gy;
                        (*gatherH->_mat)[irec][3]=gx;
                        (*gatherH->_mat)[irec][4]=sz;
                        (*gatherH->_mat)[irec][5]=sy;
                        (*gatherH->_mat)[irec][6]=sx;
                        (*gatherH->_mat)[irec][7]=offset;
                }

                fprintf(stderr, "Already writen the header information \n" );

                if(rtm){
                        // Make the extra two time frames for RTM purposes
                        for(int it=0; it < 2; it++) {

                                // Scale and inject source
                                (*newF)[soux][souy][souz] = v2dti*(*wavelet->_mat)[nt-1];

                                // Do time stepping
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                                int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                                laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW3->data()+i,oldW->data()+i,curW->data()+i,newW->data()+i,newF->data()+i);
                                                        }
                                                }
                                        }
                                });

                                // Update the wavefields
                                tmp=oldW; oldW=curW; curW=newW; newW=tmp;
                        fprintf(stderr, "Did the last two frames for RTM purposes \n" );
                        }
                }
        }
        x2.stop();
        x2.print();


        // =====================================================================
        // Ouput wavefield times frames to use for an RTM code 
        // (avoids the need to save the full wavefield)

        if(rtm){
                fprintf(stderr, "Write the RTM outputs \n" );
                // Output "previous" wavefield
                std::shared_ptr<float1DReg> oldW2(new float1DReg(hyperVel1d));
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        (*oldW2->_mat)[ii] = oldW->data()[ii]; 
                }
                outPRE->writeFloatStream(oldW2->getVals(),hyperOutFRAME->getN123());  // Shot gather


                // Output "current" wavefield
                std::shared_ptr<float1DReg> curW2(new float1DReg(hyperVel1d));
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        (*curW2->_mat)[ii] = curW->data()[ii]; 
                }
                outCUR->writeFloatStream(curW2->getVals(),hyperOutFRAME->getN123());  // Shot gather
        }


        // =============================================================================
        // Write the outputs to SEP files
        if (wavefield){
                fprintf(stderr, "Write the wavefield \n" );
                outWV->writeFloatStream(wave->getVals(),hyperOut3d->getN123());  // Full wavefield at souy
        } // Have found that for large wavefields, this fails and then the real output never gets written. 
        // Try to only use this when actually needed.

        fprintf(stderr, "Write the main outputs \n" );
        if(adj){
                wav->writeFloatStream(wavelet->getVals(),hyperWav2->getN123());  // Wavelet
        }
        else{
                outSG->writeFloatStream(gather->getVals(),hyperOut2d->getN123());       // Shot gather
                outSGH->writeFloatStream(gatherH->getVals(),hyperOut2dH->getN123());    // Shot gather header info
        }

        x.stop();
        x.print();


}




