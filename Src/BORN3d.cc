
//==============================================================================================
//
//      3D BORN APPROXIMATION CODE
//      Taylor Dahlke, taylor@sep.stanford.edu, 10/23/2017
//
//      This program runs the Born approximation to the acoustic wave equation. It uses 
//      TBB vectorization for the inner loop to speed things up. I follow the formulation
//      described in the paper:
//
//      "Accurate implementation of two-way wave-equation operators", Ali Almomim
//
//      Absorbing boundary conditions are simple dampening in a padding region around the model.
//
//      FORWARD OPERATOR:  
//         reflectivity space (Nz x NX x NY) ---->>  residual space (NT x NREC)
//
//      ADJOINT OPERATOR:  
//         residual space (NT x NREC) ---->>  reflectivity space (Nz x NX x NY)
//
//==============================================================================================

#include "boost/multi_array.hpp"
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "timer.h"
#include "laplacianOP3d.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <float1DReg.h>
#include <ioModes.h>
#include <iostream>
using namespace SEP;
using namespace giee;
using namespace std;

typedef boost::multi_array<float, 3> float3D;
class block {
public:
block(int b1,int e1,int b2,int e2,int b3,int e3) : _b1(b1),_e1(e1),_b2(b2),_e2(e2),_b3(b3),_e3(e3){
};
int _b1,_e1,_b2,_e2,_b3,_e3;
};



int main(int argc, char **argv){

        if (argc==1){
                cout << "==========================================================================\n\n" << endl;
                cout << "==========================================================================\n\n" << endl;
                cout << "NAME:   BORN3D.x  --- 3D Born operator" << endl; 
                cout << "SYNOPSIS:   BORN3D.x  par=  wavelet=wavelet.H  abcs=abcs.H  < in.H > out.H" << endl;
                cout << "DESCRIPTION:  This program runs the Born approximation to the acoustic wave equation with constant density. It uses TBB vectorization for the inner loop to speed things up. I follow the formulation described in the paper: 'Accurate implementation of two-way wave-equation operators', Ali Almomim. Absorbing boundary conditions are simple dampening in a padding region around the model.\n\n" << endl;
                cout << "FORWARD OPERATOR: "<< endl;
                cout << "       reflectivity space (Nz x NX x NY) ---->>  residual space (NT x NREC)"<< endl;
                cout << "ADJOINT OPERATOR: "<< endl;
                cout << "       residual space (NT x NREC) ---->>  reflectivity space (Nz x NX x NY)\n\n"<< endl;



                cout << "INPUT FILES: \n" << endl;                           
                cout << "wavelet: One dimensional .H file (nt x 1) that has the source wavelet. \n" << endl;                           
                cout << "vel: Three dimensional .H file (nz x nx x ny) that has the acosutic wavespeed in [m/s]. \n" << endl;                           
                cout << "rec: Two dimensional .H file of (8 x ntraces) that has header info about each trace in the shot (or receiver) gather. \n" << endl;                           
                cout << "abc: Three dimensional .H file (nz x nx x ny) that has the weights for the absorbing boundary conditions. \n" << endl;                           
                cout << "curW: Three dimensional .H file (nz x nx x ny) that has the wavefield at time nt. \n" << endl;                           
                cout << "oldW: Three dimensional .H file (nz x nx x ny) that has the wavefield at time nt-1. \n\n" << endl;                           

                cout << "INPUT PARAMETERS: \n" << endl;                           
                cout << "adj: [int] (1 or 0) Indicates whether the operator is being run as forward or adjoint.\n" << endl;                           
                cout << "souX: [int] (0 to nx-1) Indicates X index position of the source. \n" << endl;                           
                cout << "souY: [int] (0 to ny-1) Indicates Y index position of the source. \n" << endl;                           
                cout << "souZ: [int] (0 to nz-1) Indicates Z index position of the source. \n" << endl;                           
                cout << "d1: [float] Spatial sampling of each grid cell in meters. This code assumes dx = dz = dy. \n" << endl;                           
                cout << "dt: [float] Time sampling. \n" << endl;                           
                cout << "nt: [int] Number of time samples.\n" << endl;                           
                cout << "cfl: [int] (1 or 0) Indicates whether or not to check the CFL condition before running.\n" << endl;                           
                cout << "wavefield: [int] (1 or 0) Indicates whether or not to write out the receiver wavefield.\n" << endl;                           
                cout << "bs: [int] (1 to min(nx,nz,ny)) Block side used for TBB thread blocking.\n" << endl;                           

                exit(0);
        }

        timer x=timer("FULL RUN");
        timer x1=timer("INITIALIZATION");
        timer x2=timer("MAIN WAVEFIELD LOOP");
        x.start();
        x1.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> velin=io->getRegFile("vel",usageIn);
        std::shared_ptr<genericRegFile> rec=io->getRegFile("rec",usageIn);
        std::shared_ptr<hypercube> hyperVel=velin->getHyper();
        std::shared_ptr<hypercube> hyperRec=rec->getHyper();

        //==========================================================================
        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long ny=velN[1];
        long long nx=velN[2];
        float dz = hyperVel->getAxis(1).d;
        float dy = hyperVel->getAxis(2).d;
        float dx = hyperVel->getAxis(3).d;

        float oz = hyperVel->getAxis(1).o;
        float oy = hyperVel->getAxis(2).o;
        float ox = hyperVel->getAxis(3).o;

        fprintf(stderr, "nz=%d\n",nz );
        fprintf(stderr, "nx=%d\n",nx );
        fprintf(stderr, "ny=%d\n",ny );

        //==========================================================================
        // Get acquisition parameters
        std::vector<int> recN=hyperRec->getNs();
        int Nkeys=recN[0];
        int Nrec=recN[1];

        //==========================================================================
        // Get propagation parameters
        int nt=par->getInt("nt",100);
        // float vconst=par->getFloat("vconst",2.0);
        float dsamp=par->getFloat("d1",0.1);
        float dt=par->getFloat("dt",0.002);
        int adj=par->getInt("adj",0);
        int cfl=par->getInt("cfl",0);
        // int souId=par->getInt("souId",1);

        SEP::axis axT(nt,0.0,dt);
        SEP::axis axR(Nrec,0.0,1.0);
        SEP::axis axZ = hyperVel->getAxis(1);
        SEP::axis axY = hyperVel->getAxis(2);
        SEP::axis axX = hyperVel->getAxis(3);
        SEP::axis axK(Nkeys,0.0,1.0);

        x1.stop();
        x1.print();
        x1.start();

        fprintf(stderr, "Nkeys =%d\n", Nkeys);
        fprintf(stderr, "Nrec =%d\n", Nrec);
        fprintf(stderr, "nt =%d\n", nt);


        //==========================================================================
        // Auxilary inputs and outputs

        // Read in the source wavelet
        fprintf(stderr, "WAVELET INPUT\n" );
        std::shared_ptr<genericRegFile> wav;
        std::shared_ptr<hypercube> hyperWav(new hypercube(axT));
        std::shared_ptr<float1DReg> wavelet(new float1DReg(hyperWav));
        wav=io->getRegFile("wavelet",usageIn);
        wav->readFloatStream(wavelet->getVals(),hyperWav->getN123()); 

        // Setup the velocity model input
        SEP::axis ax1D(nx*ny*nz,0.0,1.0);
        std::shared_ptr<hypercube> hyperVel1d(new hypercube(ax1D));
        std::shared_ptr<float1DReg> velModel(new float1DReg(hyperVel1d));
        velin->readFloatStream(velModel->getVals(),hyperVel->getN123());

        // Read in the reciever position coordinates input
        std::shared_ptr<float2DReg> reccoor(new float2DReg(hyperRec));
        rec->readFloatStream(reccoor->getVals(),hyperRec->getN123());

// // ==============================================================================
//         // WAVEFIELD OUTPUT
        int wavefield=par->getInt("wavefield",0);
//         fprintf(stderr, "wavefield=%d\n", wavefield);
//         std::shared_ptr<hypercube> hyperOut3d(new hypercube(axZ,axY,axT));
//         std::shared_ptr<float3DReg> wave(new float3DReg(hyperOut3d));
//         std::shared_ptr<genericRegFile> outWV;
//         if (wavefield){
//                 outWV=io->getRegFile("wave",usageOut);
//                 outWV->setHyper(hyperOut3d);
//                 outWV->writeDescription();
//                 fprintf(stderr,">>>>> Done setting up the wavefield output \n");
//         }
// // ==============================================================================


        // TEST VEL OUTPUT
        std::shared_ptr<hypercube> hyperOut3dv(new hypercube(axZ,axY,axX));
        std::shared_ptr<float3DReg> testvel(new float3DReg(hyperOut3dv));
        std::shared_ptr<genericRegFile> outTV;
        outTV=io->getRegFile("testvel",usageOut);
        outTV->setHyper(hyperOut3dv);
        outTV->writeDescription();
        fprintf(stderr,">>>>> Done setting up the testvel output \n");




        x1.stop();
        x1.print();
        x1.start();

        //==========================================================================
        // Model input/output
        std::shared_ptr<genericRegFile> refl;
        std::shared_ptr<float3D> model1(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float1DReg> model2(new float1DReg(hyperVel1d));

        if(adj){// REFLECTIVITY OUTPUT
                fprintf(stderr, "REFLECTIVITY OUTPUT\n" );
                refl=io->getRegFile("out",usageOut);
                refl->setHyper(hyperVel);
                refl->writeDescription();
        }
        else{   // REFLECTIVITY INPUT
                fprintf(stderr, "REFLECTIVITY INPUT\n" );
                refl=io->getRegFile("in",usageIn);
                refl->readFloatStream(model2->getVals(),hyperVel1d->getN123());  
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        model1->data()[ii] = (*model2->_mat)[ii];
                } 
        }
        x1.stop();
        x1.print();
        x1.start();


        //==========================================================================
        // Shot gather data
        std::shared_ptr<hypercube> hyperShotG(new hypercube(axT,axR));
        std::shared_ptr<float2DReg> data(new float2DReg(hyperShotG));
        std::shared_ptr<genericRegFile> shotG;
        // Shot gather 3D header
        std::shared_ptr<hypercube> hyperShotGH(new hypercube(axK,axR));
        std::shared_ptr<float2DReg> dataH(new float2DReg(hyperShotGH));
        std::shared_ptr<genericRegFile> shotGH;
        if(adj){// SHOT GATHER INPUT
                fprintf(stderr, "SHOT GATHER INPUT\n" );
                shotG=io->getRegFile("in",usageIn);
                shotG->readFloatStream(data->getVals(),hyperShotG->getN123());    
        }
        else{   // SHOT GATHER OUTPUT
                fprintf(stderr, "SHOT GATHER OUTPUT\n" );
                shotG=io->getRegFile("out",usageOut);
                shotG->setHyper(hyperShotG);
                shotG->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather output \n");
                fprintf(stderr, "SHOT GATHER 3D HEADER OUTPUT\n" );
                shotGH=io->getRegFile("header",usageOut);
                shotGH->setHyper(hyperShotGH);
                shotGH->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather 3D spatial header output \n");
        }
        //==========================================================================
        x1.stop();
        x1.print();
        x1.start();



        //==========================================================================
        // Setup the absorbing boundary condition weights input
        std::shared_ptr<float3D> abcW2(new float3D(boost::extents[nx][ny][nz]));
        fprintf(stderr, "Using no ABC dampening \n");
        for(int ii=0; ii < nx*ny*nz; ii++) {
                abcW2->data()[ii] = 1.0;
        }

        x1.stop();
        x1.print();
        x1.start();


        //==========================================================================
        // Read in the background velocity model (v0) and scale
        std::shared_ptr<float3D> v2dt(new float3D(boost::extents[nx][ny][nz]));
        if(cfl){
                float maxvel=(*velModel->_mat)[0];
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        v2dt->data()[ii] = ((*velModel->_mat)[ii])*((*velModel->_mat)[ii])*dt*dt;
                        if ( (*velModel->_mat)[ii] > maxvel){
                                maxvel = (*velModel->_mat)[ii];
                        }
                }
                // Check the CFL condition
                float cfl=maxvel*dt/dsamp;
                float compval=sqrt(3.0)/3.0;
                if (cfl>=compval){
                        fprintf(stderr,">>>>> ERROR: %f > %f \n",cfl,compval);
                        fprintf(stderr,">>>>> maxvel=%f  dt=%f  dsamp=%f \n",maxvel,dt,dsamp);
                        fprintf(stderr,"Doesn't pass the CFL condition. Either decrease dt or increase dsamp\n");
                        exit (EXIT_FAILURE);
                }
                fprintf(stderr,">>>>> PASSED the CFL condition \n");
        }
        else{
                float maxvel=(*velModel->_mat)[0];
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        v2dt->data()[ii] = ((*velModel->_mat)[ii])*((*velModel->_mat)[ii])*dt*dt;
                }
                fprintf(stderr,">>>>> Did not check the CFL condition \n");
        }
        fprintf(stderr,">>>>> Done reading / scaling the velocity model \n");



        x1.stop();
        x1.print();
        x1.start();

        fprintf(stderr, "Initializing temp arrays \n");

        // =====================================================================
        // Initialize laplacian FD coefficients and pressure fields

        float coef[6];
        float sc=1.0/(dsamp*dsamp);
        coef[1]=sc*42000/(25200.0);
        coef[2]=-1.0*sc*6000/(25200.0);
        coef[3]=sc*1000/(25200.0);
        coef[4]=-1.0*sc*125/(25200.0);
        coef[5]=sc*8/(25200.0);
        coef[0]=-6*(coef[1]+coef[2]+coef[3]+coef[4]+coef[5]);

        std::shared_ptr<float3D> oldS(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curS(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> newS(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> newFS(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> tmpS(new float3D(boost::extents[nx][ny][nz]));

        std::shared_ptr<float3D> oldR(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curR(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> newR(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> newFR(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> tmpR(new float3D(boost::extents[nx][ny][nz]));

        std::shared_ptr<float3D> oldTMP(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curTMP(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> newTMP(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> temp(new float3D(boost::extents[nx][ny][nz]));
        std::shared_ptr<float3D> curTemp1(new float3D(boost::extents[nx][ny][nz]));

        fprintf(stderr,">>>>> Zeroing out the arrays \n");
        std::fill( newS->data(), newS->data() + newS->num_elements(), 0 ); 
        std::fill( newFS->data(), newFS->data() + newFS->num_elements(), 0 ); 
        std::fill( newR->data(), newR->data() + newR->num_elements(), 0 ); 
        std::fill( newFR->data(), newFR->data() + newFR->num_elements(), 0 ); 
        std::fill( oldR->data(), oldR->data() + oldR->num_elements(), 0 ); 
        std::fill( oldS->data(), oldS->data() + oldS->num_elements(), 0 ); 
        std::fill( curR->data(), curR->data() + curR->num_elements(), 0 ); 
        std::fill( curS->data(), curS->data() + curS->num_elements(), 0 ); 
        std::fill( oldTMP->data(), oldTMP->data() + oldTMP->num_elements(), 0 ); 
        std::fill( curTMP->data(), curTMP->data() + curTMP->num_elements(), 0 ); 
        std::fill( newTMP->data(), newTMP->data() + newTMP->num_elements(), 0 ); 
        std::fill( temp->data(), temp->data() + temp->num_elements(), 0 ); 
        std::fill( curTemp1->data(), curTemp1->data() + curTemp1->num_elements(), 0 ); 

        // for(long long i=0; i < nx*ny*nz; i++) {
        //         newS->data()[i]=0.0;
        //         newFS->data()[i]=0;
        //         newR->data()[i]=0;
        //         newFR->data()[i]=0;
        //         oldR->data()[i]=0;
        //         oldS->data()[i]=0;
        //         curR->data()[i]=0;
        //         curS->data()[i]=0;
        //         oldTMP->data()[i]=0;
        //         curTMP->data()[i]=0;
        //         newTMP->data()[i]=0;
        //         temp->data()[i]=0;
        //         curTemp1->data()[i]=0;
        // }

        // //=====================================================================
        x1.stop();
        x1.print();
        x1.start();     
        if(adj){// READ WAVEFIELD PREVIOUS FRAME INPUT
                fprintf(stderr, "READ WAVEFIELD PREVIOUS FRAME INPUT \n");
                std::shared_ptr<float1DReg> old2(new float1DReg(hyperVel1d));
                std::shared_ptr<genericRegFile> inPRE;
                inPRE=io->getRegFile("curW",usageIn); // Switched for time reversal
                inPRE->readFloatStream(old2->getVals(),hyperVel1d->getN123());
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        oldS->data()[ii] = (*old2->_mat)[ii];
                }
        }
        x1.stop();
        x1.print();
        x1.start();
        if(adj){ // READ WAVEFIELD CURRENT FRAME INPUT
                fprintf(stderr, "READ WAVEFIELD CURRENT FRAME INPUT \n");
                std::shared_ptr<float1DReg> cur2(new float1DReg(hyperVel1d));
                std::shared_ptr<genericRegFile> inCUR;
                inCUR=io->getRegFile("oldW",usageIn); // Switched for time reversal
                inCUR->readFloatStream(cur2->getVals(),hyperVel1d->getN123());
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        curS->data()[ii] = (*cur2->_mat)[ii];
                }
        }


        x1.stop();
        x1.print();
        x1.start();

        //=====================================================================
        // Set blocksize
        int bs=par->getInt("bs",20);
        int bs1,bs2,bs3;
        bs1=bs;
        bs2=bs;
        bs3=bs;

        vector<int> b1(1,5),e1(1,5+bs1);
        int nleft=nz-10-bs1,i=0;
        while(nleft>0) {
                int bs=min(nleft,bs1);
                b1.push_back(e1[i]);
                e1.push_back(e1[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b2(1,5),e2(1,5+bs2);
        nleft=ny-10-bs2;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs2);
                b2.push_back(e2[i]);
                e2.push_back(e2[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b3(1,5),e3(1,5+bs3);
        nleft=nx-10-bs3;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs3);
                b3.push_back(e3[i]);
                e3.push_back(e3[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<block> blocks;
        for(int i3=0; i3<b3.size(); ++i3) {
                for(int i2=0; i2<b2.size(); ++i2) {
                        for(int i1=0; i1<b1.size(); ++i1) {
                                blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                        }
                }
        }

        x1.stop();
        x1.print();
        x1.start();

        //=====================================================================        
        // Specify source position
        int isou=0;
        int souz=par->getFloat("souZ");
        int souy=par->getFloat("souY");
        int soux=par->getFloat("souX");
        fprintf(stderr,"souz (index) = %d\n",souz);
        fprintf(stderr,"souy (index) = %d\n",souy);
        fprintf(stderr,"soux (index) = %d\n",soux);


        int souyxz=souz+souy*nz+soux*ny*nz;
        float v2dti=-1.0*((*velModel->_mat)[souyxz])*((*velModel->_mat)[souyxz])*dt*dt;
        float dt2=dt*dt;

        //========  MAIN WAVEFIELD LOOP ========================================

        x1.stop();
        x1.print();
        x2.start();

        if (adj){ // ADJOINT   
                fprintf(stderr, "ADJOINT OPERATOR \n");

                // // TEST VEL OUTPUT TO MAKE SURE THE ACQ IS GOOD
                // // (*v2dt)[soux][souy][souz] = (*v2dt)[soux][souy][souz]*10.0;
                // for(int irec=0; irec < Nrec; irec++){
                //         // Write header info for each extracted trace
                //         int recz=(*reccoor->_mat)[irec][1];
                //         int recy=(*reccoor->_mat)[irec][2];
                //         int recx=(*reccoor->_mat)[irec][3];
                //         (*v2dt)[recx][recy][recz] = 5.0*(*v2dt)[recx][recy][recz];
                // }
                // (*testvel->_mat) = (*v2dt);

                // Kick start the source wavefield using (*wavelet->_mat)[nt-1] as the source
                for(int it=0; it < 2; it++) {
                        // Inject receiver recordings (reverse time)
                        std::fill( newFR->data(), newFR->data() + newFR->num_elements(), 0 ); 
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[irec][1];
                                int recy=(*reccoor->_mat)[irec][2];
                                int recx=(*reccoor->_mat)[irec][3];
                                (*newFR)[recx][recy][recz]=(*newFR)[recx][recy][recz]+(*data->_mat)[irec][nt-1-it];
                        }
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[soux][souy][souz] = v2dti*(*wavelet->_mat)[nt-1];
                        // Do time stepping
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        // Inject and propagate source wavefield
                                                        laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW2->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                                        // Adjoint stepping kernel (reverse time rec wavefield)
                                                        laplacianADJ3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW2->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                                        // Scale wavefield
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,oldR->data()+i,oldTMP->data()+i,v2dt->data()+i);
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,curR->data()+i,curTMP->data()+i,v2dt->data()+i);
                                                        scale3d(blocks[ib]._e1-blocks[ib]._b1,newR->data()+i,newTMP->data()+i,v2dt->data()+i);
                                                        // Take the second derivative
                                                        secder3d(blocks[ib]._e1-blocks[ib]._b1,oldTMP->data()+i,curTMP->data()+i,newTMP->data()+i,temp->data()+i,dt2);
                                                        // Imaging condition
                                                        scatADJ3d(blocks[ib]._e1-blocks[ib]._b1,newS->data()+i,temp->data()+i,model1->data()+i);
                                                }
                                        }
                                }
                        });

                        // // Extract the wavefield for debugging
                        // if (wavefield){
                        //         // (*wave->_mat)[it] = (*model1)[soux];
                        //         (*wave->_mat)[it] = (*temp)[soux];
                        // }

                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }

                fprintf(stderr, "ADJOINT OPERATOR main loop\n");

                // MAIN LOOP
                for(int it=2; it < nt; it++) {
                        // Inject receiver recordings (reverse time)
                        std::fill( newFR->data(), newFR->data() + newFR->num_elements(), 0 ); 
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[irec][1];
                                int recy=(*reccoor->_mat)[irec][2];
                                int recx=(*reccoor->_mat)[irec][3];
                                (*newFR)[recx][recy][recz]=(*newFR)[recx][recy][recz]+(*data->_mat)[irec][nt-1-it];
                        }
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[soux][souy][souz] = v2dti*(*wavelet->_mat)[nt+1-it];

                        // Inject and propagate source wavefield
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        laplacianFOR3d_noABC(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                                }
                                        }
                                }
                        });

                        // Adjoint stepping kernel (reverse time rec wavefield)
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny*nx),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        scatFOR3d(nz,v2dt->data()+i3*nz,curTemp1->data()+i3*nz,curR->data()+i3*nz);
                                }
                        });
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        waveADJ_noABC(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,oldR->data()+i,curTemp1->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                                }
                                        }
                                }
                        });

                        // Combine scaling, second time derivative, and imaging condition into one function for efficiency sake                                        
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny*nx),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        comboBornADJ(nz,v2dt->data()+i3*nz,oldR->data()+i3*nz,curR->data()+i3*nz,newR->data()+i3*nz,temp->data()+i3*nz,dt2);
                                        scatADJ3d(nz,newS->data()+i3*nz,temp->data()+i3*nz,model1->data()+i3*nz);
                                }
                        });

//==========================================================================================
                        // Do time stepping
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                        //                         for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                        //                                 int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        // // Inject and propagate source wavefield
                                                        // laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW2->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                                        // // Adjoint stepping kernel (reverse time rec wavefield)
                                                        // laplacianADJ3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW2->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                                        // // Scale wavefield
                                                        // scale3d(blocks[ib]._e1-blocks[ib]._b1,oldR->data()+i,oldTMP->data()+i,v2dt->data()+i);
                                                        // scale3d(blocks[ib]._e1-blocks[ib]._b1,curR->data()+i,curTMP->data()+i,v2dt->data()+i);
                                                        // scale3d(blocks[ib]._e1-blocks[ib]._b1,newR->data()+i,newTMP->data()+i,v2dt->data()+i);
                                                        // // Take the second derivative
                                                        // secder3d(blocks[ib]._e1-blocks[ib]._b1,oldTMP->data()+i,curTMP->data()+i,newTMP->data()+i,temp->data()+i,dt2);
                                                        // // Imaging condition
                                                        // scatADJ3d(blocks[ib]._e1-blocks[ib]._b1,newS->data()+i,temp->data()+i,model1->data()+i);
                        //                         }
                        //                 }
                        //         }
                        // });

                        // // Extract the wavefield for debugging
                        // if (wavefield){
                        //         // (*wave->_mat)[it] = (*model1)[soux];
                        //         (*wave->_mat)[it] = (*temp)[soux];
                        // }

                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }
        }
        else{ // FORWARD
                fprintf(stderr, "FORWARD OPERATOR \n");
                for(int it=0; it < nt; it++) {
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[soux][souy][souz] = v2dti*(*wavelet->_mat)[it];

                        // Propagate the source wavefield
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        laplacianFOR3d_noABC(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                                }
                                        }
                                }
                        });
                        // Combine scattering, second time derivative, and scaling into one function for efficiency sake                                        
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny*nx),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        comboBornFOR(nz,v2dt->data()+i3*nz,model1->data()+i3*nz,oldS->data()+i3*nz,curS->data()+i3*nz,newS->data()+i3*nz,newFR->data()+i3*nz,dt2);
                                }
                        });
                        // Propagate the scattered data (rec wavefield)
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                        int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                                                        laplacianFOR3d_noABC(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                                }
                                        }
                                }
                        });


                        // // Do time stepping
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                        //                         for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                        //                                 int i=blocks[ib]._b1+i2*nz+i3*nz*ny;
                        //                                 laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW2->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                        //                                 // Scatter source wave timeframe against refl model
                        //                                 scatFOR3d(blocks[ib]._e1-blocks[ib]._b1,newS->data()+i,newTMP->data()+i,model1->data()+i);
                        //                                 scatFOR3d(blocks[ib]._e1-blocks[ib]._b1,curS->data()+i,curTMP->data()+i,model1->data()+i);
                        //                                 scatFOR3d(blocks[ib]._e1-blocks[ib]._b1,oldS->data()+i,oldTMP->data()+i,model1->data()+i);
                        //                                 // Take the second derivative (rec wavefield)
                        //                                 secder3d(blocks[ib]._e1-blocks[ib]._b1,oldTMP->data()+i,curTMP->data()+i,newTMP->data()+i,temp->data()+i,dt2);
                        //                                 // Scale the wavefield (rec wavefield)
                        //                                 scale3d(blocks[ib]._e1-blocks[ib]._b1,temp->data()+i,newFR->data()+i,v2dt->data()+i);
                        //                                 // Propagate the scattered data (rec wavefield)
                        //                                 laplacianFOR3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,coef,v2dt->data()+i,abcW2->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                        //                         }
                        //                 }
                        //         }
                        // });


                        // Extract reciever recordings
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[irec][1];
                                int recy=(*reccoor->_mat)[irec][2];
                                int recx=(*reccoor->_mat)[irec][3];
                                (*data->_mat)[irec][it]=(*newR)[recx][recy][recz];
                        }

                        // // Extract the wavefield for debugging
                        // if (wavefield){
                        //         (*wave->_mat)[it] = (*newR)[soux];
                        //         // (*wave->_mat)[it] = (*model1)[soux];
                        // }

                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }

                for(int irec=0; irec < Nrec; irec++){
                        // Write header info for each extracted trace
                        int nodeId=(*reccoor->_mat)[irec][0];
                        int recz=(*reccoor->_mat)[irec][1];
                        int recy=(*reccoor->_mat)[irec][2];
                        int recx=(*reccoor->_mat)[irec][3];
                        float offset=(*reccoor->_mat)[irec][7];
                        float gz=float(recz);
                        float gy=float(recy);
                        float gx=float(recx);
                        float sz=float(souz);
                        float sy=float(souy);
                        float sx=float(soux);
                        (*dataH->_mat)[irec][0]=nodeId;
                        (*dataH->_mat)[irec][1]=gz;
                        (*dataH->_mat)[irec][2]=gy;
                        (*dataH->_mat)[irec][3]=gx;
                        (*dataH->_mat)[irec][4]=sz;
                        (*dataH->_mat)[irec][5]=sy;
                        (*dataH->_mat)[irec][6]=sx;
                        (*dataH->_mat)[irec][7]=offset;
                }

        }
        x2.stop();
        x2.print();




        fprintf(stderr, "About to write out the debugging wavefield \n");
        // =============================================================================
        // // Write the outputs to SEP files
        // if (wavefield){
        //         outWV->writeFloatStream(wave->getVals(),hyperOut3d->getN123());  // Full wavefield at souy
        //         fprintf(stderr, "Just wrote the debuggin wavefield \n");
        // }

        if(adj){
                fprintf(stderr, "Writing out the reflectivity \n");
                for(int ii=0; ii < nx*ny*nz; ii++) {
                        (*model2->_mat)[ii]=model1->data()[ii];
                } 
                refl->writeFloatStream(model2->getVals(),hyperVel->getN123());  // Wavelet
        }
        else{
                fprintf(stderr, "Writing out the shot gather \n");
                shotG->writeFloatStream(data->getVals(),hyperShotG->getN123());         // Shot gather
                shotGH->writeFloatStream(dataH->getVals(),hyperShotGH->getN123());      // Shot gather header info
        }

        // Write the test vel output
        outTV->writeFloatStream(testvel->getVals(),hyperOut3dv->getN123());  // Full wavefield at souy


        x.stop();
        x.print();

}
