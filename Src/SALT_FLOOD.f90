!===============================================================================
!		Floods salt velocity straight down when it encounters it
!		MODEL:  NZxNYxNX	(input salt model)
!		DATA:   NZxNYxNX	(flooded salt model)
!
!===============================================================================

program SALT_FLOOD
	use sep
	implicit none

	real,dimension(:,:,:),allocatable	:: input,output
	logical								:: verbose
	integer,dimension(8)               	:: timer0,timer1
	real								:: timer_sum=0.,threshold,vsalt,oy,ox,dy,dx,yy,threshY
	integer								:: nz,ny,nx,iz,iy,ix,izR,iyR,ixR

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	call from_param("verbose",verbose,.TRUE.)
	if (verbose) write(0,*) "==================== Read in initial parameters"
	call from_param("threshold",threshold,4400.0)
	call from_param("threshY",threshY,51000.0)
	call from_param("vsalt",vsalt,4480.0)
	call from_history("n1",nz)
	call from_history("n2",ny)
	call from_history("n3",nx)
	call from_history("o2",oy)
	call from_history("o3",ox)
	call from_history("d2",dy)
	call from_history("d3",dx)

	IF(verbose) WRITE(0,*) "========= Allocate ==================================="
	allocate(input(nz,ny,nx))
	allocate(output(nz,ny,nx))

	IF(verbose) WRITE(0,*) "========= Read in input =================="
	CALL sep_read(input)

	output=input
!##########################################################################################

	! Z down
	do ix=1,nx
		do iy=1,ny
			yy = oy + dy*(iy-1)
			if (yy<threshY) then
				do iz=1,nz
					if (input(iz,iy,ix)>=threshold) then
						output(iz:nz,iy,ix)=vsalt
						EXIT
					end if
				end do
			end if
		end do
	end do

!##########################################################################################

	if(verbose) write(0,*) "==================== Write output file"
	call to_history("n1",nz)
	call to_history("n2",ny)
	call to_history("n3",nx)
	call to_history("label1",'z [m]')
	call to_history("label2",'y [m]')
	call to_history("label2",'x [m]')
	call sep_write(output)

	if (verbose) write(0,*) "Program complete"

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec)",timer_sum/1000
	write(0,*) "timer sum (min)",timer_sum/1000/60
	write(0,*) "DONE!"
	call sep_close()

end program