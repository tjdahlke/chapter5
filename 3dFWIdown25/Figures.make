
##########################################################################################
# FIGURES

itf=35

ER: ${R}/initvel-side.pdf ${R}/initvel-top.pdf

CR: CRv ${R}/phi-side-it-0.pdf ${R}/phi-side-it-${itf}.pdf ${R}/phi-top-it-0.pdf \
	${R}/saltmask-side-it-0.pdf ${R}/saltmask-side-it-2.pdf ${R}/velmodel-side-it-${itf}.pdf \
	${R}/velmodel-top-it-${itf}.pdf ${R}/velmodel-diff-side-${itf}.pdf ${R}/velmodel-diff-top-${itf}.pdf \
	${R}/velback-diff-side-${itf}.pdf ${R}/velback-diff-top-${itf}.pdf ${R}/top-zone-vel-diff-214820.pdf \
	${R}/inc-zone-vel-diff-214800.pdf ${R}/inc-zone-vel-diff-214950.pdf

NR:
	# No NR Figures in 3dFWIdown25

CRv: ${R}/top-zone-vel-diff-214820.v ${R}/inc-zone-vel-diff-214800.v ${R}/inc-zone-vel-diff-214950.v
	-
##########################################################################################
min3=212000
min2=48000
max2=52000
d2num=1000
slicepar=min3=214800 n3=1 min1=400.0 min2=${min2} max2=${max2} max1=2300
sliceparT=min1=1700.0 n1=1


# Vel model used in migration
${R}/initvel-side.v: ${vel_path}comp
	Window3d ${slicepar}  n4=1 f4=0 < $< | Grey o1num=500 o2num=${min2} d2num=${d2num} color=jc wantscalebar=y newclip=1 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Velocity [m/s]' ${FIG}$@

${R}/initvel-top.v: ${vel_path}comp
	Window3d ${sliceparT} n4=1 f4=0 < $< | Transp plane=12 > $@1
	Grey o1num=213000 wantscalebar=y d2num=${d2num} color=jc newclip=1 title=" " label1='Inline [m]' label2='Crossline [m]' label3='Velocity [m/s]' < $@1 ${FIG}$@
	rm $@1

${R}/phi-side-it-0.v: ${phi_path}comp
	Window3d ${slicepar} n4=1 f4=0 < $< | Grey o1num=500 o2num=${min2} d2num=${d2num} color=F wantscalebar=y newclip=1 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Magnitude' ${FIG}$@

${R}/phi-side-it-${itf}.v: ${phi_path}comp
	Window3d ${slicepar} n4=1 f4=${itf} < $< | Grey o1num=500 o2num=${min2} d2num=${d2num} color=F wantscalebar=y newclip=1 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Magnitude' ${FIG}$@

${R}/phi-top-it-0.v: ${phi_path}comp
	Window3d ${sliceparT} n4=1 f4=0 < $< | Transp plane=12 > $@1
	Grey o1num=213000 d2num=${d2num} color=F newclip=1 title=" " label1='Inline [m]' label2='Crossline [m]' label3='Magnitude' < $@1 ${FIG}$@
	rm $@1

${R}/saltmask-side-it-0.v: ${masksalt}comp
	Window3d ${slicepar} n4=1 f4=0 < $< | Grey o1num=500 o2num=${min2} d2num=${d2num} color=F wantscalebar=y pclip=100 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Magnitude' ${FIG}$@

${R}/saltmask-side-it-2.v: ${masksalt}comp
	Window3d ${slicepar} n4=1 f4=2 < $< | Grey o1num=500 o2num=${min2} d2num=${d2num} color=F wantscalebar=y pclip=100 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Magnitude' ${FIG}$@

${R}/velmodel-side-it-${itf}.v: ${vel_path}comp
	Window3d ${slicepar}  n4=1 f4=${itf} < $< | Grey o1num=500 o2num=${min2} d2num=${d2num} color=jc wantscalebar=y newclip=1 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Velocity [m/s]' ${FIG}$@

${R}/velmodel-top-it-${itf}.v: ${vel_path}comp
	Window3d ${sliceparT} n4=1 f4=${itf} < $< > $@1
	Transp plane=12 < $@1 | Grey wantscalebar=y o1num=213000 d2num=${d2num} color=jc newclip=1 title=" " label1='Inline [m]' label2='Crossline [m]' label3='Velocity [m/s]' ${FIG}$@
	rm $@1

${R}/velmodel-diff-side-${itf}.v: ${vel_path}comp
	Window3d ${slicepar} n4=1 f4=0 < $< > $@0
	Window3d ${slicepar} n4=1 f4=${itf} < $< > $@1
	Add scale=1,-1 $@1 $@0 > $@2
	Grey < $@2 o1num=500 o2num=${min2} d2num=${d2num} color=F wantscalebar=y pclip=100 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Velocity difference [m/s]' ${FIG}$@
	rm $@0 $@1 $@2

${R}/velmodel-diff-top-${itf}.v: ${vel_path}comp
	Window3d ${sliceparT} n4=1 f4=0 < $< > $@0
	Window3d ${sliceparT} n4=1 f4=${itf} < $< > $@1
	Add scale=1,-1 $@1 $@0 | Transp plane=12 > $@2
	Grey < $@2 o1num=213000 d2num=${d2num} color=F wantscalebar=y pclip=100 title=" " label1='Inline [m]' label2='Crossline [m]' label3='Velocity difference [m/s]'  ${FIG}$@
	rm $@0 $@1 $@2

${R}/velback-diff-side-${itf}.v: ${velback}comp
	Window3d ${slicepar} n4=1 f4=0 < $< > $@0
	Window3d ${slicepar} n4=1 f4=${itf} < $< > $@1
	Add scale=1,-1 $@1 $@0 > $@2
	Grey < $@2 o1num=500 o2num=${min2} d2num=${d2num} color=F wantscalebar=y pclip=100 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Velocity difference [m/s]' ${FIG}$@
	rm $@0 $@1 $@2

${R}/velback-diff-top-${itf}.v: ${velback}comp
	Window3d ${sliceparT} n4=1 f4=0 < $< > $@0
	Window3d ${sliceparT} n4=1 f4=${itf} < $< > $@1
	Add scale=1,-1 $@1 $@0 | Transp plane=12 > $@2
	Grey < $@2 o1num=213000 d2num=${d2num} color=F wantscalebar=y pclip=100 title=" " label1='Inline [m]' label2='Crossline [m]' label3='Velocity difference [m/s]' ${FIG}$@
	rm $@0 $@1 $@2

##########################################################################################
sliceparINC=n3=1 min1=1000.0 max1=2795 min2=49000 max2=50000
sliceparTOP=n3=1 min1=500.0 max1=1500 min2=49000 max2=50000

# Vel model used in migration
${R}/inc-zone-vel-diff-%.v: extended_vel${itf}.H extended_vel0.H
	Add scale=1,-1 extended_vel${itf}.H extended_vel0.H | Window3d ${sliceparINC} min3=$* > $@1
	Grey < $@1 o1num=1200 o2num=49100 d2num=200 color=F wantscalebar=y pclip=100 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Velocity difference [m/s]' ${FIG}$@
	rm $@1

${R}/top-zone-vel-diff-%.v: extended_vel${itf}.H extended_vel0.H
	Add scale=1,-1 extended_vel${itf}.H extended_vel0.H | Window3d ${sliceparTOP} min3=$* > $@1
	Grey < $@1 o1num=600 o2num=49100 d2num=200 color=F wantscalebar=y pclip=100 title=" " label1='Depth [m]'  label2='Crossline [m]' label3='Velocity difference [m/s]' ${FIG}$@
	rm $@1

##########################################################################################

# Convert to pdfs
%.pdf: %.v
	pstexpen $< $*.ps color=y fat=y
	ps2pdf -dEPSCrop -dAutoFilterColorImages=false  -dColorImageFilter=/FlateEncode  -dAutoFilterGrayImages=false  -dGrayImageFilter=/FlateEncode  -dAutoFilterMonoImages=false  -dMonoImageFilter=/CCITTFaxEncode $*.ps $@
	rm $*.ps


