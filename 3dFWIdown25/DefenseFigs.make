

stacked_footprint.H:
	Window3d n4=1 < tmp/tmp_inv/RTMgrad.Hcomp | Transp plane=31 > $@

node_footprint1.v: stacked_footprint.H
        Window3d min1=212000 max1=218200 min2=45800 max2=52700 max3=-500 min3=-1200 < stacked_footprint.H | Transp plane=32 | Stack3d | Grey gainpanel=e title=" " label1="Inline[m]" label2="Crossline[m]" pclip=100 o1num=213000 d1num=2000 color=F >/dev/null out=$@

node_footprint2.v:
	Window3d min1=212000 max1=218200 min2=45800 max2=52700 max3=-500 min3=-1200 < ../3DdownMIGafter/RTMimageDOWN.31 | Transp plane=32 | Stack3d | Grey gainpanel=e title=" " label1="Inline[m]" label2="Crossline[m]" pclip=100 o1num=213000 d1num=2000 color=F >/dev/null out=$@



#### ALGORITHM SIDE PLOTS ####

tomo.v:
	Window3d f4=3 n4=1 min3=214800 n3=1 min1=1000.0 max1 min2=49000 max2=50000 < tmp/tmp_inv/tomograd.Hcomp  | Grey color=F wantscalebar=y title=" " label1="Depth[m]" label2="Crossline[m]" label3="Magnitude" gainpanel=e > /dev/null out=$@

rtm.v:
	Window3d f4=3 n4=1 min3=214800 n3=1 min1=1000.0 max1 min2=49000 max2=50000 < tmp/tmp_inv/RTMgrad.Hcomp  | Grey color=F wantscalebar=y title=" " label1="Depth[m]" label2="Crossline[m]" label3="Magnitude" gainpanel=e > /dev/null out=$@


phi.v:
	Window3d f4=3 n4=1 min3=214800 n3=1 min1=1000.0 max1 min2=49000 max2=50000 < tmp/tmp_inv/phi_path.Hcomp  | Grey color=F wantscalebar=y title=" " label1="Depth[m]" label2="Crossline[m]" label3="Magnitude" gainpanel=e > /dev/null out=$@


velback.v:
	Window3d f4=3 n4=1 min3=214800 n3=1 min1=1000.0 max1 min2=49000 max2=50000 < tmp/tmp_inv/vel_back_guess.Hcomp  | Grey color=F wantscalebar=y title=" " label1="Depth[m]" label2="Crossline[m]" label3="Magnitude" gainpanel=e > /dev/null out=$@


view_res_%.v:
	Sort3d nkeys=1 key1=OFFSET og1=0.0 ng1=100 dg1=100 < wrk/residual_cardamom-3dFWIdown25-$*.H  | Stack3d | Window3d min1=0 max1=5 | Grey title=" " label1="Time[s]" label2="Offset[m]" out=$@ >/dev/null

view_syn_%.v:
	Sort3d nkeys=1 key1=OFFSET og1=0.0 ng1=100 dg1=100 < wrk/syndataB4Res_$*.H  | Stack3d | Window3d min1=0 max1=5 | Grey title=" " label1="Time[s]" label2="Offset[m]"  out=$@ >/dev/null


view_obs_%.v:
	Sort3d nkeys=1 key1=OFFSET og1=0.0 ng1=100 dg1=100 < wrk/all_truedata-$*.H  | Stack3d | Window3d min1=0 max1=5 | Grey title=" " label1="Time[s]" label2="Offset[m]" out=$@ >/dev/null








