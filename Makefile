include ${SEPINC}/SEP.top
main=$(shell pwd)
include ParamMakefile

################################################################################

${DOCKER}:
	make --directory=docker environment.img
	mv docker/environment.img $@

################################################################################

EXE: ${DOCKER}
	make --directory=3dFWIdown25 $@
	make --directory=3DdownMIGsmooth $@

ER:
	make --directory=3dFWIdown25 $@
	make --directory=3DdownMIGsmooth $@
	make --directory=3DdownMIGbefore $@
	make --directory=3DdownMIGafter $@

NR:
	make --directory=3dFWIdown25 $@
	make --directory=3DdownMIGsmooth $@
	make --directory=3DdownMIGbefore $@
	make --directory=3DdownMIGafter $@

CR:
	make --directory=3dFWIdown25 $@
	make --directory=3DdownMIGsmooth $@
	make --directory=3DdownMIGbefore $@
	make --directory=3DdownMIGafter $@

################################################################################

include ${SEPINC}/SEP.bottom
