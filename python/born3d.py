#!/usr/bin/python

import time
import subprocess
import math
import copy
from batch_task_executor import *



class BORN(BatchTaskComposer):
    def __init__(self, param_reader, vel_path, prefix, adjointin, modelpath, dataname, debug, wavefield, dataAlreadySplit):
        BatchTaskComposer.__init__(self)

        # Read in the job parameters
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.wavelet = self.dict_args['wavelet']
        self.nshots = int(self.dict_args['nfiles'])
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.prefix = prefix
        self.vel_path = vel_path
        self.adjoint = int(adjointin)
        self.dataname = dataname
        self.modelpath = modelpath
        self.genpar = self.dict_args['genpar']
        self.rand_path = self.dict_args['rand_path']
        self.dataAlreadySplit=bool(dataAlreadySplit)

        self.singularity = self.dict_args['singularity']
        self.souhead = self.dict_args['souhead']
        self.recpos = self.dict_args['recpos']
        self.wavefield = bool(wavefield)
        self.abcs = self.dict_args['abcs']
        self.pad = int(self.dict_args['pad'])
        self.randpad = int(self.dict_args['randpad'])
        self.phaseOnly = int(self.dict_args['phaseOnly'])

        #--------------------------------------------------------------------------
        # Read in the node position list (since we propagate from the nodes)
        recpos=[]
        tmp=[]
        with open(self.recpos) as f:
            for i, line in enumerate(f):
                line = line.strip()
                tmp=line.split()
                recpos.append(tmp)
        self.recposlist=recpos

        # Create all subjobs info
        self.njobs = int(math.ceil(self.nshots/float(self.nsh_perjob)))
        self.subjobids = range(0, self.njobs)
        self.subjobfns_list = [None]*self.njobs
        self.subjobIND_list = range(self.ish_beg, self.nshots+1, self.nsh_perjob) # List of actual shot number (starting @ 0)

        # New arrays for
        self.shotjobids = range(1, self.nshots+1)
        self.shotjobfns_list = [None]*self.nshots

        # Make a list of all the files for each job (shots per job)
        for i in range(0,self.njobs):
            ish_start = self.subjobIND_list[i]
            nsh = min(self.nsh_perjob, self.nshots-ish_start)
            self.subjobfns_list[i] = [None]*nsh
            for j in range(0,nsh):
                souId=int(float(self.recposlist[ish_start+j][0]))
                self.subjobfns_list[i][j] = '%s/%s-%s.H' % (self.path_out,self.prefix,souId)
                self.shotjobfns_list[ish_start+j] = self.subjobfns_list[i][j]

        return
        



    def GetSubjobScripts(self, subjobid):
        # Return the scripts content for that subjob
        subjobfns = self.subjobfns_list[subjobid]

        # Build scripts that create the files designated in subjobfns
        scripts = []
        cnt = 0
        nf_act = len(subjobfns) # number of files

        # For each shot in the subjob, we create the command script segment
        for fn in subjobfns:
            shot = self.nsh_perjob*subjobid+cnt+1
            cnt += 1
            if cnt > nf_act: break
            print("Making commands for shot: %d: filename: %s " % ((shot-1),fn))
            shotscript,junk = self.GetShotScript(fn, shot)
            scripts.append(shotscript)

        return scripts, str(subjobid)



    def GetShotScript(self, fn, shot):

        nodeId=int(self.recposlist[shot-1][0])
        recZ=int(float(self.recposlist[shot-1][1]))
        recY=int(float(self.recposlist[shot-1][2]))
        recX=int(float(self.recposlist[shot-1][3]))

        synB4Res="%s/syndataB4Res_%d.Hmuted" % (self.path_out,nodeId)
        curwName="%s/curW_%s.H" % (self.path_out,nodeId)
        oldwName="%s/oldW_%s.H" % (self.path_out,nodeId)
        singleNodeData = "%s/singleNode_%s.H" % (self.path_out,nodeId)
        acqHeader = "%s/acqHeader_%s.H" % (self.path_out,nodeId)
        randomABC = "%s/randomABC_%d.H" % (self.rand_path,nodeId)
        newvel="%s/velwABCs_%d.H" % (self.path_out,nodeId)
        testvel="%s/testvel_%s.H" % (self.path_out,nodeId)
        #
        padvel="Pad extend=0 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s" % (self.randpad,self.randpad,self.randpad,self.randpad,self.randpad,self.randpad,self.vel_path,newvel)
        makenewvel="Add to_stdout=0 %s %s >/dev/null" % (newvel,randomABC)
        #
        operator = "%s %s/BORN3d.x" % (self.singularity,self.T_BIN)
        inputs1 = 'par=%s vel=%s rec=%s@@ curW=%s oldW=%s abc=%s' % (self.genpar,newvel,acqHeader,curwName,oldwName,self.abcs)
        inputs2 = 'souZ=%d souX=%d souY=%d wavelet=%s' % (recZ,recX,recY,self.wavelet)
        header_obs_select = "%s/Window_key maxsize=100000 synch=1 key1='nodeId' mink1=%s maxk1=%s < %s > /dev/null hff=%s@@" % (self.T_BIN,nodeId,nodeId,self.souhead,acqHeader)
        
        # Write wavefields?
        if (self.wavefield):
            waveC="wavefield=1 waveS=%s.Swave waveR=%s.Rwave waveI=%s.Iwave" % (fn,fn,fn)
        else:
            waveC="wavefield=0"



        if self.adjoint==1:
            
            # Select input mode of data
            if (self.dataAlreadySplit):
                data_select = "Cp %s/%s-%s.H %s1 hff=%s1@@" % (self.path_out,self.dataname,nodeId,singleNodeData,singleNodeData)
            else:
                data_select = "%s/Window_key synch=1 key1='nodeId' mink1=%s maxk1=%s < %s > %s1 hff=%s1@@" % (self.T_BIN,nodeId,nodeId,self.dataname,singleNodeData,singleNodeData)

            # Normalize the input data?
            if(self.phaseOnly==1):
                resNormalization = "%s/KINEMATIC_VSRC_OP.x adjoint=1 < %s1 syntraces=%s > %s " % (self.T_BIN,singleNodeData,synB4Res,singleNodeData)
            else:
                resNormalization = "Cp %s1 %s " % (singleNodeData,singleNodeData)

            removeheader = "echo 'hff=-1'>> %s" % (fn)
            outputs = 'adj=1 testvel=%s < %s > %s ; %s' % (testvel, singleNodeData, fn, removeheader)
            headerinit = '  '
            headerwrite = " "

            # Assemble final command
            script = '%s; %s; %s; %s; %s; %s %s %s %s %s; %s; %s \n' % (header_obs_select, padvel, makenewvel, data_select, resNormalization, operator, waveC, inputs1, inputs2, outputs, headerinit, headerwrite) # Note: need the \n at the end... also cant use < in string itself.
            # Note: need the \n at the end... also cant use < in string itself.

        else:
            if(self.phaseOnly==1):
                resNormalization = "%s/KINEMATIC_VSRC_OP.x adjoint=0 < %s1 syntraces=%s > %s " % (self.T_BIN,fn,synB4Res,fn)
            else:
                resNormalization = "Cp %s1 %s; " % (fn,fn)

            # Since the sources are the receiver locations, I call keys 1-3 source positions and 4-6 reciever positions
            header1 = 'echo "hdrkey1=nodeId hdrtype1=scalar_float hdrfmt1=xdr_float" >> %s@@' % (fn)
            header2 = 'echo "hdrkey2=SZ     hdrtype2=scalar_float hdrfmt2=xdr_float" >> %s@@' % (fn)
            header3 = 'echo "hdrkey3=SY     hdrtype3=scalar_float hdrfmt3=xdr_float" >> %s@@' % (fn)
            header4 = 'echo "hdrkey4=SX     hdrtype4=scalar_float hdrfmt4=xdr_float" >> %s@@' % (fn)
            header5 = 'echo "hdrkey5=GZ     hdrtype5=scalar_float hdrfmt5=xdr_float" >> %s@@' % (fn)
            header6 = 'echo "hdrkey6=GY     hdrtype6=scalar_float hdrfmt6=xdr_float" >> %s@@' % (fn)
            header7 = 'echo "hdrkey7=GX     hdrtype7=scalar_float hdrfmt7=xdr_float" >> %s@@' % (fn)
            header8 = 'echo "hdrkey8=OFFSET hdrtype8=scalar_float hdrfmt8=xdr_float" >> %s@@' % (fn)
            headerwrite = "%s; %s; %s; %s; %s; %s; %s; %s;" % (header1, header2, header3, header4, header5, header6, header7, header8)
            data_select = ''
            outputs = 'adj=0 < %s > %s1 header=%s@@' % (self.modelpath,fn,fn)
            headerinit = 'echo "gff=-1 hff=%s@@" >> %s' % (fn, fn)

            # Assemble final command
            script = '%s; %s; %s; %s; %s %s %s %s %s; %s; %s; %s \n' % (header_obs_select, padvel, makenewvel, data_select, operator, waveC, inputs1, inputs2, outputs, resNormalization, headerinit, headerwrite) # Note: need the \n at the end... also cant use < in string itself.
            # Note: need the \n at the end... also cant use < in string itself.

        return script, str(shot)


    def GetSubjobsInfo(self):
        '''Should return two lists, subjobids and subjobfns.'''
        return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)





class make_BORN(object):
    def __init__(self, param_reader):
        dict_args = param_reader.dict_args
        self.vel_path = dict_args['vel_path']
        self.path_out = dict_args['path_out']
        self.pypath = dict_args['pypath']
        self.T_BIN = dict_args['T_BIN']
        return


    def BORN_run(self, param_reader,prefix,cleanup,debug,adjoint,model,dataname,wavefield,dataAlreadySplit):
        #----- RUN BORN OPERATION -------------------
        bornjob = BORN(param_reader, self.vel_path, prefix, adjoint, model, dataname, debug, wavefield, dataAlreadySplit)
        bte = BatchTaskExecutor(param_reader)
        if (debug):
            print('-----------------------------------------------------------------')
            print(' RUNNING BORN OPERATOR')
        param_reader.prefix=prefix
        bte.LaunchBatchTask(prefix, bornjob)

        #----- Group the output files -------------
        _, fns_list = bornjob.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]
        args=''
        for i in range(0,len(fn_seph_list)):
            args = '%s %s' % (args,fn_seph_list[i])

        #----- Cat or Stack depending on the adjoint flag -------------
        if (adjoint==1):
            cmd = "python %s/recursive_add.py %s %s '%s/%s-*.H | sort -V' " % (self.pypath,self.T_BIN,model,self.path_out,prefix)
        else:
            if (dataAlreadySplit==1):
                cmd = " "
            else:
                # cmd = "Cat3d axis=2 max_memory=10000000 %s > %s hff=%s@@" % (args,dataname,dataname)
                cmd = "python %s/recursive_cat.py 3 %s '%s/%s-*.H | sort -V' " % (self.pypath,dataname,self.path_out,prefix)
        if (debug):
            print('-----------------------------------------------------------------')
            print(cmd)
        subprocess.call(cmd,shell=True)

        #----- Removing ADJOINT and FORWARD BORN RBF output files from wrk directory -------------
        if (int(cleanup)==1):
            junk=''
            for i in range(0,len(fn_seph_list)):
              junk = '%s %s' % (junk,fn_seph_list[i])
            cmd = 'rm -rf %s' % (junk)
            subprocess.call(cmd,shell=True,stdout=False)
            print "--------- Removing ADJOINT and FORWARD BORNRBF output files from wrk directory  ------------------------------"
            print(cmd)
        return



if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)
    dict_args = param_reader.dict_args

    #------- MAKE THE MODELED DATA ----------------
    start_time = time.time()
    born_obj=make_BORN(param_reader)

    cleanup=False
    wavefield=False
    debug=True

    prefix=dict_args['prefix']
    adjoint=dict_args['adjoint']
    model=dict_args['model']
    dataname=dict_args['dataname']
    dataAlreadySplit=dict_args['dataAlreadySplit']

    print(dataAlreadySplit)
    born_obj.BORN_run(param_reader,prefix,cleanup,debug,adjoint,model,dataname,wavefield,dataAlreadySplit)

    elapsed_time = (time.time() - start_time)/60.0
    print("ELAPSED TIME FOR BORN (minutes) : ")
    print(elapsed_time)
