#!/usr/local/bin/python


#===========================================================================
#
#   Make 3D modeled data by running PBS script jobs that use a singularity
#   runtime environment. 
#
#   FORWARD: wave propagation modeling from source
#   ADJOINT: source modeling from data
#
#===========================================================================
import time
import math
import pbs_util
import subprocess
import math, copy
from batch_task_executor import *



class data_modeling(BatchTaskComposer):
  def __init__(self, param_reader, vel_path, prefix, jobtype, model, data, adjoint):
      BatchTaskComposer.__init__(self)

#######################################################################################################
      # jobtype =  'standard'       Runs one wave propagation; Uses ABCs, and doesn't calculate the residual or save the last two wavefields. Used for source inversion. Deletes the PBS output files for the inversion's sake.
      # jobtype =  'rtm'            Runs two wave propagation; one for the current and old wavefield for RTM, and one to get the data residual. Also calculates the data residual (using ABCs). Also self corrects for the number of shots per job.
      # jobtype =  'mig'            Runs one wave propagation; Uses random ABCs.
      # jobtype =  'linesearch'     Runs one wave propagation; Uses ABCs, and calculates the data residuals.
#######################################################################################################
        
      # Read in the job parameters
      self.dict_args = param_reader.dict_args
      self.T_BIN = self.dict_args['T_BIN']
      self.vel_path = vel_path
      self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
      self.ish_beg = int(self.dict_args['ish_beg'])
      self.path_out = param_reader.path_out
      self.rand_path = self.dict_args['rand_path']
      self.prefix = prefix
      self.genpar = self.dict_args['genpar']
      self.writesrcwave = int(self.dict_args['writesrcwave'])

      self.singularity = self.dict_args['singularity']
      self.recpos = self.dict_args['recpos']
      self.abcs = self.dict_args['abcs']
      self.souhead = self.dict_args['souhead']
      self.jobtype = jobtype
      self.adjoint = int(adjoint)
      self.model = model
      self.data = data
      self.obs_prefix = self.dict_args['obs_prefix']
      self.shot_divide = int(self.dict_args['shot_divide'])
      self.pad = int(self.dict_args['pad'])

      if (self.jobtype=='sourceinversion'):
					self.nt = int(self.dict_args['nt'])

      # Subsample the shots if a linesearch job
      if (self.jobtype!='linesearch'):
          self.shot_divide=1
      else:
          self.shot_divide=1
          self.phaseOnly = int(self.dict_args['phaseOnly'])
          # self.shot_divide=2

      if (self.jobtype=='rtm'):
          self.phaseOnly = int(self.dict_args['phaseOnly'])

      # Read in the rec position list to get the number of synthetic wave props to do
      self.recposlist=[]
      tmp=[]
      self.numrecpos=0
      with open(self.recpos) as f:
          for i, line in enumerate(f):
              aa=self.numrecpos%self.shot_divide # Allows for subsampling shots (useful for making linesearches more efficient)
              if(aa==0):
                  line = line.strip()
                  tmp=line.split()
                  self.recposlist.append(tmp)
              self.numrecpos+=1
      self.nshots = len(self.recposlist)

      # Since the RTM job runs twice the number of wave props (one for the residual and one for the curW and oldW for RTM)
      if (self.jobtype=='rtm'):
          self.nsh_perjob = max(int(math.floor(self.nsh_perjob/2.0)),1)

      # Create all subjobs info
      print("\n\n NUMBER OF SHOTS = %d \n" % (self.nshots))
      print(float(self.nsh_perjob))
      self.njobs = int(math.ceil(self.nshots/float(self.nsh_perjob)))
      self.subjobids = range(0, self.njobs)
      self.subjobfns_list = [None]*self.njobs
      self.subjobIND_list = range(self.ish_beg, self.nshots+1, self.nsh_perjob) # List of actual shot number (starting @ 0)

      # Make a list of all the files for each job (shots per job)
      for i in range(0,self.njobs):
          ish_start = self.subjobIND_list[i]
          nsh = min(self.nsh_perjob, self.nshots-ish_start)
          self.subjobfns_list[i] = [None]*nsh
          for j in range(0,nsh):
              souId=int(float(self.recposlist[ish_start+j][0]))
              self.subjobfns_list[i][j] = '%s/%s-%s.H' % (self.path_out,self.prefix,souId)
              # self.subjobfns_list[i][j] = '%s/%s-%s.H' % (self.path_out,self.prefix,ish_start+j+1)
      return


  def GetSubjobScripts(self, subjobid):
      #'Return the scripts content for that subjob.'
      subjobfns = self.subjobfns_list[subjobid]
      # Build scripts that create the files designated in subjobfns
      scripts = []
      srcwaveout=" "
      cnt = 1
      nf_act = len(subjobfns) # number of files per job

      # Loop over all shots
      for fn in subjobfns:
          shot = self.nsh_perjob*(subjobid)+cnt
          if(shot > self.numrecpos):
              print(">>>>>> You have more PBS shots than you have actual shot positions!\n")
              print(">>>>>> PBS shot # = %d:   # of shot positions = %d \n" % (shot,numrecpos) )
              break
          if cnt > nf_act: break
          cnt += 1

          # common params
          souId=int(float(self.recposlist[shot-1][0]))
          souZ=int(float(self.recposlist[shot-1][1]))
          souY=int(float(self.recposlist[shot-1][2]))
          souX=int(float(self.recposlist[shot-1][3]))

          # Common names
          randomABC = "%s/randomABC_%d.H" % (self.rand_path,souId)
          newvelRBCs="%s/velwrandABCs_%d.H" % (self.path_out,souId)
          newvel="%s/velnoRandABCs_%d.H" % (self.path_out,souId)
          nodeHeader = "%s/nodeHeader_%s.H" % (self.path_out,souId)

          # Common commands
          operator = "%s %s/main3d.x" % (self.singularity,self.T_BIN)

          if (self.jobtype=='sourceinversion'):
              param2 = 'souZ=%d souX=%d souY=%d nt=%d' % (souZ,souX,souY,self.nt)
          else:
              param2 = 'souZ=%d souX=%d souY=%d ' % (souZ,souX,souY)


          shot_select = "%s/Window_key synch=1 key1='nodeId' mink1=%s maxk1=%s < %s > /dev/null hff=%s" % (self.T_BIN,souId,souId,self.souhead,nodeHeader)

          # Get the Z,X,Y position in a list format
          if (self.adjoint==1):
              scripts.append(shot_select+';\n')

              # Common adjoint names
              indata="%s/indata-%d.H" % (self.path_out,souId)
              # Common adjoint commands
              param1 = 'souId=%s cfl=0 rec=%s abc=%s adj=1' % (souId,nodeHeader,self.abcs)
              data_shot_select = "%s/Window_key synch=1 key1='nodeId' mink1=%s maxk1=%s < %s > %s hff=%s@@; " % (self.T_BIN,souId,souId,self.data,indata,indata)
              nohff = " echo 'hff=-1' >> %s " % (fn)
              makenewvel="Pad extend=1 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s" % (self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,self.vel_path,newvel)
              io = '< %s > %s' % (indata,fn)

              if (self.jobtype=='standard'):
                  # Assemble regular wave-prop    (No ABCs, no curW or oldW writeout, and with header info) 
                  optional = "vel=%s rtm=0 curW=/dev/null oldW=/dev/null useabcs=0" % (newvel)
                  cmdMAIN = '%s; %s; %s %s %s %s %s; %s \n' % (makenewvel, data_shot_select, operator, param1, param2, optional, io, nohff)
                  scripts.append(cmdMAIN)
              elif (self.jobtype=='sourceinversion'):
                  # Assemble regular wave-prop    (with ABCs, no curW or oldW writeout, and with header info) 
                  optional = "vel=%s rtm=0 curW=/dev/null oldW=/dev/null useabcs=1" % (newvel)
                  cmdMAIN = '%s; %s; %s %s %s %s %s; %s \n' % (makenewvel, data_shot_select, operator, param1, param2, optional, io, nohff)
                  scripts.append(cmdMAIN)
              else:
                  print("\n Adjoint not defined for jobtype = %s \n" % (self.jobtype))
          



          else:
              # Common forward names
              beforeRes="%s/syndataB4Res_%d.H" % (self.path_out,souId)
              obsSoudata="%s/%s-%d.H" % (self.path_out,self.obs_prefix,souId)
              curwName="%s/curW_%s.H" % (self.path_out,souId)
              oldwName="%s/oldW_%s.H" % (self.path_out,souId)
              # Common forward commands
              param1 = 'souId=%s par=%s cfl=0 rec=%s abc=%s adj=0' % (souId,self.genpar,nodeHeader,self.abcs)
              # Write wavefield if asked
              if (self.writesrcwave):
                  srcwaveout = 'wavefield=1 wave=%s.wave' % (fn)
              else:
                  srcwaveout = " "
              # Header commands (Since the sources are the receiver locations, I call keys 1-3 source positions and 4-6 reciever positions)
              header1 = 'hdrkey1=nodeId hdrtype1=scalar_float hdrfmt1=xdr_float'
              header2 = 'hdrkey2=SZ hdrtype2=scalar_float hdrfmt2=xdr_float'
              header3 = 'hdrkey3=SY hdrtype3=scalar_float hdrfmt3=xdr_float'
              header4 = 'hdrkey4=SX hdrtype4=scalar_float hdrfmt4=xdr_float'
              header5 = 'hdrkey5=GZ hdrtype5=scalar_float hdrfmt5=xdr_float'
              header6 = 'hdrkey6=GY hdrtype6=scalar_float hdrfmt6=xdr_float'
              header7 = 'hdrkey7=GX hdrtype7=scalar_float hdrfmt7=xdr_float'
              header8 = 'hdrkey8=OFFSET hdrtype8=scalar_float hdrfmt8=xdr_float'

              # Grab the right header info for acquisition. Needed for any job type.
              scripts.append(shot_select+';\n')
        

              if (self.jobtype=='rtm'):
                  self.obs_path=self.dict_args['obs_path']
                  self.pypath = self.dict_args['pypath']
                  self.randpad = int(self.dict_args['randpad'])
                  self.dz = self.dict_args['dz']
                  # Assemble wave-prop for RTM stuff   (no ABCs, with curW and oldW writeout)
                  headerall = 'echo "%s %s %s %s %s %s %s %s" >> %s@@ ; echo "gff=-1 hff=%s@@" >> %s' % (header1, header2, header3, header4, header5, header6, header7, header8, beforeRes, beforeRes, beforeRes)
                  makenewvel="Pad extend=0 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s1; Add %s1 %s > %s" % (self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,self.vel_path,newvel,newvel,randomABC,newvelRBCs)
                  optional = "vel=%s rtm=1 curW=%s oldW=%s useabcs=0" % (newvelRBCs,curwName,oldwName)
                  io = '< %s >/dev/null header=/dev/null' % (self.model)
                  cmdRTM = '%s; %s %s %s %s %s %s; \n' % (makenewvel, operator, param1, param2, optional, srcwaveout, io)
                  scripts.append(cmdRTM)
                  # Assemble regular wave-prop    (with ABCs, no curW or oldW writeout, and with header info) We run this second, so that the PBS submitter will wait for the output file from this last rather than skip ahead if the RTM job was still running. ( Note: need the \n at the end... also cant use < in string itself.)
                  makenewvel="Pad extend=1 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s2" % (self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,self.vel_path,newvel)
                  optional = "vel=%s2 rtm=0 curW=/dev/null oldW=/dev/null useabcs=1" % (newvel)
                  ioRes = '< %s > %s header=%s@@' % (self.model,beforeRes,beforeRes)
                  cmdMAIN = '%s; %s %s %s %s %s %s; %s;\n' % (makenewvel, operator, param1, param2, optional, srcwaveout, ioRes, headerall)
                  scripts.append(cmdMAIN)
                  # Assemble residual calculation
                  # cmdRES = 'Add %s %s scale=1,-1 > %s hff=%s@@; \n' % (obsSoudata,beforeRes,fn,fn)
                  obs_shot_select = "%s/Window_key synch=1 key1='nodeId' mink1=%s maxk1=%s < %s > %s hff=%s@@; " % (self.T_BIN,souId,souId,self.obs_path,obsSoudata,obsSoudata)
                  scripts.append(obs_shot_select)

                  cmdMUTEsyn = "%s/MUTE.x  < %s header=%s dz=%s > %smuted; " % (self.T_BIN,beforeRes,nodeHeader,self.dz,beforeRes)
                  # cmdMUTEsyn = "Cp %s %smuted; " % (beforeRes,beforeRes)
                  scripts.append(cmdMUTEsyn)
                  cmdMUTEobs = "%s/MUTE.x  < %s header=%s dz=%s > %smuted; " % (self.T_BIN,obsSoudata,nodeHeader,self.dz,obsSoudata)
                  # cmdMUTEobs = "Cp %s  %smuted; " % (obsSoudata,obsSoudata)
                  scripts.append(cmdMUTEobs)

#---------------------------------------------------------------------------------------------
                  if (self.phaseOnly==1):
                      # PHASE ONLY RESIDUAL CALCULATION
                      cmdRES = '%s/NORMALIZED_RESIDUAL.x < %smuted obsdata=%smuted > %s hff=%s@@; \n' % (self.T_BIN,beforeRes,obsSoudata,fn,fn)
                      scripts.append(cmdRES)
                      # tmpRes = "%s/tempRes%s.H" % (self.path_out,souId)
                      # cmdRES2 = '%s/NORMALIZED_RESIDUAL.x < %smuted obsdata=%smuted > %s hff=%s@@; \n' % (self.T_BIN,beforeRes,obsSoudata,tmpRes,tmpRes)
                      # cmdRES3 = '%s/KINEMATIC_VSRC_OP.x < %s syntraces=%smuted > %s hff=%s@@; \n' % (self.T_BIN,tmpRes,beforeRes,fn,fn)
                      # cmd = '%s %s' % (cmdRES2,cmdRES3)
                      # scripts.append(cmd)
                  else:
                      # STANDARD RESIDUAL CALCULATION
                      cmdRES = "Math file1=%smuted file2=%smuted exp='file1-file2' > %s  \n" % (obsSoudata,beforeRes,fn)
                      scripts.append(cmdRES)
#---------------------------------------------------------------------------------------------

                  # Assemble objective function calculation
                  cmdOBJ = 'python %s/calc_norm.py %s %s/%s_%s.txt; \n' % (self.pypath,fn,self.path_out,self.prefix,souId)
                  scripts.append(cmdOBJ)
              elif (self.jobtype=='standard'):
                  # Assemble regular wave-prop    (with ABCs, no curW or oldW writeout, and with header info) 
                  makenewvel="Pad extend=1 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s" % (self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,self.vel_path,newvel)
                  headerall = 'echo "%s %s %s %s %s %s %s %s" >> %s@@ ; echo "gff=-1 hff=%s@@" >> %s' % (header1, header2, header3, header4, header5, header6, header7, header8, fn, fn, fn)
                  io = '< %s > %s header=%s@@' % (self.model,fn,fn)
                  optional = "vel=%s rtm=0 curW=/dev/null oldW=/dev/null useabcs=1" % (newvel)
                  cmdMAIN = '%s; %s %s %s %s %s %s; %s; \n' % (makenewvel, operator, param1, param2, optional, srcwaveout, io, headerall)
                  scripts.append(cmdMAIN)
              elif (self.jobtype=='sourceinversion'):
                  # Assemble regular wave-prop    (with ABCs, no curW or oldW writeout, and with header info) 
                  makenewvel="Pad extend=1 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s" % (self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,self.vel_path,newvel)
                  headerall = 'echo "%s %s %s %s %s %s %s %s" >> %s@@ ; echo "gff=-1 hff=%s@@" >> %s' % (header1, header2, header3, header4, header5, header6, header7, header8, fn, fn, fn)
                  io = '< %s > %s header=%s@@' % (self.model,fn,fn)
                  optional = "vel=%s rtm=0 curW=/dev/null oldW=/dev/null useabcs=1" % (newvel)
                  cmdMAIN = '%s; %s %s %s %s %s %s; %s; \n' % (makenewvel, operator, param1, param2, optional, srcwaveout, io, headerall)
                  scripts.append(cmdMAIN)
              elif (self.jobtype=='linesearch'):
                  self.obs_path=self.dict_args['obs_path']
                  self.pypath = self.dict_args['pypath']
                  self.dz = self.dict_args['dz']
                  # Assemble regular wave-prop    (with ABCs, no curW or oldW writeout, and with header info) We run this second, so that the PBS submitter will wait for the output file from this last rather than skip ahead if the RTM job was still running. ( Note: need the \n at the end... also cant use < in string itself.)
                  makenewvel="Pad extend=1 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s" % (self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,self.vel_path,newvel)
                  # makenewvel="Pad extend=1 end1=%d beg2=%d end2=%d < %s > %s" % (self.pad,self.pad,self.pad,self.vel_path,newvel)
                  headerall = 'echo "%s %s %s %s %s %s %s %s" >> %s@@ ; echo "gff=-1 hff=%s@@" >> %s' % (header1, header2, header3, header4, header5, header6, header7, header8, beforeRes, beforeRes, beforeRes)
                  optional = "vel=%s rtm=0 curW=/dev/null oldW=/dev/null useabcs=1" % (newvel)
                  ioRes = '< %s > %s header=%s@@' % (self.model,beforeRes,beforeRes)
                  cmdMAIN = '%s; %s %s %s %s %s %s; %s;\n' % (makenewvel, operator, param1, param2, optional, srcwaveout, ioRes, headerall)
                  scripts.append(cmdMAIN)
                  # Assemble residual calculation
                  # cmdRES = 'Add %s %s scale=1,-1 > %s hff=%s@@; \n' % (obsSoudata,beforeRes,fn,fn)
                  obs_shot_select = "%s/Window_key synch=1 key1='nodeId' mink1=%s maxk1=%s < %s > %s hff=%s@@; " % (self.T_BIN,souId,souId,self.obs_path,obsSoudata,obsSoudata)
                  scripts.append(obs_shot_select)

                  cmdMUTEsyn = "%s/MUTE.x  < %s header=%s dz=%s > %smuted; " % (self.T_BIN,beforeRes,nodeHeader,self.dz,beforeRes)
                  # cmdMUTEsyn = "Cp %s %smuted; " % (beforeRes,beforeRes)
                  scripts.append(cmdMUTEsyn)
                  cmdMUTEobs = "%s/MUTE.x  < %s header=%s dz=%s > %smuted; " % (self.T_BIN,obsSoudata,nodeHeader,self.dz,obsSoudata)
                  # cmdMUTEobs = "Cp %s  %smuted; " % (obsSoudata,obsSoudata)
                  scripts.append(cmdMUTEobs)

#---------------------------------------------------------------------------------------------
                  if (self.phaseOnly==1):
                      # PHASE ONLY RESIDUAL CALCULATION
                      cmdRES = '%s/NORMALIZED_RESIDUAL.x < %smuted obsdata=%smuted > %s hff=%s@@; \n' % (self.T_BIN,beforeRes,obsSoudata,fn,fn)
                      scripts.append(cmdRES)
                      # tmpRes = "%s/tempRes%s.H" % (self.path_out,souId)
                      # cmdRES2 = '%s/NORMALIZED_RESIDUAL.x < %smuted obsdata=%smuted > %s hff=%s@@; \n' % (self.T_BIN,beforeRes,obsSoudata,tmpRes,tmpRes)
                      # cmdRES3 = '%s/KINEMATIC_VSRC_OP.x < %s syntraces=%smuted > %s hff=%s@@; \n' % (self.T_BIN,tmpRes,beforeRes,fn,fn)
                      # cmd = '%s %s' % (cmdRES2,cmdRES3)
                      # scripts.append(cmd)
                  else:
                      # STANDARD RESIDUAL CALCULATION
                      cmdRES = "Math file1=%smuted file2=%smuted exp='file1-file2' > %s  \n" % (obsSoudata,beforeRes,fn)
                      scripts.append(cmdRES)
#---------------------------------------------------------------------------------------------

                   # Assemble objective function calculation
                  cmdOBJ = 'python %s/calc_norm.py %s %s/%s_%s.txt; \n' % (self.pypath,fn,self.path_out,self.prefix,souId)
                  scripts.append(cmdOBJ)
              elif (self.jobtype=='mig'):
                  self.randpad = int(self.dict_args['randpad'])
                  # Assemble wave-prop for RTM stuff   (no ABCs, with curW and oldW writeout)
                  headerall ='echo "%s %s %s %s %s %s %s %s" >> %s@@ ; echo "gff=-1 hff=%s@@" >> %s' % (header1, header2, header3, header4, header5, header6, header7, header8, fn, fn, fn)
                  makenewvel="Pad extend=0 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s; Add %s %s > %s" % (self.randpad,self.randpad,self.randpad,self.randpad,self.randpad,self.randpad,self.vel_path,newvel,newvel,randomABC,newvelRBCs)
                  optional = "vel=%s rtm=1 curW=%s oldW=%s useabcs=0" % (newvelRBCs,curwName,oldwName)
                  io = '< %s > %s header=%s@@' % (self.model,fn,fn)
                  cmdMIG = '%s; %s %s %s %s %s %s; %s \n' % (makenewvel, operator, param1, param2, optional, srcwaveout, io,headerall)
                  scripts.append(cmdMIG)
              elif (self.jobtype=='migmirror'):
                  self.randpad = int(self.dict_args['randpad'])
                  # Assemble wave-prop for RTM stuff   (no ABCs, with curW and oldW writeout)
                  headerall ='echo "%s %s %s %s %s %s %s %s" >> %s@@ ; echo "gff=-1 hff=%s@@" >> %s' % (header1, header2, header3, header4, header5, header6, header7, header8, fn, fn, fn)
                  makenewvel="Pad extend=0 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s; Add %s %s > %s" % (self.randpad,self.randpad,self.randpad,self.randpad,self.randpad,self.randpad,self.vel_path,newvel,newvel,randomABC,newvelRBCs)
                  optional = "vel=%s rtm=1 curW=%s oldW=%s useabcs=0" % (newvelRBCs,curwName,oldwName)
                  io = '< %s > %s header=%s@@' % (self.model,fn,fn)
                  cmdMIG = '%s; %s %s %s %s %s %s; %s \n' % (makenewvel, operator, param1, param2, optional, srcwaveout, io,headerall)
                  scripts.append(cmdMIG)

      return scripts, str(subjobid)


  def GetSubjobsInfo(self):
      '''Should return two lists, subjobids and subjobfns.'''
      return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)




class make_MODELED_DATA3d(object):
  def __init__(self, param_reader):
      dict_args = param_reader.dict_args
      self.path_out = param_reader.path_out
      self.singularity = dict_args['singularity']
      # self.singularity = param_reader.singularity
      return


  def CAT_FILES(self, inputfiles, outputFileName):
      # Initialize
      sublist=[]
      ListOfVirtualCats=[]
      checklist=[]
      subCount=0
      bigCount=0
      allFileCount=1
      threshold=30
      # Make virtual sub-Cats of subsets of the files
      for thisFile in inputfiles:
          if((subCount==threshold) or (allFileCount==len(inputfiles))):
              # Add the last file
              sublist.append(thisFile)
              subCount=subCount+1
              # Cat the files together
              checklist=checklist+sublist
              smallListOfFiles = ' '.join(map(str, sublist))
              bigfilename="%s%s" % (outputFileName,bigCount)
              cmd = "Cat3d max_memory=1000 axis=2 virtual=1 %s hff=%s@@ > %s" % (smallListOfFiles,bigfilename,bigfilename)
              # cmd = "%s Cat3d max_memory=1000 axis=2 virtual=1 %s hff=%s@@ > %s" % (self.singularity,smallListOfFiles,bigfilename,bigfilename)
              print("==============================================")
              print(cmd)
              commands.getoutput(cmd)
              ListOfVirtualCats.append(bigfilename)
              bigCount=bigCount+1
              # Zero the list and count
              subCount=0
              sublist=[]
          else:
              sublist.append(thisFile)
              subCount=subCount+1
          allFileCount=allFileCount+1

      # Do the final cat of the virtual sub-Cats
      StringOfVirtualCats = ' '.join(map(str, ListOfVirtualCats))
      final="%s Cat3d axis=2 %s > %s hff=%s@@" % (self.singularity,StringOfVirtualCats,outputFileName,outputFileName)
      print(final)
      commands.getoutput(final)

      # Remove the subcats
      rmcmd="rm -f %s" % (StringOfVirtualCats)
      print(rmcmd)
      commands.getoutput(rmcmd)

      return




  def MODELED_DATA(self, param_reader, debug, prefix, jobtype, vel_path, adjoint, model, data):

      if (int(adjoint)==1):
          #----- Adjoint wave modeling -------------------
          adjoint_modeling = data_modeling(param_reader,vel_path,prefix,jobtype,model,data,adjoint)
          bte = BatchTaskExecutor(param_reader)
          if (debug):
              print('-----------------------------------------------------------------')
              print('ADJOINT MODELING')
          param_reader.prefix=prefix
          bte.LaunchBatchTask(prefix, adjoint_modeling)
          # Get the list of file names
          _, fns_list = adjoint_modeling.GetSubjobsInfo()
          fn_seph_list = [fn for fns in fns_list for fn in fns]
          # Combine the files to one single file.
          args=''
          for i in range(0,len(fn_seph_list)):
            args = '%s %s' % (args,fn_seph_list[i])
          nf=len(fn_seph_list)
          cmd = "Cat3d max_memory=10000 axis=2 %s | Stack3d maxsize=10000 > %s" % (args,model)
          print(cmd)
          commands.getoutput(cmd)
      else:
          #----- Forward wave modeling -------------------
          forward_modeling = data_modeling(param_reader,vel_path,prefix,jobtype,model,data,adjoint)
          bte = BatchTaskExecutor(param_reader)
          if (debug):
              print('-----------------------------------------------------------------')
              print('FORWARD MODELING')
          param_reader.prefix=prefix
          bte.LaunchBatchTask(prefix, forward_modeling)

          # Get the list of file names
          if (jobtype=='sourceinversion'):
              _, fns_list = forward_modeling.GetSubjobsInfo()
              fn_seph_list = [fn for fns in fns_list for fn in fns]
              print("catting files? \n")
              self.CAT_FILES(fn_seph_list,data)

      #----- Removing ADJOINT and FORWARD modeling output files from wrk directory -------------
      if (jobtype=='sourceinversion'):
          arg_wave = ''
          for i in range(0, len(fn_seph_list)):
              arg_wave = '%s %s' % (arg_wave, fn_seph_list[i])
          cmd1 = 'rm -rf %s' % (arg_wave)
          cmd2 = 'rm -rf %s/%s*' % (self.path_out,prefix)
          cmd = '%s; %s;' % (cmd1, cmd2)
          subprocess.call(cmd, shell=True)
          print "--------- Removing ADJOINT and FORWARD source inversion output files from wrk directory  ------------------------------"
          print(cmd)

      return





if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)
    start_time = time.time()

    #-------- Parameters -----------------------------------
    dict_args = param_reader.dict_args
    vel_path = dict_args['vel_path']
    data = dict_args['data']
    model = dict_args['model']
    prefix = dict_args['prefix']
    adjoint = int(dict_args['adjoint'])
    jobtype = dict_args['jobtype']

    #------- RUN THE WAVE MODELING OPERATOR ----------------
    waveModeling=make_MODELED_DATA3d(param_reader)
    waveModeling.MODELED_DATA(param_reader, True, prefix, jobtype, vel_path, adjoint, model, data)

    elapsed_time = (time.time() - start_time)/60.0
    print("ELAPSED TIME FOR MODELED DATA (minutes) : ")
    print(elapsed_time)
