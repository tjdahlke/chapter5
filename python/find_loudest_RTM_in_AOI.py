

# Find RTM images that cross correlate the strongest with an ideal image in the given AOI
# Taylor Dahlke 8/24/2018


import sepbase
import sep2npy
import sys,os,math
import commands
import time
import matplotlib.pyplot as plt
import numpy as np
from operator import itemgetter



# Function to check is file exists
def check_file(file):
	# Check if file exists
	cmd0 = "ls %s " % file
	stat0,out0=commands.getstatusoutput(cmd0)
	if (int(stat0) != 0):  # File does not exist
		print("File does not exist")
		return 0
	else:
		return 1


# MAIN PROGRAM
if __name__ == '__main__':
	eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
	dict_args = eq_args_from_cmdline

	################  PARAMETERS ################
	# List of nodes and relative positions
	recpos = dict_args['recpos']
	# PBS template
	PBStemp = dict_args['PBStemp']
	rtmpath = dict_args['rtmpath']
	# Output path for PBS stuff
	outpath = dict_args['outpath']
	runpath = dict_args['runpath']
	pypath = dict_args['pypath']
	binpath = dict_args['binpath']
	cutoffpercentL = float(dict_args['cutoffpercentL'])
	cutoffpercentH = float(dict_args['cutoffpercentH'])
	stackedfile = dict_args['stackedfile']
	idealAOI = dict_args['idealAOI']

	# Initialize
	reclist=[]
	cmd1="less %s | wc -l" % (recpos)
	stat1,numNodes=commands.getstatusoutput(cmd1)
	numNodes=int(numNodes)
	recList = list(open(recpos))
	someNodesNotDone=True
	numTries=5


	#--------------------------------------------
	# First make the AOI files
	tries=0
	while(someNodesNotDone):

		someNodesNotDone=False

		for ii in range(0,numNodes):

			nodeId=int(str(recList[ii]).split()[0])
			aoifile='%s/aoi_RTM_%d.H' % (outpath,nodeId)

			makeAOI = 'rm -f %s; make %s' % (aoifile,aoifile)

			# Check if the file is already made
			if (sepbase.CheckSephFileError(aoifile)==0):
				print("File: %s already exists \n" % (aoifile))
			else:
				someNodesNotDone=True
				# Make a qsub file
				pbsfile="%s/PBS_AOI_%d.sh" % (outpath,nodeId)

				cmdp3="echo '#PBS -N pbsjob-%s' >> %s" % (nodeId,pbsfile)
				cmdp4="echo '#PBS -d %s' >> %s" % (outpath,pbsfile)
				cmdp5="echo 'setenv DATAPATH %s/' >> %s" % (outpath,pbsfile)
				cmdp="%s; %s; %s" % (cmdp3, cmdp4, cmdp5)
				print(cmdp)
				stat2,junk2=commands.getstatusoutput(cmdp)

				cmd2="cp %s %s; echo '%s' >> %s " % (PBStemp,pbsfile,makeAOI,pbsfile)
				stat1,junk1=commands.getstatusoutput(cmd2)
				# Submit the qsub file
				cmd3="qsub -d %s -o %s.O -e %s.E %s" % (runpath,pbsfile,pbsfile,pbsfile)
				print(cmd3)
				stat2,junk2=commands.getstatusoutput(cmd3)

		if (someNodesNotDone):
			print("Waiting for jobs to finish")
			time.sleep(60)

		# Try some checks
		tries=tries+1
		if (tries>numTries):
			someNodesNotDone=False


	#------------------------------------------------------
	# Then get the dot product with the ideal version
	tries=0
	someNodesNotDone=True
	while(someNodesNotDone):

		someNodesNotDone=False

		for ii in range(0,numNodes):

			nodeId=int(str(recList[ii]).split()[0])
			dotlogfile="%s/dotvalue_%d.log" % (outpath,nodeId)
			aoifile='%s/aoi_RTM_%d.H' % (outpath,nodeId)

			dotAOI  = "python %s/calc_norm2.py %s %s %s " % (pypath,aoifile,idealAOI,dotlogfile)

			# Check if the file is already made
			if (check_file(dotlogfile)==1):
				print("File: %s already exists \n" % (dotlogfile))
			else:
				someNodesNotDone=True
				# Make a qsub file
				pbsfile="%s/PBS_DOT_%d.sh" % (outpath,nodeId)
				cmd2="cp %s %s" % (PBStemp,pbsfile)
				stat1,junk1=commands.getstatusoutput(cmd2)
				cmdp5="echo 'setenv DATAPATH %s/' >> %s" % (outpath,pbsfile)
				stat1,junk1=commands.getstatusoutput(cmdp5)
				cmd2="echo '%s' >> %s " % (dotAOI,pbsfile)
				stat1,junk1=commands.getstatusoutput(cmd2)

				# Submit the qsub file
				cmd3="qsub -o %s.O -e %s.E %s" % (pbsfile,pbsfile,pbsfile)
				print(cmd3)
				stat2,junk2=commands.getstatusoutput(cmd3)

		if (someNodesNotDone):
			print("Waiting for josb to finish")
			time.sleep(200)

		# Try some checks
		tries=tries+1
		if (tries>numTries):
			someNodesNotDone=False


	#--------------------------------------------------------------
	# List of nodes that are important DOT PRODUCT
	impNodes=[]
	for ii in range(0,numNodes):
		nodepair=()
		nodeId=int(str(recList[ii]).split()[0])
		dotlogfile="%s/dotvalue_%d.log" % (outpath,nodeId)
		dotVal = float(list(open(dotlogfile))[0])
		if (dotVal>0.0):
			nodepair=(nodeId,dotVal)
			impNodes.append(nodepair)


	sortedByDOT=sorted(impNodes, key=itemgetter(1),reverse=True)

	print(sortedByDOT)
	print(len(sortedByDOT))
	print(numNodes)

	numPosNodes=len(sortedByDOT)
	numNodesToGrabL=int((cutoffpercentL)*numPosNodes/100)
	numNodesToGrabH=int((cutoffpercentH)*numPosNodes/100)
	mostImportantNodes=sortedByDOT[numNodesToGrabH:numNodesToGrabL]


	#--------------------------------------------------------------
	# Write files to a list
	recursiveFileList='recursiveFileListDOT.txt'
	cmd1="rm %s" % (recursiveFileList)
	stat2,junk2=commands.getstatusoutput(cmd1)
	for nodepair in mostImportantNodes:
		nodeId=nodepair[0]
		filename='%s%s.H' % (rtmpath,nodeId)
		writefilename="echo '%s' >> %s" % (filename,recursiveFileList)
		stat2,junk2=commands.getstatusoutput(writefilename)

	#--------------------------------------------------------------
	# Write node numbers to a list
	nodefile_list='dotprod_nodelist_%s-%s.txt' % (cutoffpercentH,cutoffpercentL)
	cmd1="rm %s" % (nodefile_list)
	stat2,junk2=commands.getstatusoutput(cmd1)
	for nodepair in mostImportantNodes:
		nodeId=nodepair[0]
		writefilename="echo '%s' >> %s" % (nodeId,nodefile_list)
		stat2,junk2=commands.getstatusoutput(writefilename)

	#--------------------------------------------------------------
	# Recursively add those files now
	cmd1='python %s/recursive_add_file_list.py %s %s "%s" > recursiveADD.log' % (pypath,binpath,stackedfile,recursiveFileList)
	print(cmd1)
	stat2,junk2=commands.getstatusoutput(cmd1)















