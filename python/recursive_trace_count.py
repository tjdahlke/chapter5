#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM THAT COUNTS TRACES IN A GROUP OF INPUT FILES
#
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands

# Read the inputs
if len(sys.argv) < 1:
    print('Error: Not enough input args')
    sys.exit()
inputfiles = sys.argv[1:len(sys.argv)]


# Check the key range for each file
n2all=0
for thisFile in inputfiles:
    cmd = "Get parform=n n2 < %s " % (thisFile)
    # print(cmd)
    # print('===========================')
    stat,n2i=commands.getstatusoutput(cmd)
    n2all=n2all+int(n2i)
print(n2all)

