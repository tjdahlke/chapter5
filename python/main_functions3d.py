import subprocess
import numpy as np
import shlex
import os.path
import commands



class MAIN_FUNCTIONS(object):
    def __init__(self, param_reader):
        dict_args = param_reader.dict_args
        self.pathtemp = dict_args['pathtemp']
        self.path_out = dict_args['path_out']
        self.rtm_path= dict_args['rtm_path']
        self.res_path = dict_args['res_path']
        self.T_BIN = dict_args['T_BIN']
        self.vel_path = dict_args['vel_path']
        self.phigrad = dict_args['phigrad']
        self.tomograd = dict_args['tomograd']
        self.genpar = dict_args['genpar']
        self.scaling = dict_args['scaling']

        self.phi_path = dict_args['phi_path']
        self.velback = dict_args['velback']
        self.phi_path = dict_args['phi_path']
        self.rbf_path = dict_args['rbf_path']
        self.hess_out_path = dict_args['hess_out_path']
        self.pypath = dict_args['pypath']

        self.rbfmask = dict_args['rbfmask']
        self.masksalt = dict_args['masksalt']
        self.sparsemodel = dict_args['sparsemodel']
        self.vsalt = dict_args['vsalt']
        self.rbfcoord = dict_args['rbfcoord']
        self.rbftable = dict_args['rbftable']
        self.guide = dict_args['guide']
        self.watermask = dict_args['watermask']
        self.rbfmask = dict_args['rbfmask']

        self.pad=int(dict_args['pad'])
        self.randpad=int(dict_args['randpad'])

        self.nz=int(dict_args['nz'])
        self.nx=int(dict_args['nx'])
        self.ny=int(dict_args['ny'])

        self.nzr=int(dict_args['nzr'])
        self.nxr=int(dict_args['nxr'])
        self.nyr=int(dict_args['nyr'])

        self.fzr=int(dict_args['f1r'])
        self.fyr=int(dict_args['f2r'])
        self.fxr=int(dict_args['f3r'])

        self.fzR=self.fzr+self.randpad
        self.fxR=self.fxr+self.randpad
        self.fyR=self.fyr+self.randpad

        self.wavelet = dict_args['wavelet']
        self.kappa = dict_args['kappa']

        return


    def residual(self,debug,syn_path,obs_path,res_path):
        #----- MAKE RESIDUAL  ------------------------------------------
        cmd = "Math file1=%s file2=%s exp='(file1-file2)' > %s" % (syn_path,obs_path,res_path) # Need residual to mave meters in d2 and o2 for MUTE_DA to work properly
        if (debug):
            print('-----------------------------------------------------------------')
            print " COMPUTING RESIDUAL "
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def MAKE_SCALING2d(self, debug):
        #------- MAKE SCALING ------------------------------------------
        cmd="Math file1=%s exp='%s-file1'> %s1" % (self.velback,float(self.vsalt),self.scaling)
        cmdwin = "Window3d n1=%d f1=%d n2=%d f2=%d < %s1 > %s; rm %s1" % (
            self.nzr,self.fzr,self.nyr,self.fyr,self.scaling,self.scaling,self.scaling)
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE SCALING for 2D model')
            print(cmd)
            print(cmdwin)
        subprocess.call(cmd,shell=True)
        subprocess.call(cmdwin,shell=True)
        return


    def MAKE_SCALING(self, debug):
        #------- MAKE SCALING ------------------------------------------
        cmd="Math file1=%s exp='%s-file1'> %s1" % (self.velback,float(self.vsalt),self.scaling)
        cmdwin = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s1 > %s; rm %s1" % (
            self.nzr,self.fzr,self.nyr,self.fyr,self.nxr,self.fxr,self.scaling,self.scaling,self.scaling)
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE SCALING')
            print(cmd)
            print(cmdwin)
        subprocess.call(cmd,shell=True)
        subprocess.call(cmdwin,shell=True)
        return


    def COPY(self, debug, fileIN, fileOUT):
        #----- COPY A FILE  -------------------
        cmd1 = "Cp %s %s " % (fileIN,fileOUT)
        aa=commands.getstatusoutput(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('COPY FILE')
            print(cmd1)
        return


    def HADAMARD_PRODUCT(self, debug, file1, file2, fileOUT):
        #----- HADAMARD_PRODUCT TWO FILES  -------------------
        cmd1 = "Math_base file1=%s file2=%s exp='file1*file2' > %s " % (file1,file2,fileOUT)
        aa=commands.getstatusoutput(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('HADAMARD_PRODUCT OF TWO FILES (ELEMENT WISE MULTIPLICATION)')
            print(cmd1)
        return


    def WINDOW3D_PAD(self, debug, fileIN, fileOUT):
        #----- WINDOW THE PADDING OFF OF THE FILE -------------------
        cmdwin = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s;\n" % (
            self.nz,self.pad,self.ny,self.pad,self.nx,self.pad,fileIN,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('WINDOW THE ABC PADDING OFF OF THE 3D FILE')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return


    def PAD2D_RBF(self, debug, fileIN, fileOUT):
        #----- PAD A FILE ACCORDING FROM SMALL RBF AOI WINDOW TO FULL MODEL WITH NO ABCS  -------------------
        cmdwin = "Pad extend=0 n1out=%d beg1=%d n2out=%d beg2=%d < %s > %s datapath=%s@;\n" % (
            self.nz,self.fzr,self.ny,self.fyr,fileIN,fileOUT,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('PAD A 2D FILE ACCORDING FROM SMALL RBF AOI WINDOW TO FULL')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return


    def PAD2D_ABC(self, debug, fileIN, fileOUT):
        #----- PAD A FILE ACCORDING FROM FULL MODEL TO FULL MODEL WITH ABCS -------------------
        cmdwin = "Pad extend=1 end1=%d beg1=%d end2=%d beg2=%d < %s > %s datapath=%s@;\n" % (
            self.pad,self.pad,self.pad,self.pad,fileIN,fileOUT,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('PAD A FILE ACCORDING FROM FULL MODEL TO FULL MODEL WITH ABCS')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return


    def PAD3D_ABC(self, debug, fileIN, fileOUT):
        #----- PAD A FILE ACCORDING FROM FULL MODEL TO FULL MODEL WITH ABCS -------------------
        cmdwin = "Pad extend=1 end1=%d beg1=%d end2=%d beg2=%d end3=%d beg3=%d < %s > %s datapath=%s@; \n" % (
            self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,fileIN,fileOUT,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('PAD A FILE ACCORDING FROM FULL MODEL TO FULL MODEL WITH ABCS')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return


    def PAD3D_RBF(self, debug, fileIN, fileOUT):
        #----- PAD A FILE ACCORDING FROM SMALL RBF AOI WINDOW TO FULL MODEL WITH NO ABCS  -------------------
        cmdwin = "Pad extend=0 n1out=%d beg1=%d n2out=%d beg2=%d n3out=%d beg3=%d < %s > %s datapath=%s@;\n" % (
            self.nz,self.fzr,self.ny,self.fyr,self.nx,self.fxr,fileIN,fileOUT,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('PAD A 3D FILE ACCORDING FROM SMALL RBF AOI WINDOW TO FULL')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return


    def WINDOW3D_RBF(self, debug, fileIN, fileOUT):
        #----- WINDOW AN ABC PADDED FILE TO THE SMALL RBF AOI WINDOW -------------------
        cmdwin = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s;\n" % (
            self.nzr,self.fzR,self.nyr,self.fyR,self.nxr,self.fxR,fileIN,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('WINDOW A 3D FILE ACCORDING TO THE SMALL RBF AOI WINDOW')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return



    def WINDOW2D_RBF(self, debug, fileIN, fileOUT):
        #----- WINDOW A FILE ACCORDING TO THE SMALL RBF AOI WINDOW -------------------
        cmdwin = "Window3d n1=%d f1=%d n2=%d f2=%d < %s > %s;\n" % (
            self.nzr,self.fzR,self.nyr,self.fyR,fileIN,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('WINDOW A 2D FILE ACCORDING TO THE SMALL RBF AOI WINDOW')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return

    def WINDOW2D_PAD(self, debug, fileIN, fileOUT):
        #----- WINDOW A FILE ACCORDING TO THE SMALL RBF AOI WINDOW -------------------
        cmdwin = "Window3d n1=%d f1=%d n2=%d f2=%d < %s > %s;\n" % (
            self.nz,self.pad,self.ny,self.pad,fileIN,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('WINDOW A 2D FILE ACCORDING TO THE SMALL RBF AOI WINDOW')
            print(cmdwin)
        subprocess.call(cmdwin,shell=True)
        return



    def SCALE(self, debug, fileIN, fileOUT, coeff):
        #----- SCALE A FILE  -------------------
        cmd1 = "Math_base file1=%s exp='file1*%s'> %s " % (fileIN,coeff,fileOUT)
        aa=commands.getstatusoutput(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('SCALE FILE')
            print(cmd1)
        return


    def ONES_2D(self, debug, fileIN, fileOUT):
        cmd = "Math file1=%s exp='file1*0.0+1.0' > %s;\n" % (fileIN,fileOUT)
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE A FILE OF ALL ONES')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return




    def MAKE_FWIMASK(self, debug, shift, tomomask):

        # Make salt mask
        cmd0a="%s/CLIP.x clipval=4400.0 replaceval=1.0                    < %s  > %sb" % (self.T_BIN,self.vel_path,tomomask)
        cmd0b="%s/CLIP.x clipval=1.1    replaceval=0.0 replacelessthan=0  < %sb > %s1" % (self.T_BIN,tomomask,tomomask)

        # Shift watermask since the shallow part is noisy
        cmdW2="Pad extend=1 beg1=%d < %s > %s1; Window3d n1=%s < %s1 > %s2" % (shift,self.watermask,self.watermask,self.nz,self.watermask,self.watermask)
        # Apply watermask and add RBF mask
        cmdW3="Math file1=%s2 file2=%s1 file3=%s exp='file1*file2+file3' > %scc " % (self.watermask,tomomask,self.rbfmask,tomomask)
        cmdW4="%s/CLIP.x clipval=1.0    replaceval=1.0 replacelessthan=0  < %scc > %s" % (self.T_BIN,tomomask,tomomask)

        # Make full command
        cmd='%s; %s; %s; %s; %s;' % (cmd0a,cmd0b,cmdW2,cmdW3,cmdW4)
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE TOMO MASK')
            print(cmd)
        subprocess.call(cmd,shell=True)

        return


    def MAKE_TOMOMASK(self, debug, shift, tomomask):

        # Make salt mask
        cmd0a="%s/CLIP.x clipval=4400.0 replaceval=1.0                    < %s  > %sb" % (self.T_BIN,self.vel_path,tomomask)
        cmd0b="%s/CLIP.x clipval=1.1    replaceval=0.0 replacelessthan=0  < %sb > %s1" % (self.T_BIN,tomomask,tomomask)

        # Shift watermask since the shallow part is noisy
        cmdW2="Pad extend=1 beg1=%d < %s > %s1; Window3d n1=%s < %s1 > %s2" % (shift,self.watermask,self.watermask,self.nz,self.watermask,self.watermask)
        # Apply watermask and add RBF mask
        cmdW3="Math file1=%s2 file2=%s1 exp='file1*file2' > %s " % (self.watermask,tomomask,tomomask)

        # Make full command
        cmd='%s; %s; %s; %s;' % (cmd0a,cmd0b,cmdW2,cmdW3)
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE TOMO MASK')
            print(cmd)
        subprocess.call(cmd,shell=True)

        return




    def MAKE_MASKS_RBF(self, debug, phi_path, masksalt, **optparams):
        if ('addguide' in optparams):
            addguide = optparams['addguide']
        else:
            addguide = False

        #------- MAKE SALT MASK ------------------------------------------
        cmdwin1 = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s1" % (
            self.nzr,self.fzr,self.nyr,self.fyr,self.nxr,self.fxr,phi_path,phi_path)

        cmdwin2 = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s1" % (
            self.nzr,self.fzr,self.nyr,self.fyr,self.nxr,self.fxr,self.watermask,self.watermask)

        cmdwin3 = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s1" % (
            self.nzr,self.fzr,self.nyr,self.fyr,self.nxr,self.fxr,self.rbfmask,self.rbfmask)

        heaviside='%s/heavisideCUR.H' % (self.path_out)
        cmd1="%s/APPROX_HEAVISIDE.x kappa=%s < %s1 > %s" % (self.T_BIN,self.kappa,phi_path,heaviside)
        cmd2="%s/PHI_SLOPE.x verbose=1 < %s | Scale > %s1" % (self.T_BIN,heaviside,masksalt)

        if (addguide):
            cmdG = "Add %s1 %s | Scale > %s_wg ;" % (masksalt,self.guide,masksalt)
            cmd3 = "Math file1=%s_wg file2=%s1 exp='file1*file2' > %s; rm %s_wg " % (masksalt,self.rbfmask,masksalt,masksalt)
        else:
            cmdG = " "
            cmd3 = "Math file1=%s1 file2=%s1 exp='file1*file2' > %s; rm %s1 " % (masksalt,self.rbfmask,masksalt,masksalt)

        cmdFinal="%s; %s; %s; %s; %s; %s %s rm %s1 %s1" % (cmdwin1, cmdwin2, cmdwin3, cmd1, cmd2, cmdG, cmd3, phi_path,self.rbfmask)
        
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE SALT MASK')
            print(cmdFinal)
        subprocess.call(cmdFinal,shell=True)

        return



 # For sparsemodel that includes ONLY phi updates
    def DELTAP_BEFORE_RBF2d(self, debug, rtm_path, sparsemodel):
        #------- MAKE PHI GRADIENTS  ------------------------------------
        cmdwin1 = "Window3d n1=%d f1=%d n2=%d f2=%d < %s > %s.trimmed" % (
            self.nzr,self.fzR,self.nyr,self.fyR,rtm_path,rtm_path)
        cmdwin2 = "Window3d n1=%d f1=%d n2=%d f2=%d < %s > %s.trimmed" % (
            self.nzr,self.fzR,self.nyr,self.fyR,self.masksalt,self.masksalt)
        cmd1 = "%s/APPLY_Dop_RBF2d.x rbftable=%s rbfcoord=%s masksalt=%s.trimmed scaling=%s verbose=0 adjoint=1 par=%s < %s.trimmed > %s" % (self.T_BIN,self.rbftable,self.rbfcoord,self.masksalt,self.scaling,self.genpar,rtm_path,sparsemodel)
        #------- PURELY FOR VISUALIZATON PURPOSES ------------------------------
        cmd2 = "%s/APPLY_RBF2d.x rbftable=%s rbfcoord=%s verbose=0 adjoint=0 par=%s < %s > %s/fullphigrad.H" % (self.T_BIN,self.rbftable,self.rbfcoord,self.genpar,self.sparsemodel,self.pathtemp)
        cmd = "%s; %s; %s; %s" % (cmdwin1,cmdwin2,cmd1, cmd2)
        if (debug):
            print('-------------------------------------------------------------')
            print('MAKE PHIrbf GRADIENTS for 2D model')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return

 # For sparsemodel that includes ONLY phi updates
    def DELTAP_BEFORE_RBF1(self, debug, rtm_path, sparsemodel):
        #------- MAKE PHI & TOMO GRADIENTS  ------------------------------------
        cmdwin1 = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s.trimmed" % (
            self.nzr,self.fzR,self.nyr,self.fyR,self.nxr,self.fxR,rtm_path,rtm_path)
        # cmdwin2 = "Cp %s %s.trimmed" % (self.masksalt,self.masksalt)
        cmdwin2 = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s.trimmed" % (
            self.nzr,self.fzR,self.nyr,self.fyR,self.nxr,self.fxR,self.masksalt,self.masksalt)
        cmd1 = "%s/APPLY_Dop_RBF3d.x rbftable=%s rbfcoord=%s masksalt=%s.trimmed scaling=%s verbose=0 adjoint=1 par=%s < %s.trimmed > %s" % (self.T_BIN,self.rbftable,self.rbfcoord,self.masksalt,self.scaling,self.genpar,rtm_path,sparsemodel)
        cmd = "%s; %s; %s;" % (cmdwin1,cmdwin2,cmd1)
        if (debug):
            print('-------------------------------------------------------------')
            print('MAKE PHIrbf GRADIENTS')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def DELTAP_AFTER_RBF1(self, debug, sparseinput, phigrad, tomograd):
        #------- MAKE SCALED RBF GRADIENT TO USE IN LINESEACH  ------------------------------------------
        cmd1 = "Scale < %s > %s_scaled" % (sparseinput, sparseinput)
        #------- CONVERT RBF GRADIENT TO VEL MODEL SPACE FOR VISUALIZING  ------------------------------------------
        cmd2 = "%s/APPLY_RBF3d.x rbftable=%s rbfcoord=%s verbose=0 adjoint=0 par=%s < %s_scaled > %s" % (self.T_BIN,self.rbftable,self.rbfcoord,self.genpar,sparseinput,phigrad)
        cmd3 = "Cp %s %s; Solver_ops op=zero file1=%s" % (phigrad,tomograd,tomograd)
        cmd4 = "rm %s_scaled" % (sparseinput)
        cmd = "%s; %s; %s;" % (cmd1, cmd2, cmd3)
        if (debug):
            print('-----------------------------------------------------------------')
            print('Make phigrad (from RBF model after inversion) and EMPTY tomograd')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def DELTAP_AFTER_RBF2d(self, debug, sparseinput, phigrad, tomograd):
        #------- MAKE SCALED RBF GRADIENT TO USE IN LINESEACH  ------------------------------------------
        cmd1 = "Scale < %s > %s_scaled" % (sparseinput, sparseinput)
        cmdwin2 = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d < %s > %s1" % (
            self.nzr,self.fzr,self.nyr,self.fyr,self.nxr,self.fxr,self.watermask,self.watermask)
        #------- CONVERT RBF GRADIENT TO VEL MODEL SPACE FOR VISUALIZING  ------------------------------------------
        cmd2 = "%s/APPLY_RBF2d.x rbftable=%s rbfcoord=%s verbose=0 adjoint=0 par=%s < %s_scaled > %s1" % (self.T_BIN,self.rbftable,self.rbfcoord,self.genpar,sparseinput,phigrad)
        water = "Math file1=%s1 file2=%s1 exp='file1*file2' > %s" % (phigrad,self.watermask,phigrad)
        cmd3 = "Cp %s %s; rm %s1; Solver_ops op=zero file1=%s" % (phigrad,tomograd,phigrad,tomograd)
        cmd4 = "rm %s_scaled" % (sparseinput)
        cmd = "%s; %s; %s; %s; %s;" % (cmd1, cmdwin2, cmd2, water, cmd3)
        if (debug):
            print('-----------------------------------------------------------------')
            print('Make phigrad (from RBF model after inversion) and EMPTY tomograd')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return



    def PAD_WAVELET(self, debug, wavelet, nt):
        # --------  Calculate the maxbeta   ----------------------
        cmd = 'Pad n1out=%s < %s > %s1 datapath=%s1@; Cp %s1 %s' % (nt, wavelet, wavelet, wavelet, wavelet, wavelet)
        if (debug):
            print('-----------------------------------------------------------------')
            print('PAD_WAVELET')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return



    def CALC_MAXBETA_RBF(self, debug, upperlim, phigrad, phi_path):
        # --------  Calculate the maxbeta   ----------------------
        operator = '%s/MAXBETA_RBF.x upperlim=%s verbose=1' % (self.T_BIN,upperlim)
        inputs1 = 'phigrad=%s phi=%s'  % (phigrad,phi_path)
        cmd = '%s %s' % (operator, inputs1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('CALC_MAXBETA')
            print(cmd)
        subprocess.call(cmd,shell=True)
        # --------  Grab the maxbeta   ----------------------
        file = open('maxbetaval.txt')
        for line in file:
            maxbeta = float(line)
        if (debug):
            print('MAXBETA = %s') % (maxbeta)
        return maxbeta



    def SAVE_INTERMEDIATE_FILES(self, debug, invname):
        cmd1 = "%s %s %s %s %s %s %s %s %s %s %s_BEFORE %s" % (self.rtm_path,self.vel_path,self.phigrad,self.hess_out_path,self.phi_path,self.tomograd,self.scaling,self.rbf_path,self.wavelet,self.masksalt,self.phigrad,self.velback)
        # cmd1 = "%s %s %s %s %s %s %s %sbefore %sbefore %s" % (self.rtm_path,self.vel_path,self.phigrad,self.hess_out_path,self.phi_path,self.tomograd,self.scaling,self.phigrad,self.tomograd,self.rbf_path)
        qq=shlex.split(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('SAVING INTERMEDIATE FILES')
        for file in qq:
            filecomp = '%scomp' % (file)
            if (os.path.isfile(filecomp)):
                cmd1a = 'Cat3d axis=4 %s  %s > %s/out.H' % (filecomp, file, self.path_out)
                cmd1b = 'rm %s ; mv %s/out.H %s' % (filecomp,self.path_out,filecomp)
                cmd= '%s; %s' % (cmd1a, cmd1b)
            else:
                cmd = 'Cp %s %s ' % (file, filecomp)
            cmdp = '%s\n' % (cmd)
            if (debug):
                print(cmdp)
            subprocess.call(cmd,shell=True)

        # # Saving the residuals
        # filecomp = '%scomp' % (self.res_path)
        # if (os.path.isfile(filecomp)):
        #     cmd1a = 'Cat %s  %s > %s/out.H' % (filecomp, self.res_path, self.path_out)
        #     cmd1b = 'rm %s ; mv %s/out.H %s' % (filecomp,self.path_out,filecomp)
        #     cmd= '%s; %s' % (cmd1a, cmd1b)
        # else:
        #     cmd = 'Cp %s %s ' % (self.res_path, filecomp)
        # if (debug):
        #     print(cmd)
        # subprocess.call(cmd,shell=True)

        # Saving the hessian inverison stuff
        prefs = "%s %s %s" % ("obj","model","gradient")
        qq=shlex.split(prefs)
        for pref in qq:
            file = '%s%s.H' % (pref,invname)
            filecomp = '%s%s.Hcomp' % (pref,invname)
            if (os.path.isfile(filecomp)):
                cmd1a = 'Cat %s  %s > %s/out.H' % (filecomp, file, self.path_out)
                cmd1b = 'rm %s ; mv %s/out.H %s' % (filecomp,self.path_out,filecomp)
                cmd= '%s; %s' % (cmd1a, cmd1b)
            else:
                cmd = 'Cp %s %s ' % (file, filecomp)
            cmdp = '%s\n' % (cmd)
            if (debug):
                print(cmdp)
            subprocess.call(cmd,shell=True)
        return


    def SAVE_INTERMEDIATE_VALUES(self, debug, objfuncval, maxbeta, final_alpha, final_beta):

        if (debug):
            print('-----------------------------------------------------------------')
            print('SAVING INTERMEDIATE VALUES')

        file1="%s/objfuncval.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file1,objfuncval,file1)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)

        file2="%s/maxbeta.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file2,maxbeta,file2)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)

        file3="%s/final_alpha.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file3,final_alpha,file3)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)

        file4="%s/final_beta.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file4,final_beta,file4)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def CALC_OBJ_VAL(self, debug, prefix):
        #----- CALCULATE THE OBJECTIVE FUNCTION VALUE  -------------------
        searchForFiles="ls %s/%s*.txt" % (self.path_out,prefix)
        print(searchForFiles)
        junk,listofResFiles=commands.getstatusoutput(searchForFiles)
        inputfiles = listofResFiles.split()

        # print(inputfiles)

        new_objfuncval=0.0
        for objfile in inputfiles:
            # print(objfile)
            f=open(objfile, "r")
            dotval=float(f.read())
            # dotval=float(aa[1].split()[2])
            new_objfuncval=new_objfuncval+dotval
            # print(new_objfuncval)

        return new_objfuncval



    def CLEAN_UP(self, debug, invname):
        #----- Write the velmodel to output (current iteration) --------------------
        cmd1 = 'rm %s/*; rm -rf scratch/Window_key* scratch/KINE* scratch/MUTE* scratch/NORM* scratch/main* scratch/Pad* scratch/syn* scratch/single* scratch/ADD* scratch/BORN* scratch/res* scratch/curW* scratch/oldW* scratch/test*' % (self.path_out)
        cmd2 = 'rm -f %s' % (self.hess_out_path)
        cmd3 = 'rm -f %s' % (invname)
        cmd =  "%s; %s; %s;" % (cmd1, cmd2, cmd3)
        if (debug):
            print('-----------------------------------------------------------------')
            print('Remove WRK and scratch/ stuff that is irrelevant')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def RM_SRC_WAVELET(self, debug):
        #----- Remove the source wavelet --------------------
        cmd = 'rm -f %s %s_unscaled*' % (self.wavelet,self.wavelet)
        if (debug):
            print('-----------------------------------------------------------------')
            print('Remove SOURCE WAVELET')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def CLEAN_TMP(self, debug):
        #----- Write the velmodel to output (current iteration) --------------------
        cmd = 'rm %s/*' % (self.path_tmp)
        if (debug):
            print('-----------------------------------------------------------------')
            print('CLEAN_TMP')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return
