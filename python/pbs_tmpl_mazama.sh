#!/bin/tcsh
### Each node on cees-mazama has 24 cores.
#PBS -N jobname
#PBS -l nodes=1:ppn=24
#PBS -q sep
#PBS -V
#PBS -j oe
#PBS -e /data/biondo/tjdahlke/research/wrk
#PBS -o /data/biondo/tjdahlke/research/wrk
#PBS -d /data/biondo/tjdahlke/research/wrk
##PBS -l walltime=00:60:00

cd $PBS_O_WORKDIR
# setenv OMP_NUM_THREADS 2
echo $OMP_NUM_THREADS

echo WORKDIR is: $PBS_O_WORKDIR
sleep 1 #5
echo NodeFile is: $PBS_NODEFILE
echo hostname is: $HOSTNAME

