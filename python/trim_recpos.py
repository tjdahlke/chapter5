#================================================================================================
#       Program to subset recpos info for a select list of nodes
#
#       Usage:
#               >> python trim_recpos.py input=data_header.H select_nodes=select_nodes.H output=gridfile
#
#       Taylor Dahlke, 8/29/2018 taylor@sep.stanford.edu
#================================================================================================

#!/usr/bin/python
import subprocess
import sys
import math, copy
import sep2npy
import numpy as np


# Read in the input parameters
eq_args_from_cmdline, args = sep2npy.parse_args(sys.argv)
pars=sep2npy.RetrieveAllEqArgs(eq_args_from_cmdline)
infile=pars["infile"]
select_nodes=pars["select_nodes"]
outfile=pars["outfile"]

# Clear out the old file so as not to simply append to it
cmd = "rm %s" % (outfile)

# Read in the input recpos list
input_list=[]
file = open(infile)
for line in file:
	input_list.append(line.split())

# Read in the select node list
select_List=[]
file = open(select_nodes)
for line in file:
	select_List.append(line.split()) 



# Loop over all nodes
reclist=[]
for nodeInfo in input_list:
	# print("percent searched= %f" % (100.0*ii/nAllNodes))

	nodeIdO=nodeInfo[0]

	# Loop over select nodes
	for snode in select_List:
		nodeIdS=snode[0]

		# Check if node info is for a selected node
		if (nodeIdO == nodeIdS):
			print("match!!")

			# Grab node info if new node
			reclist.append(nodeInfo)
			break


print("=====================================")
print("Write to output")
for selectLine in reclist:
	# Write the reclist info to disk
	nodeInfo = "%s %s %s %s" % ( selectLine[0],selectLine[1],selectLine[2],selectLine[3])
	cmd = "echo %s >> %s" % (nodeInfo,outfile)
	subprocess.call(cmd,shell=True)


print("=============================================")
print("Length of input list: %d" % len(input_list))
print("Length of select list: %d" % len(select_List))
print("Length of final list: %d" % len(reclist))


