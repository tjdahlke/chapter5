#!/usr/local/bin/python

import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import pbs_util
from born3d import *


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------ INITIALIZE SOME STUFF ----------------------
    dict_args = param_reader.dict_args
    debug = True
    migvel = param_reader.dict_args['migvel']
    param_reader.dict_args['vel_path'] = migvel
    param_reader.dict_args['hess_out_path'] = 'junk'
    obs_path = param_reader.dict_args['obs_path']
    gradtag = param_reader.dict_args['gradtag']
    inversionname = param_reader.dict_args['inversionname']
    writewave = int(param_reader.dict_args['writewave'])

    # INITIALIZING THE MAIN FUNCTION SPACES. FUNCTIONS THAT CALL OTHER FUNCTIONS RELY ON THESE OBJECT INSTANCES
    born_modeling =make_BORN(param_reader)

    print("###################################################################################################")
    print("########################      RTM Imaging   #######################################################")
    print("###################################################################################################")


    print("====================================================================================")
    print("     MAKING RTM")

    dataAlreadySplit=False

    if (writewave==1):
        wavefield=True
    else:
        wavefield=False

    born_modeling.BORN_run(param_reader, gradtag, False, debug, True, inversionname, obs_path, wavefield,dataAlreadySplit)







