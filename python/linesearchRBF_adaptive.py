#!/usr/local/bin/python

import copy, random, os, sepbase, tempfile, time, unittest, math, subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import sys, os
from main_functions3d import *


#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
class MAIN_LS_FUNCTIONS(object):
    def __init__(self, debug, param_reader, waveinstance, maininstance, lsprefix, Ntries, tomoupd, phiupd):
        self.param_reader=param_reader
        self.dict_args = self.param_reader.dict_args
        self.pathtemp = self.dict_args['pathtemp']
        self.path_out = self.dict_args['path_out']
        self.T_BIN = self.dict_args['T_BIN']
        self.velback = self.dict_args['velback']
        self.phi_path = self.dict_args['phi_path']
        self.vel_path = self.dict_args['vel_path']
        self.vsalt = self.dict_args['vsalt']
        self.genpar = self.dict_args['genpar']
        # self.rbf_path = self.dict_args['rbf_path']
        # self.rbfcoord = self.dict_args['rbfcoord']
        # self.rbftable = self.dict_args['rbftable']
        self.wavelet = self.dict_args['wavelet']
        self.watermask = self.dict_args['watermask']
        self.kappa = self.dict_args['kappa']
        self.debug=debug
        self.tomoupd=tomoupd
        self.phiupd=phiupd
        self.Ntries=Ntries
        self.lsprefix=lsprefix
        self.waveinstance=waveinstance
        self.maininstance=maininstance

        self.randpad=int(self.dict_args['randpad'])
        self.pad=int(self.dict_args['pad'])
        self.nz=int(self.dict_args['nz'])
        self.nx=int(self.dict_args['nx'])
        self.ny=int(self.dict_args['ny'])
        self.nzr=int(self.dict_args['nzr'])
        self.nxr=int(self.dict_args['nxr'])
        self.nyr=int(self.dict_args['nyr'])
        self.fzr=int(self.dict_args['f1r'])
        self.fyr=int(self.dict_args['f2r'])
        self.fxr=int(self.dict_args['f3r'])
        self.fzR=self.fzr+self.randpad
        self.fxR=self.fxr+self.pad
        self.fyR=self.fyr+self.pad

        return

    # Disable printing
    def blockPrint(self):
        sys.stdout = open("linesearch_modeling.txt", 'w')
        # sys.stdout = open(os.devnull, 'w')

    # Restore printing
    def enablePrint(self):
        sys.stdout = sys.__stdout__


    # def UPDATE_MODEL_LS3d(self,debug,alpha,beta,phigradIN,tomogradIN,testvelpath,testphimod,testvelback):
    #     #----- UPDATE THE VELOCITY MODEL AND PHI SURFACE -----------------------
    #     phigrad=phigradIN+'full'
    #     tomograd=tomogradIN+'full'
    #     cpp1 = "Cp %s %s" % (self.phi_path,testphimod)
    #     cpp2 = "Cp %s %s" % (self.velback,testvelback)
    #     # pad1 = "Pad extend=0 beg1=%d n1out=%d beg2=%d n2out=%d beg3=%d n3out=%d < %s > %s" % (self.fzr,self.nz,self.fyr,self.ny,self.fxr,self.nx,phigradIN,phigrad)
    #     # pad2 = "Pad extend=0 beg1=%d n1out=%d beg2=%d n2out=%d beg3=%d n3out=%d < %s > %s" % (self.fzr,self.nz,self.fyr,self.ny,self.fxr,self.nx,tomogradIN,tomograd)
    #     pad1 = "Cp %s %s" % (phigradIN,phigrad)
    #     pad2 = "Cp %s %s" % (tomogradIN,tomograd)
    #     add1 = "Solver_ops op=scale_addscale file1=%s file2=%s scale2_r=%s" % (testphimod,phigrad,beta)
    #     add2 = "Solver_ops op=scale_addscale file1=%s file2=%s scale2_r=%s" % (testvelback,tomograd,alpha)
    #     rbf1 = "%s/PHI0_BUILDER_BINARY.x thresh=0.0 verbose=1 par=%s < %s > %s/saltoverlay.H" % (self.T_BIN,self.genpar,testphimod,self.pathtemp)
    #     rbf2 = "Math file1=%s/saltoverlay.H exp='file1+1.0' | Scale > %s/saltoverlay2.H" % (self.pathtemp,self.pathtemp)
    #     add3 = "Math file1=%s/saltoverlay2.H file2=%s exp='%f*file1+file2'> %s/unclipppedvel.h" % (self.pathtemp,testvelback,float(self.vsalt),self.pathtemp)
    #     clip = "Clip clip=%f chop=greater < %s/unclipppedvel.h> %s" % (float(self.vsalt),self.pathtemp,testvelpath)
    #     rm1 = "rm %s/saltoverlay.H* %s/unclipppedvel.h" % (self.pathtemp,self.pathtemp)
    #     cmd = "%s; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s;" % (cpp1, cpp2, pad1, pad2, add1, add2, rbf1, rbf2, add3, clip, rm1)
    #     # Padding for ABCs/RBCs is done in modeling functions, not here
    #     if (debug):
    #         print('-----------------------------------------------------------------')
    #         print('UPDATING MODEL')
    #         print(cmd)
    #     subprocess.call(cmd,shell=True)
    #     return



    def UPDATE_MODEL_LS3d(self,debug,alpha,beta,phigradIN,tomogradIN,testvelpath,testphimod,testvelback):
        #----- UPDATE THE VELOCITY MODEL AND PHI SURFACE -----------------------
        phigrad=phigradIN+'full'
        tomograd=tomogradIN+'full'
        heaviside='%s/heavisideTMP.H' % (self.path_out)

        cpp1 = "Cp %s %sraw" % (self.phi_path,testphimod)
        cpp2 = "Cp %s %sraw" % (self.velback,testvelback)
        cpp3 = "Cp %s %s" % (phigradIN,phigrad)
        cpp4 = "Cp %s %s" % (tomogradIN,tomograd)
        add1 = "Solver_ops op=scale_addscale file1=%sraw file2=%s scale2_r=%s" % (testphimod,phigrad,beta)
        add2 = "Solver_ops op=scale_addscale file1=%sraw file2=%s scale2_r=%s" % (testvelback,tomograd,alpha)

        # Clip the velback so that it doesn't get too big or small
        cmdc1="%s/CLIP.x verbose=0 clipval=5000.0  replacelessthan=0 < %sraw > %s1" % (self.T_BIN,testvelback,testvelback)
        cmdc2="%s/CLIP.x verbose=0 clipval=1500.0 replacelessthan=1 < %s1 > %s" % (self.T_BIN,testvelback,testvelback)

        # Clip the phi surface so that it doesn't just keep growing
        phiclipval=3.0
        cmdc3="%s/CLIP.x verbose=0 clipval=%f  replacelessthan=0 < %sraw > %s1" % (self.T_BIN,phiclipval,testphimod,testphimod)
        cmdc4="%s/CLIP.x verbose=0 clipval=-%f replacelessthan=1 < %s1 > %s" % (self.T_BIN,phiclipval,testphimod,testphimod)

        cmd1="%s/APPROX_HEAVISIDE.x kappa=%s < %s > %s" % (self.T_BIN,self.kappa,testphimod,heaviside)
        cmd2="%s/MAKE_VEL_MODEL.x watermask=%s heaviside=%s hicut=5000 < %s > %s" % (self.T_BIN,self.watermask,heaviside,testvelback,testvelpath)

        cmd = "%s; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s; %s;" % (cpp1, cpp2, cpp3, cpp4, add1, add2, cmdc1, cmdc2, cmdc3, cmdc4, cmd1, cmd2)
        # Padding for ABCs/RBCs is done in modeling functions, not here
        if (debug):
            print('-----------------------------------------------------------------')
            print('UPDATING MODEL')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return




    def LINE_SEARCH_GREEDY(self, maxbeta, zero_objfuncval, phigrad, tomograd, dpIncrease):
        print('-----------------------------------------------------------------')
        print('LINE SEARCH')

        # Initialize the lists
        Npoints=3
        testP=[0]*Npoints
        new_objfuncval=[0]*Npoints
        X=[0]*Npoints
        Y=[0]*Npoints

        dp = maxbeta/(Npoints-1)
        ip=0
        for itry in range(0,self.Ntries):
            # Get ip value
            if (ip==0):
                testP[ip] = 0
            else:
                testP[ip] = testP[ip-1] + dp

            print("\n BETA VALUE = %f" % testP[ip])

            if (self.debug):  print(">>>>>>>>>>>>>>>>>>>>>   TEST BETA =  %s . Try #%s") % (testP[ip],itry)
            testvelpath = '%s/testvel%s_%s.H' % (self.path_out,self.lsprefix,itry)
            testphipath = '%s_PHI' % (testvelpath)
            testbackpath = '%s_velback' % (testvelpath)
            testrespath = '%s/testres%s_%s.H' % (self.path_out,self.lsprefix,itry)

            #FOR 3D or 2D CASE
            if(self.phiupd and self.tomoupd):
                if(not os.path.isfile(testvelpath)):
                    self.UPDATE_MODEL_LS3d(self.debug,alpha=testP[ip],beta=testP[ip],testvelpath=testvelpath,phigradIN=phigrad,tomogradIN=tomograd,testphimod=testphipath,testvelback=testbackpath)
            if((self.phiupd) and (not self.tomoupd)):
                if(not os.path.isfile(testvelpath)):
                    self.UPDATE_MODEL_LS3d(self.debug,alpha=0.0,beta=testP[ip],testvelpath=testvelpath,phigradIN=phigrad,tomogradIN=tomograd,testphimod=testphipath,testvelback=testbackpath)
            if((not self.phiupd) and (self.tomoupd)):
                if(not os.path.isfile(testvelpath)):
                    self.UPDATE_MODEL_LS3d(self.debug,alpha=testP[ip],beta=0.0,testvelpath=testvelpath,phigradIN=phigrad,tomogradIN=tomograd,testphimod=testphipath,testvelback=testbackpath)

            if (ip==0):
                new_objfuncval[ip]=zero_objfuncval
                objval = "objfuncval #%s =  %s" % (ip, new_objfuncval[ip])
                print(objval)
                ip = ip+1
            else:

                # # #VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
                # #Makes random objective function values for fast testing purposes only
                # aa = random.uniform(-0.05, 0.05)
                # new_objfuncval[ip] = new_objfuncval[ip-1] + new_objfuncval[ip-1]*aa
                # # #VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

                prefixA = "LINESEARCH_%s" % (itry)
                testsynpath = "%s/%s.H" % (self.path_out,prefixA)
                self.blockPrint()
                self.waveinstance.MODELED_DATA(self.param_reader,self.debug,prefixA,'linesearch',testvelpath,False,self.wavelet,testrespath)
                self.enablePrint()
                new_objfuncval[ip]=self.maininstance.CALC_OBJ_VAL(self.debug,prefixA) # Calc the objective function value

                # Shrink maxbeta if first objfunc val is not less than the initial objfunc val
                shrinkscale=1.5
                if (new_objfuncval[ip]>new_objfuncval[ip-1]):
                    dp = dp/shrinkscale
                    printout1 = "objfuncval #%s =  %s (bad)" % (itry, new_objfuncval[ip])
                    print(printout1)
                    print("Shrinking the beta step size by %s" % (shrinkscale))
                    if (ip==1): # Reuse the last testP and objective function value since the new ones are bad
                        new_objfuncval[2] = new_objfuncval[1]
                        testP[2] = testP[1]
                    if (ip==2): # Found a place where the objective function value is increasing after a minimum
                        break
                elif (new_objfuncval[ip]==new_objfuncval[ip-1]):
                    dp = dp*dpIncrease
                    printout1 = "objfuncval #%s =  %s (bad; hasn't done anything!)" % (itry, new_objfuncval[ip])
                    print(printout1)
                    print("increasing the beta step size by %s: from dp=%s to dp=%s." % (dpIncrease,dp,dp*dpIncrease))
                else:
                    objval = "objfuncval #%s =  %s" % (itry, new_objfuncval[ip])
                    print(objval)
                    ip = ip+1

                    # Shift the results back and keep evaluating
                    if (ip>2):
                        testP[0] = testP[1]
                        testP[1] = testP[2]
                        new_objfuncval[0] = new_objfuncval[1]
                        new_objfuncval[1] = new_objfuncval[2]
                        ip = 2


        # Find the best objective function value and the Beta that made it
        bestval=max(new_objfuncval)
        maxip= (Npoints-1)
        if (self.debug): print("range(0,Npoints) = %s    maxip = %s    dp =%s       Npoints=%s") % (range(0,Npoints),maxip, dp, Npoints)
        for ip in range(0,Npoints):
            if (self.debug): print("beta = %s       objfuncval= %s   ip=%s") % (testP[ip],  new_objfuncval[ip], ip)
            if (new_objfuncval[ip]<=bestval):
                bestval=new_objfuncval[ip]
                best_ip=ip

        if (best_ip==0):
            final_beta = testP[best_ip]
            bestval=new_objfuncval[best_ip]
            print("We tried, but couldn't find a step size that was better than zero!")
            print("final_obj = %s \n") % (bestval)
        if (best_ip==2):
            final_beta = testP[best_ip]
            bestval=new_objfuncval[best_ip]
            print("The best beta was the last one we tried. You might get a better beta if you increase the Ntries parameter.")
            print("final_obj = %s \n") % (bestval)
        if (best_ip==1):
            X = testP
            Y = new_objfuncval

            print("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")
            # Invert the Adjoint : (X^T)-1
            a = np.array(  [ [ (X[0]**2.), X[0], 1 ], [ (X[1]**2.), X[1], 1 ], [ (X[2]**2.), X[2], 1 ] ]  )
            b = np.array(  [ [ Y[0], Y[1], Y[2] ] ]  )
            ainv = np.linalg.inv(a)

            # Print if debug
            if (self.debug): print("ainv = %s") % (ainv)
            if (self.debug): print("b = %s") % (b)

            # Multiply : ((X^T)-1)*d
            M = np.dot(ainv, np.transpose(b))
            # Find the minimum value
            final_beta = -1.0*M[1][0]/(2.*M[0][0])

            # Check the objfunc_val guess
            bestval= (final_beta**2.)*M[0][0]  +  (final_beta)*M[1][0]  +  M[2][0]
            print("final_obj (estimate) = %s \n") % (bestval)

        if (self.tomoupd and self.phiupd):
            final_beta=final_beta
            final_alpha=final_beta
        if ((self.tomoupd) and (not self.phiupd)):
            final_beta=0.0
            final_alpha=final_beta
        if ((not self.tomoupd) and (self.phiupd)):
            final_beta=final_beta
            final_alpha=0.0

        print("\nfinal_alpha = %s \n") % (final_alpha)
        print("\nfinal_beta = %s \n") % (final_beta)
        return final_alpha,final_beta,bestval




    def LINE_SEARCH(self, maxbeta, zero_objfuncval, phigrad, tomograd, betaDecrease):
        print('-----------------------------------------------------------------')
        print('BACK TRACKING LINE SEARCH')


        objval = "objfuncval #0 = %s" % (zero_objfuncval)
        print(objval)

        beta=maxbeta

        for itry in range(1,self.Ntries):


            print("\n BETA VALUE = %f" % beta)

            if (self.debug):  print(">>>>>>>>>>>>>>>>>>>>>   TEST BETA =  %s . Try #%s") % (beta,itry)
            testvelpath = '%s/testvel%s_%s.H' % (self.path_out,self.lsprefix,itry)
            testphipath = '%s_PHI' % (testvelpath)
            testrespath = '%s/testres%s_%s.H' % (self.path_out,self.lsprefix,itry)
            testbackpath = '%s_velback' % (testvelpath)


            # FOR 3D -or- 2D CASE
            if(self.phiupd and self.tomoupd):
                if(not os.path.isfile(testvelpath)):
                    self.UPDATE_MODEL_LS3d(self.debug,alpha=beta,beta=beta,testvelpath=testvelpath,phigradIN=phigrad,tomogradIN=tomograd,testphimod=testphipath,testvelback=testbackpath)
            if((self.phiupd) and (not self.tomoupd)):
                if(not os.path.isfile(testvelpath)):
                    self.UPDATE_MODEL_LS3d(self.debug,alpha=0.0,beta=beta,testvelpath=testvelpath,phigradIN=phigrad,tomogradIN=tomograd,testphimod=testphipath,testvelback=testbackpath)
            if((not self.phiupd) and (self.tomoupd)):
                if(not os.path.isfile(testvelpath)):
                    self.UPDATE_MODEL_LS3d(self.debug,alpha=beta,beta=0.0,testvelpath=testvelpath,phigradIN=phigrad,tomogradIN=tomograd,testphimod=testphipath,testvelback=testbackpath)
            
            # Get the objective function value
            prefixA = "LINESEARCH_%s" % (itry)
            testsynpath = "%s/%s.H" % (self.path_out,prefixA)
            self.blockPrint()
            self.waveinstance.MODELED_DATA(self.param_reader,self.debug,prefixA,'linesearch',testvelpath,False,self.wavelet,testrespath)
            self.enablePrint()
            new_objfuncval=self.maininstance.CALC_OBJ_VAL(self.debug,prefixA) # Calc the objective function value

            # Shrink maxbeta if first objfunc val is not less than the initial objfunc val
            if (new_objfuncval<zero_objfuncval):
                objval = "objfuncval #%s =  %s" % (itry, new_objfuncval)
                print(objval)
                break
            else:
                printout1 = "objfuncval #%s =  %s (bad)" % (itry, new_objfuncval)
                print(printout1)
                print("Shrinking the beta step size by %s" % (betaDecrease))
                beta = beta*betaDecrease

        if (itry==(self.Ntries-1)):
            print("We tried, but couldn't find a step size that was better than zero!")
            final_beta=0.0
        else:
            final_beta=beta

        if (self.tomoupd and self.phiupd):
            final_beta=final_beta
            final_alpha=final_beta
        if ((self.tomoupd) and (not self.phiupd)):
            final_beta=0.0
            final_alpha=final_beta
        if ((not self.tomoupd) and (self.phiupd)):
            final_beta=final_beta
            final_alpha=0.0

        print("\nfinal_alpha = %s \n") % (final_alpha)
        print("\nfinal_beta = %s \n") % (final_beta)

        return final_alpha,final_beta









    def UPDATE_MODEL_LS3d_FWI(self,debug,alpha,tomogradIN,velin,testvelpath):

        #----- UPDATE THE VELOCITY MODEL AND PHI SURFACE -----------------------
        cpp1 = "Cp %s %sraw" % (velin,velin)
        add1 = "Solver_ops op=scale_addscale file1=%sraw file2=%s scale2_r=%s" % (velin,tomogradIN,alpha)

        # Clip the velfull model so that it doesn't get too big or small
        cmd1="%s/CLIP.x verbose=0 clipval=5480.0 replacelessthan=0 < %sraw > %s1" % (self.T_BIN,velin,velin)
        cmd2="%s/CLIP.x verbose=0 clipval=1500.0 replacelessthan=1 < %s1 > %s" % (self.T_BIN,velin,testvelpath)

        cmd = "%s; %s; %s; %s; " % (cpp1, add1, cmd1, cmd2)
        # Padding for ABCs/RBCs is done in modeling functions, not here
        if (debug):
            print('-----------------------------------------------------------------')
            print('UPDATING MODEL')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return




    def LINE_SEARCH_FWI(self, maxalpha, zero_objfuncval, tomograd, alphaDecrease):
        print('-----------------------------------------------------------------')
        print('BACK TRACKING LINE SEARCH')


        objval = "objfuncval #0 = %s" % (zero_objfuncval)
        print(objval)

        alpha=maxalpha

        for itry in range(1,self.Ntries):


            print("\n ALPHA VALUE = %f" % alpha)

            if (self.debug):  print(">>>>>>>>>>>>>>>>>>>>>   TEST ALPHA =  %s . Try #%s") % (alpha,itry)
            testvelpath = '%s/testvel%s_%s.H' % (self.path_out,self.lsprefix,itry)
            testrespath = '%s/testres%s_%s.H' % (self.path_out,self.lsprefix,itry)

            # FOR 3D -or- 2D CASE
            if(not os.path.isfile(testvelpath)):
                self.UPDATE_MODEL_LS3d_FWI(self.debug,alpha=alpha,tomogradIN=tomograd,velin=self.vel_path,testvelpath=testvelpath)

            # Get the objective function value
            prefixA = "LINESEARCH_%s" % (itry)
            testsynpath = "%s/%s.H" % (self.path_out,prefixA)
            self.blockPrint()
            self.waveinstance.MODELED_DATA(self.param_reader,self.debug,prefixA,'linesearch',testvelpath,False,self.wavelet,testrespath)
            self.enablePrint()
            new_objfuncval=self.maininstance.CALC_OBJ_VAL(self.debug,prefixA) # Calc the objective function value

            # Shrink maxalpha if first objfunc val is not less than the initial objfunc val
            if (new_objfuncval<zero_objfuncval):
                objval = "objfuncval #%s =  %s" % (itry, new_objfuncval)
                print(objval)
                break
            else:
                printout1 = "objfuncval #%s =  %s (bad)" % (itry, new_objfuncval)
                print(printout1)
                print("Shrinking the alpha step size by %s" % (alphaDecrease))
                alpha = alpha*alphaDecrease

        if (itry==(self.Ntries-1)):
            print("We tried, but couldn't find a step size that was better than zero!")
            final_alpha=0.0
        else:
            final_alpha=alpha

        print("\nfinal_alpha = %s \n") % (final_alpha)

        return final_alpha







