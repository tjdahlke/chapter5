#!/usr/bin/python
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
from GNhess3d import *

#=======================================================================================================
#=======================================================================================================
#=======================================================================================================


class make_hessGN(object):
    def __init__(self, param_reader):

        #-------------------------------------------------------------------------------------
        dict_args = param_reader.dict_args
        self.hess_in_path = dict_args['hess_in_path']
        self.hess_out_path = dict_args['hess_out_path']
        self.path_out = param_reader.path_out

        self.tomo = int(param_reader.dict_args['tomo'])

        # Read in the job parameters
        self.param_reader=param_reader
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.vel_path = self.dict_args['vel_path']
        self.genpar = self.dict_args['genpar']
        self.mask = self.dict_args['mask']
        self.scaling = self.dict_args['scaling']

        self.prefix = self.dict_args['prefix']

        self.pad = int(self.dict_args['pad'])
        self.randpad = int(self.dict_args['randpad'])

        self.nz = int(self.dict_args['nz'])
        self.nx = int(self.dict_args['nx'])
        self.ny = int(self.dict_args['ny'])

        self.nzp = self.nz+self.pad+self.randpad
        self.nxp = self.nx+2*self.randpad
        self.nyp = self.ny+2*self.randpad

        self.nzr = int(self.dict_args['nzr'])
        self.nxr = int(self.dict_args['nxr'])
        self.nyr = int(self.dict_args['nyr'])

        self.fz = int(self.dict_args['fz'])+self.randpad
        self.fx = int(self.dict_args['fx'])+self.randpad
        self.fy = int(self.dict_args['fy'])+self.randpad

        self.gnhess_obj=make_GNHESS(param_reader)
        fn_seph_list = ' '

        hessIN  = "%s/hessGNin_full.H" % (self.path_out)
        hessOUT = "%s/hessGNout_full.H" % (self.path_out)


        if (self.tomo):

            #----- MASKING AND SCALING FULL MODEL -----------------------
            maskop = "Math file1=%s file2=%s exp='file1*file2' > %s1" % (
                self.mask, self.hess_in_path, hessIN)
            pad="Pad extend=0 beg1=%d beg2=%d beg3=%d end1=%d end2=%d end3=%d < %s1 > %s;" % (
                self.pad,self.pad,self.pad,self.pad,self.pad,self.pad,hessIN,hessIN)
            startcall="%s; %s" % (maskop,pad)
            print(startcall)
            subprocess.call(startcall, shell=True)

            # # Copy
            # startcall="Cp %s %s" % (hessIN,hessOUT)
            # subprocess.call(startcall, shell=True)

            #----- RUN PBS:  (B^T)B --------------------------------------------
            hesstag = 'HessGN_'+self.prefix
            self.gnhess_obj.GNHESS_run(self.param_reader,hesstag,True,True,hessIN,hessOUT)

            #----- MASKING AND SCALING FULL MODEL -----------------------
            unpad="Window3d f1=%d f2=%d f3=%d n1=%d n2=%d n3=%d < %s > %s1" % (
                self.pad,self.pad,self.pad,self.nz,self.ny,self.nx, hessOUT, hessOUT)
            maskop = "Math file1=%s file2=%s1 exp='file1*file2' > %s;" % (
                self.mask, hessOUT, self.hess_out_path)
            startcall="%s; %s" % (unpad,maskop)
            print(startcall)
            subprocess.call(startcall, shell=True)


        else:

            # ----- MASKING AND SCALING FULL MODEL -----------------------
            maskop = "Math file1=%s file2=%s exp='file1*file2' > %s1" % (
                self.mask, self.hess_in_path, hessIN)
            pad="Pad extend=0 beg1=%d beg2=%d beg3=%d n1out=%d n2out=%d n3out=%d < %s1 > %s;" % (
                self.fz,self.fy,self.fx,self.nzp,self.nyp,self.nxp,hessIN,hessIN)
            startcall="%s; %s" % (maskop,pad)
            print(startcall)
            subprocess.call(startcall, shell=True)

            # # Copy
            # startcall="Cp %s %s" % (hessIN,hessOUT)
            # subprocess.call(startcall, shell=True)

            #----- RUN PBS:  (B^T)B --------------------------------------------
            hesstag = 'HessGN_'+self.prefix
            self.gnhess_obj.GNHESS_run(self.param_reader,hesstag,True,True,hessIN,hessOUT)

            #----- MASKING AND SCALING FULL MODEL -----------------------
            unpad="Window3d f1=%d f2=%d f3=%d n1=%d n2=%d n3=%d < %s > %s1" % (
                self.fz,self.fy,self.fx,self.nzr,self.nyr,self.nxr, hessOUT, hessOUT)
            maskop = "Math file1=%s file2=%s1 exp='file1*file2' > %s;" % (
                self.mask, hessOUT, self.hess_out_path)
            startcall="%s; %s" % (unpad,maskop)
            print(startcall)
            subprocess.call(startcall, shell=True)


        return




if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------- APPLY GAUSS NEWTON HESSIAN TO GRADIENT ----------------
    start_time = time.time()
    make_hessGN(param_reader)
    elapsed_time = (time.time() - start_time) / 60.0
    print("ELAPSED TIME FOR BTE Hessian (min) : ")
    print(elapsed_time)
