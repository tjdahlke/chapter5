#!/bin/tcsh
### Each node on cees-mazama has 24 cores.
### Each node on cees-rcf has 16 cores.
#PBS -l nodes=1:ppn=24
#PBS -q default
#PBS -V
#PBS -j oe
##PBS -l walltime=01:0:00
