#!/usr/bin/python
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
from GNhess3d import *

#=======================================================================================================
#=======================================================================================================
#=======================================================================================================


class make_hessGN(object):
    def __init__(self, param_reader):

        #-------------------------------------------------------------------------------------
        dict_args = param_reader.dict_args
        self.hess_in_path = dict_args['hess_in_path']
        self.hess_out_path = dict_args['hess_out_path']
        self.path_out = param_reader.path_out

        # Read in the job parameters
        self.param_reader=param_reader
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.vel_path = self.dict_args['vel_path']
        self.genpar = self.dict_args['genpar']
        self.rbf_path = self.dict_args['rbf_path']
        self.masksalt = self.dict_args['masksalt']
        self.scaling = self.dict_args['scaling']
        self.rbfcoord = self.dict_args['rbfcoord']
        self.rbftable = self.dict_args['rbftable']
        self.prefix = self.dict_args['prefix']

        self.pad = int(self.dict_args['pad'])
        self.randpad = int(self.dict_args['randpad'])

        self.nz0 = int(self.dict_args['nz'])
        self.ny0 = int(self.dict_args['ny'])
        self.nx0 = int(self.dict_args['nx'])

        self.nz = int(self.dict_args['nz'])+self.pad+self.randpad
        self.nx = int(self.dict_args['nx'])+2*self.randpad
        self.ny = int(self.dict_args['ny'])+2*self.randpad

        self.nzr = int(self.dict_args['nzr'])
        self.nxr = int(self.dict_args['nxr'])
        self.nyr = int(self.dict_args['nyr'])

        self.fz = int(self.dict_args['fz'])+self.randpad
        self.fx = int(self.dict_args['fx'])+self.randpad
        self.fy = int(self.dict_args['fy'])+self.randpad

        self.gnhess_obj=make_GNHESS(param_reader)
        fn_seph_list = ' '

        hessIN = "%s/hessGNin_full.H" % (self.path_out)
        hessTMP = "%s/hessGNtmp_full.H" % (self.path_out)
        hessOUT = "%s/hessGNout_sparse.H" % (self.path_out)

        cmdwin2 = "Window3d n1=%d f1=%d n2=%d f2=%d n3=%d f3=%d  < %s > %s.trimmed" % (
        self.nzr,self.fz,self.nyr,self.fy,self.nxr,self.fx,self.masksalt,self.masksalt)
        print(cmdwin2)
        subprocess.call(cmdwin2, shell=True)

        #----- CONVERT FROM SPARSE MODEL TO FULL MODEL WITH ABC PADDING -----------------------
        derivop1 = "%s/APPLY_Dop_RBF3d.x rbftable=%s rbfcoord=%s masksalt=%s.trimmed scaling=%s verbose=0 adjoint=0 par=%s < %s > %s1" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, self.hess_in_path, hessIN)
        pad="Pad extend=0 beg1=%d beg2=%d beg3=%d n1out=%d n2out=%d n3out=%d < %s1 > %s; rm %s1" % (
            self.fz,self.fy,self.fx,self.nz,self.ny,self.nx,hessIN,hessIN,hessIN)
        startcall="%s; %s" % (derivop1,pad)
        print(startcall)
        subprocess.call(startcall, shell=True)

        # # Copy
        # startcall="Cp %s %s" % (hessIN,hessTMP)
        # subprocess.call(startcall, shell=True)
        
        #----- RUN PBS:  (B^T)B --------------------------------------------
        hesstag = 'HessGN_'+self.prefix
        self.gnhess_obj.GNHESS_run(self.param_reader,hesstag,True,True,hessIN,hessTMP)

        #----- CONVERT FROM FULL MODEL WITH ABC PADDING TO SPARSE MODEL -----------------------
        unpad="Window3d f1=%d f2=%d f3=%d n1=%d n2=%d n3=%d < %s > %s1" % (
            self.fz,self.fy,self.fx,self.nzr,self.nyr,self.nxr,hessTMP, hessOUT)
        derivop2 = "%s/APPLY_Dop_RBF3d.x rbftable=%s rbfcoord=%s masksalt=%s.trimmed scaling=%s verbose=0 adjoint=1 par=%s < %s1 > %s" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, hessOUT, self.hess_out_path)
        # endcall="%s; %s; rm %s1" % (unpad,derivop2,hessOUT)
        endcall="%s; %s;" % (unpad,derivop2)
        print(endcall)
        subprocess.call(endcall, shell=True)





        # # TEST BOTH FOR ADJOINTNESS
        # derivop1 = "%s/APPLY_Dop_RBF3d.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=0 adjoint=0 par=%s < %s > %s1" % (
        #     self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, self.hess_in_path, hessIN)
        # pad="Pad extend=0 beg1=%d beg2=%d beg3=%d n1out=%d n2out=%d n3out=%d < %s1 > %s; rm %s1" % (
        #     self.fz,self.fx,self.fy,self.nz,self.nx,self.ny,hessIN, hessIN, hessIN)
        # startcall="%s; %s" % (derivop1,pad)
        # print(startcall)
        # subprocess.call(startcall, shell=True)

        # # Copy
        # startcall="Cp %s %s" % (hessIN,hessOUT)
        # subprocess.call(startcall, shell=True)

        # unpad="Window3d f1=%d f2=%d f3=%d n1=%d n2=%d n3=%d < %s > %s1" % (
        #     self.fz,self.fx,self.fy,self.nzr,self.nxr,self.nyr,hessOUT, hessOUT)
        # derivop2 = "%s/APPLY_Dop_RBF3d.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=0 adjoint=1 par=%s < %s1 > %s" % (
        #     self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, hessOUT, self.hess_out_path)
        # endcall="%s; %s; rm %s1" % (unpad,derivop2,hessOUT)
        # print(endcall)
        # subprocess.call(endcall, shell=True)


        # # TEST THE PADDING FOR ADJOINTNESS
        # pad="Pad extend=0 beg1=1000 n1out=80000 < %s > %s;" % (
        #     self.hess_in_path, hessIN)
        # subprocess.call(pad, shell=True)
        # # Copy
        # startcall="Cp %s %s" % (hessIN,hessOUT)
        # subprocess.call(startcall, shell=True)
        # unpad="Window3d f1=1000  n1=70849  < %s > %s" % (
        #     hessOUT, self.hess_out_path)
        # subprocess.call(unpad, shell=True)


        # # TEST THE RBF KERNEL FOR ADJOINTNESS
        # derivop1 = "%s/APPLY_Dop_RBF3d.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=0 adjoint=0 par=%s < %s > %s" % (
        #     self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, self.hess_in_path, hessIN)
        # print(derivop1)
        # subprocess.call(derivop1, shell=True)
        # # Copy
        # startcall="Cp %s %s" % (hessIN,hessOUT)
        # subprocess.call(startcall, shell=True)
        # derivop2 = "%s/APPLY_Dop_RBF3d.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=0 adjoint=1 par=%s < %s > %s" % (
        #     self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, hessOUT, self.hess_out_path)
        # print(derivop2)
        # subprocess.call(derivop2, shell=True)


        # # TEST THE GNHESSIAN FOR ADJOINTNESS
        # hesstag = 'HessGN_'+self.prefix
        # self.gnhess_obj.GNHESS_run(self.param_reader,hesstag,True,True,self.hess_in_path,self.hess_out_path)

        return




if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------- APPLY GAUSS NEWTON HESSIAN TO GRADIENT ----------------
    start_time = time.time()
    make_hessGN(param_reader)
    elapsed_time = (time.time() - start_time) / 60.0
    print("ELAPSED TIME FOR BTE Hessian (min) : ")
    print(elapsed_time)
