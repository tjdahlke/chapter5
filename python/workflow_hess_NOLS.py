#!/usr/local/bin/python

import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import pbs_util
from main_functions3d import *
from inversion3d import *
from source_inversion3D import *
from linesearchRBF_adaptive import *


#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------ INITIALIZE SOME STUFF ----------------------
    dict_args = param_reader.dict_args
    levelset_iter = int(dict_args['levelset_iter'])
    dpIncrease=1.25
    betaDecrease=0.75
    Ntries = 6
    debug = True
    LStomoupd=True
    LSphiupd=True

    inversionname = param_reader.dict_args['inversionname']
    vel_path = param_reader.dict_args['vel_path']
    velback  = param_reader.dict_args['velback']
    syn_path = param_reader.dict_args['syn_path']
    res_path = param_reader.dict_args['res_path']
    phi_path = param_reader.dict_args['phi_path']
    masksalt = param_reader.dict_args['masksalt']
    maskvelb = param_reader.dict_args['maskvelb']
    pathtemp = param_reader.dict_args['pathtemp']
    wavelet  = param_reader.dict_args['wavelet']
    obs_path = param_reader.dict_args['obs_path']
    obs_pathSI = param_reader.dict_args['obs_pathSI']
    sparsemodel = param_reader.dict_args['sparsemodel']
    nt = param_reader.dict_args['nt']

    # RBF=int(param_reader.dict_args['RBF'])
    # phiANDvelb=int(param_reader.dict_args['phiANDvelb'])
    # phiANDvelb=False
    # hessian=int(param_reader.dict_args['hessian'])
    # masking=int(param_reader.dict_args['masking'])
    # maxbetaR=float(param_reader.dict_args['maxbetaR'])

    # phigrad = param_reader.dict_args['phigrad']
    rbfmask = param_reader.dict_args['rbfmask']
    tomograd = param_reader.dict_args['tomograd']
    pathtemphess = param_reader.dict_args['pathtemphess']

    hess_out_path = param_reader.dict_args['hess_out_path']
    rtm_path  = param_reader.dict_args['rtm_path']
    rtm_path1 = rtm_path+'1'

    hessian = int(param_reader.dict_args['hessian'])
    RBF = int(param_reader.dict_args['RBF'])
    masking = int(param_reader.dict_args['masking'])
    maxbetaR = float(param_reader.dict_args['maxbetaR'])

    # INITIALIZING THE MAIN FUNCTION SPACES. FUNCTIONS THAT CALL OTHER FUNCTIONS RELY ON THESE OBJECT INSTANCES
    main = MAIN_FUNCTIONS(param_reader)
    wave_modeling = make_MODELED_DATA3d(param_reader)
    born_modeling =make_BORN(param_reader)
    mainLS = MAIN_LS_FUNCTIONS(debug, param_reader, wave_modeling, main, inversionname, Ntries, LStomoupd, LSphiupd)
    inversion = inversion(param_reader,inversionname)
    souInvname = 'sourceInv_'+ inversionname
    souInv=SOURCE_INVERSION(param_reader,souInvname)
    masksaltFULL=masksalt+'_FULL'




    ###################################################################################################
    #----------- BEGIN LEVELSET ITERATIONS ------------------------------------------------------------
    for i in range(0, levelset_iter):
        print("###################################################################################################")
        print("########################      LEVELSET ITERATION   %s / %s   ######################################") % (i, (levelset_iter - 1))
        print("###################################################################################################")
        
        #------- DO SOURCE INVERSION -------------------------------------------------
        UnScaledWavelet=wavelet+'_unscaled'
        souInv.souInvMAIN(param_reader, True, UnScaledWavelet, obs_pathSI)

        # Pad wavelet to be the size of the DATA
        main.PAD_WAVELET(debug,UnScaledWavelet,nt)

        # Scale wavelet to small amplitude for numerical stability 
        ScaleFactor=-1.0
        main.SCALE(debug,UnScaledWavelet,wavelet,ScaleFactor)

        #------- MAKE SYNTHETIC DATA AND RESIDUAL -------------------------------------------------
        resname='residual_'+inversionname
        jobtype="rtm"
        normtag='normval_MAIN'
        wave_modeling.MODELED_DATA(param_reader, debug, resname, jobtype, vel_path, False, wavelet, syn_path)

        #-------- CALCULATE THE OBJECTIVE FUNCTION VALUE  ---------------------------------------------
        new_objfuncval = main.CALC_OBJ_VAL(debug, resname)
        print(">>>>>>>>>>>> Objective function value: %s" % (new_objfuncval))

        print(" ====================================================================================")
        #------- MAKE FULL RTM GRADIENT ------------------------------------------
        gradtag = 'gradient_'+inversionname
        dataAlreadySplit=True
        wavefield=False
        born_modeling.BORN_run(param_reader, gradtag, False, debug, True, rtm_path, resname, wavefield, dataAlreadySplit)
        phiANDvelb=False


        print("== DOING TOMOGRAPHIC UPDATE ====")
        tomogradTMP1 = tomograd+'_TMP1'
        rtm_pathRBF1 = rtm_path+'_unpadded'

        main.MAKE_FWIMASK(debug, 0, maskvelb)
        main.WINDOW3D_PAD(debug, rtm_path, rtm_pathRBF1)
        tomo=1

        if (hessian):
            #----- For PLAIN WITH masking ------------------------------------------
            main.HADAMARD_PRODUCT(debug,rtm_pathRBF1,maskvelb,tomogradTMP1)
            inversion.hessMAIN(param_reader, debug, True, False, masking, maskvelb, tomo, phiANDvelb, "junk", True, tomogradTMP1, hess_out_path)
            main.SCALE(debug, hess_out_path, tomograd, 1.0)
        else:
            main.HADAMARD_PRODUCT(debug,rtm_pathRBF1,maskvelb,tomogradTMP1)
            main.SCALE(debug, tomogradTMP1, tomograd, -1.0)

        # main.SCALE(debug, tomograd, phigrad, 0.0)

        #----- Calc maxalpha ----------------------------------------------------
        maxbeta = main.CALC_MAXBETA_RBF(debug,0.05,tomograd,velback)

        #------- LINE SEARCH ---------------------------------------------------
        final_alpha=mainLS.LINE_SEARCH_FWI(maxbeta, new_objfuncval, tomograd, betaDecrease)
        final_beta=0.0

        #------- SAVE THE OUTPUTS FROM THIS ITERATION  -------------------------
        main.SAVE_INTERMEDIATE_FILES(debug,inversionname)
        main.SAVE_INTERMEDIATE_VALUES(debug, new_objfuncval, maxbeta, final_alpha, final_beta)

        #------- BUILD MODEL FROM NEW PHI AND MAXBETA  -------------------------
        newphi_path="%s_GUESS_%s" % (phi_path,i)
        new_velback="%s_GUESS_%s" % (velback,i)



        mainLS.UPDATE_MODEL_LS3d_FWI(debug, final_alpha, tomograd, vel_path, new_velback)
        # main.COPY(debug,newphi_path,phi_path)
        main.COPY(debug,new_velback,vel_path)

        #-------- CLEAN THE WORKING DIRECTORY   --------------------------------
        main.CLEAN_UP(debug,inversionname)

        # #-------- REMOVE THE ESTIMATED SOURCE WAVELET   --------------------------------
        # main.RM_SRC_WAVELET(debug)


