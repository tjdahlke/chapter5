# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM THAT DOES A RECURSIVE ADD
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands

# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
binpath = sys.argv[1]
outputFileName = sys.argv[2]
inputlist = sys.argv[3]

# Get the inputfile list
aa=open(inputlist)
inputfiles = [x.strip() for x in aa.readlines()]
print(inputfiles)

# Initialize
k=1
working=" "
outfile='%s-tmp%d.h' % (outputFileName,k)
oldoutfile='%s-tmp%d.h' % (outputFileName,(k-1))
inputs="file1=%s file2=%s file3=%s " % (inputfiles[0],inputfiles[1],inputfiles[2])


# Run the main loop
while (True):
    cmd1 = "%s/ADD.x %s > %s" % (binpath,inputs,outfile)
    print(cmd1)
    commands.getoutput(cmd1)
    working=outfile

    cmd2 = "rm -f %s" % (oldoutfile) # Removes the working file to keep the scrach space from filling up
    print(cmd2)
    commands.getoutput(cmd2)

    k=k+1
    if ((2*k+1)>(len(inputfiles))):
        break

    oldoutfile=outfile
    outfile='%s-tmp%d.h' % (outputFileName,k)

    inputs="file1=%s file2=%s file3=%s " % (working,inputfiles[2*k-1],inputfiles[2*k])



# Add last file if odd number of files
if ((2*k)==len(inputfiles)):
    print("=====================================================")
    outfile='%s-tmp%d.h' % (outputFileName,k)
    inputs="file1=%s file2=%s " % (working,inputfiles[2*k-1])
    cmd1 = "%s/ADD.x %s > %s" % (binpath,inputs,outfile)
    cmd2 = "rm -f %s" % (oldoutfile) # Removes the working file to keep the scrach space from filling up
    cmd = "%s; %s" % (cmd1,cmd2)
    print(cmd)
    commands.getoutput(cmd)


# Write to output filename
cmd= "Cp %s %s" % (outfile,outputFileName)
print(cmd)
commands.getoutput(cmd)

# # Remove the old tmp files
# cmd = "rm -f %s" % (outfile)
# print(cmd)
# # commands.getoutput(cmd)
