# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM THAT CALCULATES THE L2 NORM FOR A SINGLE FILE
#
#   INPUT:      residual##.H seplib file
#   OUTPUT:     obj##.txt text file with the norm value written
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands

# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
inputFileName1 = sys.argv[1]
inputFileName2 = sys.argv[2]
outputFileName = sys.argv[3]

# Calculate the norm
cmd1 = "Solver_ops op=dot file1=%s file2=%s" % (inputFileName1,inputFileName2)
aa=commands.getstatusoutput(cmd1)
print(aa)
dotval=float(aa[1].split()[2])
print(dotval)

# Write it to a text file
cmd1 = "echo %.12f > %s" % (dotval,outputFileName)
aa=commands.getstatusoutput(cmd1)