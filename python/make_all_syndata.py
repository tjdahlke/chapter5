#!/usr/local/bin/python

import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import pbs_util
from main_functions3d import *
from modeled_data3d import *


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------ INITIALIZE SOME STUFF ----------------------
    dict_args = param_reader.dict_args
    debug = True

    inversionname = param_reader.dict_args['inversionname']
    pathtemp = param_reader.dict_args['pathtemp']
    wavelet  = param_reader.dict_args['wavelet']
    syn_path="%s/AllSyndata.H" % (pathtemp)
    migvel = param_reader.dict_args['velmodel']
    prefix = param_reader.dict_args['prefix']
    param_reader.dict_args['vel_path'] = migvel
    flag = param_reader.dict_args['flag']

    param_reader.dict_args['hess_out_path'] = 'junk'
    wave_modeling = make_MODELED_DATA3d(param_reader)

    print("###################################################################################################")
    print("########################      MAKE ALL SYNDATA   #######################################################")
    print("###################################################################################################")

    #------- MAKE SYNTHETIC DATA -------------------------------------------------
    wave_modeling.MODELED_DATA(param_reader, debug,prefix, flag, migvel, False, wavelet, syn_path)
    # wave_modeling.MODELED_DATA(param_reader, debug,prefix, 'standard', migvel, False, wavelet, syn_path)
    # wave_modeling.MODELED_DATA(param_reader, debug,prefix, 'mig', migvel, False, wavelet, syn_path)









