#!/usr/bin/python

import time
import subprocess
import math
import copy
from batch_task_executor import *
from born3d import *


class GNHESS(BatchTaskComposer):
    def __init__(self, param_reader, vel_path, prefix, modelpath, datapath, debug):
        BatchTaskComposer.__init__(self)

        # Read in the job parameters
        self.param_reader=param_reader
        self.dict_args = self.param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        # self.wavelet = self.dict_args['wavelet']
        self.nshots = int(self.dict_args['nfiles'])
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.prefix = prefix
        self.vel_path = vel_path
        self.datapath = datapath
        self.modelpath = modelpath
        # self.genpar = self.dict_args['genpar']
        # self.rand_path = self.dict_args['rand_path']

        # self.singularity = self.dict_args['singularity']
        # self.souhead = self.dict_args['souhead']
        self.recpos = self.dict_args['recpos']
        # self.randpad = int(self.dict_args['randpad'])
        # self.abcs = self.dict_args['abcs']

        #--------------------------------------------------------------------
        # Read in the node position list (since we propagate from the nodes)
        recpos=[]
        tmp=[]
        with open(self.recpos) as f:
            for i, line in enumerate(f):
                line = line.strip()
                tmp=line.split()
                recpos.append(tmp)
        self.recposlist=recpos

        # Create all subjobs info
        self.njobs = int(math.ceil(self.nshots/float(self.nsh_perjob)))
        self.subjobids = range(0, self.njobs)
        self.subjobfns_list = [None]*self.njobs
        self.subjobIND_list = range(self.ish_beg, self.nshots+1, self.nsh_perjob) # List of actual shot number (starting @ 0)

        # New arrays for
        self.shotjobids = range(1, self.nshots+1)
        self.shotjobfns_list = [None]*self.nshots

        # Make a list of all the files for each job (shots per job)
        for i in range(0,self.njobs):
            ish_start = self.subjobIND_list[i]
            nsh = min(self.nsh_perjob, self.nshots-ish_start)
            self.subjobfns_list[i] = [None]*nsh
            for j in range(0,nsh):
                souId=int(float(self.recposlist[ish_start+j][0]))
                self.subjobfns_list[i][j] = '%s/%s-%s.H' % (self.path_out,self.prefix,souId)
                self.shotjobfns_list[ish_start+j] = self.subjobfns_list[i][j]
        return



    def GetSubjobScripts(self, subjobid):
        # Return the scripts content for that subjob
        subjobfns = self.subjobfns_list[subjobid]

        # Build scripts that create the files designated in subjobfns
        scripts = []
        cnt = 0
        nf_act = len(subjobfns) # number of files

        # For each shot in the subjob, we create the command script segment
        for fn in subjobfns:
            shot = self.nsh_perjob*subjobid+cnt+1
            cnt += 1
            if cnt > nf_act: break
            print("Making commands for shot: %d: filename: %s " % ((shot-1),fn))
            shotscript,junk = self.GetShotScript(fn, shot)
            scripts.append(shotscript)

        return scripts, str(subjobid)



    def GetShotScript(self, fn, shot):
        wavefield=False
        debug=False
        dataAlreadySplit=True

        # Forward BORN
        born_instanceF=BORN(self.param_reader, self.vel_path, self.prefix, False, self.modelpath, self.prefix, debug, wavefield, dataAlreadySplit)
        shotscriptF,junk = born_instanceF.GetShotScript(fn, shot)

        # Adjoint BORN
        born_instanceA=BORN(self.param_reader, self.vel_path, self.prefix, True, self.datapath, self.prefix, debug, wavefield, dataAlreadySplit)
        shotscriptA,junk = born_instanceA.GetShotScript(fn, shot)

        # GN HESS command string
        script="%s; \n %s" % (shotscriptF, shotscriptA)
        print(script)

        return script, str(shot)


    def GetSubjobsInfo(self):
        # Should return two lists, subjobids and subjobfns
        return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)

    def GetShotjobsInfo(self):
        # Should return two lists, shotjobids and shotjobfns
        return copy.deepcopy(self.shotjobids), copy.deepcopy(self.shotjobfns_list)



class make_GNHESS(object):
    def __init__(self, param_reader):
        dict_args = param_reader.dict_args
        self.vel_path = dict_args['vel_path']
        self.path_out = dict_args['path_out']
        self.pypath = dict_args['pypath']
        self.T_BIN = dict_args['T_BIN']
        return


    def GNHESS_run(self, param_reader,fileprefix,cleanup,debug,model,data):
        #----- RUN GNHESS OPERATION -------------------
        gnhessjob = GNHESS(param_reader, self.vel_path, fileprefix, model, data, debug)
        bte = BatchTaskExecutor(param_reader)
        if (debug):
            print('-----------------------------------------------------------------')
            print(' RUNNING 3D GNHESS OPERATOR')
        param_reader.prefix=fileprefix
        bte.LaunchBatchTask(fileprefix, gnhessjob)

        # Group the output files
        _, fns_list = gnhessjob.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]


        # Stack the partially stacked subjob GNHess outputs
        cmd = "python -u %s/recursive_add.py %s %s '%s/%s-*.H | sort -V' "  % (self.pypath,self.T_BIN,data,self.path_out,fileprefix)
        if (debug):
            print('-----------------------------------------------------------------')
            print(cmd)
        subprocess.call(cmd,shell=True)

        #----- Removing ADJOINT and FORWARD GNHESS RBF output files from wrk directory -------------
        if (int(cleanup)==1):
            junk=''
            for i in range(0,len(fn_seph_list)):
                junk = '%s %s' % (junk,fn_seph_list[i])
            cmd = 'rm -rf %s' % (junk)
            subprocess.call(cmd,shell=True,stdout=False)
            print "--------- Removing 3D ADJOINT and FORWARD GNHESSRBF output files from wrk directory  ------------------------------"
            print(cmd)
        return



if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------- RUN GNHESS ON ONE NODE GATHER ----------------
    start_time = time.time()
    datapath = dict_args['datapath']
    modelpath = dict_args['modelpath']
    gnhess_obj=make_GNHESS(param_reader)
    gnhess_obj.GNHESS_run(param_reader,True,modelpath,datapath)

    elapsed_time = (time.time() - start_time)/60.0
    print("ELAPSED TIME FOR GNHESS (minutes) : ")
    print(elapsed_time)
