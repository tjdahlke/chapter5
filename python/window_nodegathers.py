# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM GRABS NODE GATHERS AND DOES PROCESSING ON THEM
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess
import pbs_util


# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
recfile = sys.argv[1]
obsdata = sys.argv[2]
writepath = sys.argv[3]
pbsTemplate = sys.argv[4]
pythonpath = sys.argv[5]

cmd1="less %s | wc -l" % (recfile)
stat1,numNodes=commands.getstatusoutput(cmd1)
recList = list(open(recfile))

# cwd = os.getcwd()


# Visit each location to grab the binned shot data
for ii in range(0,int(numNodes)):
# for ii in range(0,10):

    print("----------------------------------------")
    percent=100.0*ii/float(numNodes)
    percentcmd="percent done:  %f " % (percent)
    print(percentcmd)

    nodeId=int(str(recList[ii]).split()[0])
    outputFileName="%s/obsNode_%d.H" % (writepath,nodeId)
    temp2="%s/temp2_%d.sh" % (writepath,nodeId)
    finalPBS="%s/PBSscript_%d.sh" % (writepath,nodeId)


    # Check if file already exists
    check="ls %s" % (outputFileName)
    stat2,junk=commands.getstatusoutput(check)
    if (stat2>0): # If file doesn't exist
        runjob=True
    else:
        # check if file is empty
        file_error = pbs_util.CheckSephFileError(outputFileName,False)
        if file_error == 0:
            print "Target file is good, skip: %s" % outputFileName
            runjob=False
        else: # File is bad
            print "Target file %s is BAD. Recomputing!" % outputFileName
            runjob=True

    if (runjob):

        rmcmd="rm -f %s %s;" % (temp2,finalPBS)
        print(rmcmd)
        subprocess.call(rmcmd,shell=True)

        cmdp1="echo '#PBS -o %s/logfile-%s.txt' > %s" % (writepath,nodeId,temp2)
        cmdp2="echo '#PBS -e %s/logfile-%s.txt' >> %s" % (writepath,nodeId,temp2)
        cmdp3="echo '#PBS -N pbsjob-%s' >> %s" % (nodeId,temp2)
        cmdp4="echo 'setenv DATAPATH %s/' >> %s" % (writepath,temp2)
        cmdp="%s; %s; %s; %s;" % (cmdp1, cmdp2, cmdp3, cmdp4)
        print(cmdp)
        subprocess.call(cmdp,shell=True)


        # Run the job for a single node
        cmd="python %s/window_nodegather_SINGLE.py %d %s %s" % (pythonpath,nodeId,obsdata,outputFileName)
        cmdW1="echo '%s' >> %s;" % (cmd,temp2)
        print(cmdW1)
        subprocess.call(cmdW1,shell=True)

        cmdW3="cat %s %s > %s;" % (pbsTemplate,temp2,finalPBS)
        print(cmdW3)
        subprocess.call(cmdW3,shell=True)
        cmdW4="qsub -l nodes=1:ppn=16 -d %s %s;" % (writepath,finalPBS)
        # cmdW4="qsub -l nodes=1:ppn=24 -d %s %s;" % (writepath,finalPBS)
        print(cmdW4)
        subprocess.call(cmdW4,shell=True)





