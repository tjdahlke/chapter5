''' Python functions written by Yang Zhang, somewhere between 2009-2016'''
import array
import commands
import inspect
import sys,re,os,string
import numpy as np
import ConfigParser

sepbin=os.environ["SEP"]+"/bin"
debug=0



def msg(strng):
  """Print out a message to the screen, do a flush to guarantee immediate action"""
  lines=strng.split('\n')
  for line in lines:
    print  "     %s"%line
  sys.stdout.flush()

def err(m=""):
  """Quit with an error first printing out the string m"""
	#self_doc();
  if  debug==0:
    msg( m)
    sys.exit(-1)
  else:
    raise error,m


def parse_args(args):
  eqs={}
  aout=[]
  eqs["basic_sep_io"]="0"
  eq=re.compile("^(.+?)=(.+)$")
  for arg in args:
    a=eq.search(arg)
    if a:
       eqs[a.group(1)]=a.group(2)
    else:
       aout.append(arg)
  return eqs,aout

def get_sep_his_par(file, par):
  """Return the par=? term from the .H file."""
  stat1,out1=commands.getstatusoutput("%s/Get parform=no <%s %s"%(sepbin,file,par))
  assert stat1==0, err("!Trouble reading param %s from file %s" % (par, file))
  return out1


def get_sep_axis_params(file, iaxis):
  """Note that iaxis is 1-based, returns a list of *strings* [n,o,d,label]."""
  stat1,out1=commands.getstatusoutput("%s/Get parform=no <%s n%d"%(sepbin,file,iaxis))
  stat2,out2=commands.getstatusoutput("%s/Get parform=no <%s o%d"%(sepbin,file,iaxis))
  stat3,out3=commands.getstatusoutput("%s/Get parform=no <%s d%d"%(sepbin,file,iaxis))
  stat4,out4=commands.getstatusoutput("%s/Get parform=no <%s label%d"%(sepbin,file,iaxis))
  if stat1:
    err("Trouble reading parameters about axis%d from %s" % (iaxis, file))
  if len(out1)==0: out1="1"
  if len(out2)==0: out2="0"
  if len(out3)==0: out3="1"
  if len(out4)==0: out4=" "
  return [out1, out2, out3, out4]



def put_sep_axis_params(file, iaxis, ax_info):
  """Note that ax_info is a list of *strings* [n,o,d,label]."""
  assert iaxis > 0
  cmd = "echo n%d=%s o%d=%s d%d=%s label%d=%s >>%s" % (iaxis,ax_info[0], iaxis,ax_info[1], iaxis,ax_info[2], iaxis,ax_info[3], file)
  RunShellCmd(cmd)
  return


def RunShellCmd(cmd, print_cmd=False, print_output=False):
  if print_cmd:
    print "RunShellCmd: %s" % cmd
  stat1,out1=commands.getstatusoutput(cmd)
  if stat1 != 0:
    assert False, 'Shell cmd Failed: %s!' % out1
  if print_output:
    print "CmdOutput: %s" % out1
  return


def GetScratchPath():
	scratchpath=''
	PWD=os.environ['PWD']
	HOME=os.environ['HOME']

	stat1,hostname=commands.getstatusoutput ('hostname -s')
	if stat1 != 0:
		assert False, 'Shell hostname cmd Failed: %s!' % hostname

	cmd = "grep %s %s/.datapath" % (hostname, PWD)
	stat1,scratchpath=commands.getstatusoutput (cmd)
	if stat1 == 0:
		junk, scratchpath = scratchpath.split('=')
		return scratchpath

	cmd = "grep '^datapath' %s/.datapath" % (PWD)
	stat1,scratchpath=commands.getstatusoutput (cmd)
	if stat1 == 0:
		junk, scratchpath = scratchpath.split('=')
		return scratchpath

	cmd = "grep %s %s/.datapath" % (hostname, HOME)
	stat1,scratchpath=commands.getstatusoutput (cmd)
	if stat1 == 0:
		junk, scratchpath = scratchpath.split('=')
		return scratchpath

	cmd = "grep '^datapath' %s/.datapath" % (HOME)
	stat1,scratchpath=commands.getstatusoutput (cmd)
	if stat1 == 0:
		junk, scratchpath = scratchpath.split('=')
		return scratchpath

	DATAPATH=os.environ.get('DATAPATH', '/tmp/')
	return DATAPATH



def parse_args(args):
  eqs={}
  aout=[]
  eqs["basic_sep_io"]="0"
  eq=re.compile("^(.+?)=(.+)$")
  for arg in args:
    a=eq.search(arg)
    if a:
       eqs[a.group(1)]=a.group(2)
    else:
       aout.append(arg)
  return eqs,aout


def RetrieveAllEqArgs(eq_args_from_cmdline):
  '''Read other parameters from the .param file and combine it with the cmdline arguments. Notice that entries in eq_args will take priority if there are duplicate keys.'''
  eq_args = eq_args_from_cmdline
  if ('param' in eq_args):
    param_file = eq_args['param']
    dict_args = GetParamsFromIniFile(param_file, None)
    dict_args.update(eq_args)
  else:
    dict_args = eq_args
  # Find the current user name
  if 'user' not in dict_args:
    dict_args['user'] = os.environ['USER']
  return dict_args



def ConfigurationFromIniFile(param_file):
  '''Parse a Ini format parameter file, and return the config object.'''
  config = ConfigParser.ConfigParser()
  config.optionxform = str  # Set the configparser to preserve case, by default config Parser makes everything lowercase.
  config.read(param_file)
  return config


def GetParamsBySection(config, section):
  '''Return the parameters under a certain section as a dictionary.
  If section is None, then return all sections.
  '''
  sections_all = config.sections()
  secs = []
  if section in sections_all:
    secs.append(section)
  else:
    assert section is None, 'Un Recognized section name %s !' % section
    secs = sections_all
  dict = {}
  for sec in secs:
    for key in config.options(sec):
      val = config.get(sec, key)
      #print "[%s].%s = " % (sec, opt), val, type(val)
      assert key not in dict, 'Found duplicate entries for key=%s' % key
      dict[key] = val
  return dict


def GetParamsFromIniFile(param_filename, section=None):
  config = ConfigurationFromIniFile(param_filename)
  return GetParamsBySection(config, section)




# function expects data to be a numpy ndarray
def sep_write_scratch (history, data):

  # test the data is numpy array, and history file is not an empty string
  assert 'numpy' in str(type(data))
  assert history

  scratchpath = GetScratchPath()
  tmp = history.split('/')
  history_tail = tmp[len(tmp)-1]

  scratch = scratchpath+history_tail+'@'
  cmd = "echo in=%s >> %s" % (scratch, history)
  RunShellCmd (cmd)

  with open (scratch, 'wb') as f:
    data.tofile (f)
    f.close()


# SEP read to npy function written by Taylor Dahlke 11/13/2017
def sep_read_scratch(history):
  # test that history file is not an empty string
  assert history
  # Grab the binary path
  scratch=get_sep_his_par(history, 'in')
  formatN=get_sep_his_par(history, 'data_format')
  esize=get_sep_his_par(history, 'esize')
  n1=int(get_sep_axis_params(history, 1)[0])
  n2=int(get_sep_axis_params(history, 2)[0])
  # Open and read the file as 1D array
  if ((formatN=='xdr_float') or (esize=='4')):
    with open(scratch, 'r') as f:
      data1d = np.fromfile(f, '>f4')
  else:
    print("Not sure how to read this binary")
    sys.exit()
  # Reshape data to 2D
  data2d = np.reshape(data1d, [n2, n1])
  return data2d


