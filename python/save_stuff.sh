#!/bin/bash


mkdir $1

Cp tmp/tmp_inv/phigrad.Hcomp $1/phigrad.Hcomp datapath=$1/

Cp tmp/tmp_inv/vel_full.Hcomp $1/vel_full.Hcomp datapath=$1/

Cp tmp/tmp_inv/RTMgrad.Hcomp $1/RTMgrad.Hcomp datapath=$1/

Cp tmp/tmp_inv/phi_path.Hcomp $1/phi_path.Hcomp datapath=$1/

Cp tmp/tmp_inv/tomograd.Hcomp $1/tomograd.Hcomp datapath=$1/

Cp tmp/tmp_inv/masksalt.Hcomp $1/masksalt.Hcomp datapath=$1/

Cp tmp/tmp_inv/vel_back_guess.Hcomp $1/vel_back_guess.Hcomp datapath=$1/

Cp tmp/tmp_inv/wavelet.Hcomp $1/wavelet.Hcomp datapath=$1/

Cp tmp/tmp_inv/scaling.Hcomp $1/scaling.Hcomp datapath=$1/

Cp tmp/tmp_rbf/rbf_model.Hcomp $1/rbf_model.Hcomp datapath=$1/

Cp centers.h $1/centers.h datapath=$1/

Cp msb.h $1/msb.h datapath=$1/

cp maxbetaval.txt $1/.

cp tmp/tmp_inv/objfuncval.txt  $1/.

cp Par/cardamomS.p $1/.

cp Makefile $1/.

cp ../python/workflow_hess_RBF3d.py $1/.

cp screenlog.0 $1/.

