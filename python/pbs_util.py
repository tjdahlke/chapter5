import commands,os
import pickle
import sepbase
import time
import subprocess
from timeit import default_timer as timer
# from os.path import abspath
from sepbase import line_no

def CheckPrevCmdResultCShellScript(prev_cmd):
  '''Return a piece of script that checks the return status of the previous cmd executed in the same script.'''
  cmd = '''
if ( $status != 0 ) then
  echo ! The prev cmd failed: \" %s \"
  exit -1
endif
''' % (prev_cmd)
  return cmd

def CheckSephFileError(fn_seph, check_binary=False):
  '''Check whether a .H file is a valid one, i.e. has 100% of data and has no NaN(not a number) in it.
  Args:
    fn_seph: The name of the file to be checked.
    check_binary: if False, then only check the existence and size; if True, then check in addition whether there is nan.
  Returns:
    0 if the file is OK. 1 if it has Nan; -1 if there is problem with the file itself, like not exist or binary is incomplete.
  '''
  print("--------------------------------------------")
  if not check_binary:  # Use In3d command to check.

    # Check if file exists
    cmd0 = "ls %s " % fn_seph
    stat0,out0=commands.getstatusoutput(cmd0)
    if (int(stat0) != 0):  # File does not exist
      print("File does not exist")
      return -1

    # Check if binary is present
    cmd0 = "Get parform=n < %s in " % fn_seph
    junk,binfile=commands.getstatusoutput(cmd0)
    cmd0 = "du %s" % binfile
    stat0,out0=commands.getstatusoutput(cmd0)    
    if (int(stat0) != 0):  # File binary cannot be calculated (not present?)
      print("Binary has no size!")
      return -1


    # Search for indications of binary portion being complete.
    cmd1 = "In3d %s " % fn_seph
    # print(cmd1)
    stat1,out1=commands.getstatusoutput(cmd1)
    # print(stat1)
    # print(out1)
    pos = out1.find('[  100./')
    if pos == -1:
      print("Binary portion doesn't seem complete")
      return -1
    else:
      return 0
  else:  # Use Attr to check the values of the binary part as well.
    cmd1 = "Attr < %s " % fn_seph
    # print(cmd1)
    stat1,out1=commands.getstatusoutput(cmd1)
    if (int(stat1) != 0):  # File content is not complete.
      print("File content is not complete")
      return -1
    nan_pos = out1.find('nan')
    if nan_pos == -1:  # not find not-a-number, file content is valid.
      return 0
    else:
      print("File content seems to have Nan's")
      return 1

def GenShotsList(param_reader):
  '''Figure out the indices for the shots we need to run/compute.
  Return (ishot_list, nshot_list), the first shot index in each job, and the number of shots in each job.'''
  dict_args = param_reader.dict_args
  input_is_file_list = "ish_beglist" in dict_args
  if not input_is_file_list:
    # Use ish_beg, the shots are generated as an arithmetic sequence.
    assert "ish_beg" in dict_args, (
        "!Need to supply one of the two: ish_beg or ish_beglist!")
  else:
    assert "ish_beg" not in dict_args, (
        "!!! cannot have both: ish_beg & ish_beglist!")
  N = param_reader.nfiles
  n = param_reader.nfiles_perjob
  # If shots are grouped by shot_n1 amount, which means when dividing the shots for each job, don't cross the shot_n1 boundary
  shot_n1_bnd = dict_args.get("shot_n1_bnd")
  if shot_n1_bnd:  shot_n1_bnd = int(shot_n1_bnd)
  ish_start = 0; ishot_list = []; nshot_list = []
  if not input_is_file_list:
    ish_start = int(dict_args["ish_beg"])
    if not shot_n1_bnd:
      ishot_list = range(ish_start,N,n)
      nshot_list = [n]*(len(ishot_list)-1)
      nshot_list.append(N - ishot_list[-1])
    else: # divide the job such that it did not cross the given n1 boundary
      nearest_shot_n1_mult = (ish_start / shot_n1_bnd + 1) * shot_n1_bnd
      shot_end_1st_row = min(nearest_shot_n1_mult,N)
      ishot_list = range(ish_start, shot_end_1st_row, n)
      nshot_list = [n]*(len(ishot_list)-1)
      nshot_list.append(shot_end_1st_row - ishot_list[-1])
      for ii in range(nearest_shot_n1_mult, N, shot_n1_bnd):
        shot_end = min(N, ii+shot_n1_bnd)
        for ishot in range(ii,shot_end,n):
          ishot_list.append(ishot)
          nshot_list.append(min(shot_end-ishot,n))
  else:  # The input shots are listed in a file and might not be consecutive.
    file_ish_flist = dict_args["ish_beglist"]
    # read the list files to generate a list of shots
    fp_list = open(file_ish_flist,'r')
    while True:
      line = fp_list.readline();
      if not line:
        break
      else:  # Read in the number stored in this line into a number.
        line = line.strip()
        if line:
          ishot_list.append(int(line))
          nshot_list.append(1)  # Assume in this case, each job only does one shot.
    fp_list.close()
    ish_start = ishot_list[0]

  njobs_max = param_reader.njobs_max
  print "ish_start=%d:  njobs_max=%d" % (ish_start, njobs_max)
  if njobs_max < len(ishot_list):  del ishot_list[njobs_max:]
  return ishot_list, nshot_list


class ShotsInfo:
  def __init__(self, fn_shots_info):
    '''Helper class that manages the individual shot file information stored in a .shotsInfo file.'''
    self._fn_shots_info = fn_shots_info
    assert os.path.exists(fn_shots_info)
    # Read all the lines.
    fp = open(fn_shots_info,'r')
    self.lines = []
    for line in fp:
      line = line.strip()
      if line:
        self.lines.append(line)
    fp.close()

  def TotNumShots(self):
    return len(self.lines)-2

  def ShotFileNameAt(self,ish):
    '''Find the filename in the shotInfoFile corresponding to the shot index.
    ####and extract the receiver geometry.'''
    return self.lines[ish+2].split(" ")[0]  # Extract the first word, which is the filename.

  def ShotFileApertureAt(self, ish):
    '''Return a tuple of [xmin,xmax,ymin,ymax] indicating the aperture of the shot record.'''
    return map(float, self.lines[ish+2].split(" ")[1:])


class JobParamReader:
  '''Parse the common parameters for PBS jobs.'''
  def __init__(self, dict_args):
    self.dict_args = dict_args
    self.fname_template_script = dict_args['pbs_template']
    assert os.path.exists(self.fname_template_script)
    self.queues = dict_args.get('queues','default').split(',')
    self.queues_cap = dict_args.get('queues_cap')
    nqueue = len(self.queues)

    if self.queues_cap:  # Not None
      self.queues_cap = map(int, self.queues_cap.split(','))
      assert len(self.queues_cap) == nqueue, 'queues and queues_cap have different num_of_elements!'
    else:  # Provide default values.
      self.queues_cap = [15]*nqueue
    self.prefix = dict_args['prefix']  # The prefix used for generating filenames for intermediate/output datafiles.
    # The cap of total number of jobs in *each* queue at any given time.
    self.total_jobs_cap = dict_args.get('total_jobs_cap')
    if self.total_jobs_cap is not None:
      self.total_jobs_cap = map(int, self.total_jobs_cap.split(','))
      assert nqueue == len(self.total_jobs_cap)
    self.priority_num_Q_jobs = None
    if 'priority_num_Q_jobs' in dict_args:
      self.priority_num_Q_jobs = map(int, self.priority_num_Q_jobs.split(','))
    self.njobs_max = 1  # For now set it to 1.
    self.path_out = os.path.abspath(dict_args.get("path_out", os.getcwd()))
    assert os.path.exists(self.path_out)
    self.path_tmp = os.path.abspath(dict_args.get('path_tmp', '/tmp'))
    if self.dict_args.get('user') is None:
      self.user = os.environ['USER']
    else:
      self.user = dict_args['user']
    return

class ParallelParamReader(JobParamReader):
  '''A subclass that read extra parameters about job parallelization, mostly through sharding the input data.'''
  def __init__(self, dict_args):
    JobParamReader.__init__(self, dict_args)
    self.nfiles = int(dict_args['nfiles'])  # Total number of shots to simulate/generate/compute.
    self.nfiles_perjob = int(dict_args.get("nfiles_perjob", 1))
    assert dict_args.get("nfiles_perbatch") is None, 'Obsolete option nfiles_perbatch!!'
    self.njobs_max = int(dict_args.get('njobs_max', self.nfiles))  # Total number of jobs to simulate, will stop submission after such number of jobs have been submitted.


class PbsScriptCreator:
  """Given the input arg parameters, generate a PBS script body."""
  def __init__(self, param_reader):
    self.param_reader = param_reader
    self.dict_args = param_reader.dict_args
    self.user = param_reader.user
    self._starting_new_script = False
    self._job_filename_stem = None

  def CmdFinalCleanUpTempDir(self):
    cmd = '\n# Final clean up, remove the files at tmp folder.'
    cmd += "\nfind %s/ -maxdepth 1 -type f -user %s -exec rm {} \\;\n" % (
        self.param_reader.path_tmp, self.user)
    return cmd

  def AppendScriptsContent(self, scripts):
    '''Append the list of lines in 'scripts' to the underlying script file.
    Returns the script file name.'''
    prefix = self.param_reader.prefix
    if self._starting_new_script:  # Create the new script
      self._starting_new_script = False
      fname_template_script = self.param_reader.fname_template_script
      nnodes = int(self.dict_args.get('nnodes',1))  # By default, use one node per job
      logfile = self.fn_log.replace("/","\/")
      pathout_str = self.param_reader.path_out.replace("/","\/")
      cmd0 = "sed 's/#PBS -N jobname/#PBS -N %s-%s/' < %s | sed 's/#PBS -o/#PBS -o %s/' | sed 's/#PBS -e/#PBS -e %s/' | sed 's/#PBS -d/#PBS -d %s/' | sed 's/set CURRDIR=/set CURRDIR=%s/' > %s" % (
          prefix, self._job_filename_stem, fname_template_script,logfile,logfile,pathout_str,pathout_str,self.fn_script)
      sepbase.RunShellCmd(cmd0)
      if nnodes != 0:  # Need to change the number of nodes used for this job
        cmd1 = ("sed -i 's/#PBS\ -l\ nodes=1/#PBS\ -l\ nodes=%d/g'   %s" %
                (nnodes, self.fn_script))
        sepbase.RunShellCmd(cmd1)
    # Then write the content in scripts.
    ntries=30;
    itry=1;
    while True:
        try:
            fp_o = open(self.fn_script,'a');
            fp_o.writelines(scripts);
            fp_o.close()
            break
        except(IOError):
            print "Error opening file %s: retrying after 5 seconds for other %s times"%(self.fn_script,ntries-itry)
            os.system("sleep 5")  # Wait for retrying to open file again
            # os.system("sleep 30")  # Wait for retrying to open file again
            itry+=1
            if(itry==ntries): assert False, "Problem writing pbs script file %s! Stopping submitter!"%(self.fn_script)
    return self.fn_script


  def CreateScriptForNewJob(self, script_filename_stem):
    """Construct the pbs script from script_template.
    The actual script file write happens when you call the first AppendScriptsContent() after calling this funciton.
    Args:
      script_filename_stem: The basename (without extension) for the new script name.
      """
    self._starting_new_script = True
    self._job_filename_stem = script_filename_stem
    path_out = self.param_reader.path_out
    prefix = self.param_reader.prefix
    self.fn_script = '%s/%s-%s.sh' % (path_out, prefix, script_filename_stem)
    self.fn_log = '%s/%s-%s.log' % (path_out, prefix, script_filename_stem)
    return


class JobScriptor:
  def __init__(self, param_reader):
    self.param_reader = param_reader
    self.dict_args = param_reader.dict_args
    return

  def CmdFinalCleanUpTempDir(self):
    cmd = '\n# Final clean up, remove the files at tmp folder.'
    cmd += "\nfind %s/ -maxdepth 1 -type f -user %s -exec rm {} \\;\n" % (
        self.param_reader.path_tmp, self.param_reader.user)
    return cmd


  def NewJob(self, sz_shotrange):
    """Call this func first everytime you start a new job."""
    self.sz_shotrange = sz_shotrange
    path_out = self.param_reader.path_out
    prefix = self.param_reader.prefix
    self.fnt_output_list = []
    return

class PbsSubmitter:
  """Implements a greedy job submission strategy."""
  def __init__(self, queues_info=[('default',1)], total_jobs_cap=None, user=None, priority_num_Q_jobs=None,param_reader=None):
    """
    Args:
      queues_info: A list of queue names, ordered by the priorities(descending).
          Each tuple has two fields, [0] is the name of queue and [1] is the
          max_num_Q_jobs, i.e. maximum number of jobs pending in that queue
          (waiting for execution). In a normal scenario, the last queue should
          be 'default'.
      total_jobs_cap: The cap for running jobs + pending jobs for EACH queue, (for the sake of code simplicity, this is a per-queue cap).
      priority_num_Q_jobs:  for each queue, if the current num of jobs in Q status is smaller than this number, then it will be submitted to this queue as normal, but if the current num of Q jobs is bigger than this, the program will check other queues to see it can be submited other queue that is less crowded.
    """
    nq = len(queues_info)
    assert nq > 0
    self._queues_info = queues_info[:]
    if user is None:
      self._user_name = os.environ['USER']
    else:
      self._user_name = user
    if total_jobs_cap is None:
      self._total_jobs_cap = [200]*nq  # Can have at most 1000 jobs per queue on the cluster.
    else:
      assert nq == len(total_jobs_cap)
      self._total_jobs_cap = total_jobs_cap[:]
    if priority_num_Q_jobs is None:
      self._priroty_num_Q_jobs = [1]*nq
    else:
      self._priroty_num_Q_jobs = priority_num_Q_jobs[:]
      assert len(self._priroty_num_Q_jobs)==nq
    self.param_reader=param_reader
    #Defining variables for SSH submitter
    self.nodelist=[]
    #List with running processes [subprocess object,node name,job name,initial time]
    self.processes=[]
    # Added by TJD
    self.max_time=None 
    return

  # def WaitOnAllJobsFinish(self, unstacked_outputfiles=None, grep_pattern=None):
  def WaitOnAllJobsFinish(self, grep_pattern=None):
    '''The function will block our script until all related jobs (that can be found under the current user and matches the grep_pattern) have been finished.
    Args:
      grep_pattern: An optional pattern string that will be used to find all jobs under the given user and matches the grep_pattern.'''
    icnt = 0
    nsecs_backoff = 10 #30 #120
    start_time=None
    while True:
      # Exclude the error jobs (' C ') coz these jobs can remain in qstat info for quite some time.
      if not grep_pattern:
        cmd = "qstat -a | grep %s | grep -v \' C \' | wc -l " % (self._user_name) #Total number of jobs
        cmd_Q = "qstat -a | grep %s | grep -v \' C \' | grep \' Q \' | wc -l " % (self._user_name) #Total number of jobs queued
        cmd_kill = "qstat -a | grep %s | grep -v \' C \' | awk '{print $1}' | xarg qdel -W force " % (self._user_name)
        grep_pattern = ""
      else:
        # Due to the column width constrain from qstat display, only maximum of 15 chars in grep_pattern(job name basically) will be shown.
        cmd = "qstat -a | grep %s | grep -v \' C \' | grep %s | wc -l " % (self._user_name, grep_pattern[0:15]) #Total number of jobs
        cmd_Q = "qstat -a | grep %s | grep -v \' C \' | grep \' Q \' | grep %s | wc -l " % (self._user_name, grep_pattern[0:15]) #Total number of jobs queued
        cmd_kill = "qstat -a | grep %s | grep -v \' C \' | grep %s | awk '{print $1}' | xarg qdel -W force " % (self._user_name, grep_pattern[0:15])
      #Checking number of job running for specific grep_pattern if any
      stat1,out1=commands.getstatusoutput(cmd)
      statQ,outQ=commands.getstatusoutput(cmd_Q)
      if (stat1 != 0 and statQ != 0) or not (out1.isdigit() and outQ.isdigit()):  # abnormal case, qstat query is not successful
        print 'qstat query error, will back off for %s seconds; \n return msg = %s' % (nsecs_backoff,out1)
        time.sleep(nsecs_backoff)
        # print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      else:  # Normal case, query is successful
        njobs_tot=int(out1)
        njobs_que=int(outQ)

        if njobs_tot > 0:
          icnt += 1
          if icnt == 1:
            print "Wait On All Jobs to Finish (%s)..." % grep_pattern[0:15]
            # Start timer for first time
            start_time=timer()


#=============================================================================
#   THIS PART TRIES TO SUM TOGETHER FINISHED JOB RTM'S WHILE THE OTHERS
#   ARE STILL COMPUTING (INSTEAD OF JUST MINDLESSLY WATING AND CHECKING)

          # print("Checking if there are any finished jobs to stack!!")

          # # for subjobid, subjobfns in zip(subjobids,unstacked_outputfiles):  # For each job
          # for subjobfns in unstacked_outputfiles:  # For each job
          #   for fn in subjobfns:
          #     file_error = CheckSephFileError(fn,False)
          #     if file_error == 0:
          #       print "Awesome! File %s is done. Lets stack it!" % fn
          #       stackcmd="Add to_stdout=0 %s %s" % (stackedfile,fn)
          #       stat1,out1=commands.getstatusoutput(stackcmd)
          #       # Remove file from the list of unstacked outputfiles
          #       unstacked_outputfiles=unstacked_outputfiles.remove(subjobfns)
#=============================================================================



          #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          time.sleep(5) # This is very important! If you dont have a sleep here, then you end up barraging the head node 
          # with qstat requests and you end up screwing up the cluster for yourself and everyone else!!!!!
          #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          #Checking if maximum waiting time is reached. In case kill all related jobs and restart the bad ones outside
          #Timer restarts when a job is queued
          if(njobs_que > 0):
            start_time=timer()
          if(self.max_time!=None and start_time!=None):
            if(self.max_time<(timer()-start_time)):
              stat_kill,out_kill=commands.getstatusoutput(cmd_kill)
              print "Maximum waiting time of %s seconds reached! Restarting unfinished jobs!"%(max_time)
              #Restarting timer
              start_time=timer()
        else:
          break
    return
    # return unstacked_outputfiles


  def WaitOnAllJobsFinish_ORIG(self, grep_pattern = None):
    '''The function will block our script until all related jobs (that can be found under the current user and matches the grep_pattern) have been finished.
    Args:
      grep_pattern: An optional pattern string that will be used to find all jobs under the given user and matches the grep_pattern.'''
    icnt = 0
    nsecs_backoff = 10 #30 #120
    start_time=None
    while True:
      # Exclude the error jobs (' C ') coz these jobs can remain in qstat info for quite some time.
      if not grep_pattern:
        cmd = "qstat -a | grep %s | grep -v \' C \' | wc -l " % (self._user_name) #Total number of jobs
        cmd_Q = "qstat -a | grep %s | grep -v \' C \' | grep \' Q \' | wc -l " % (self._user_name) #Total number of jobs queued
        cmd_kill = "qstat -a | grep %s | grep -v \' C \' | awk '{print $1}' | xarg qdel -W force " % (self._user_name)
        grep_pattern = ""
      else:
        # Due to the column width constrain from qstat display, only maximum of 15 chars in grep_pattern(job name basically) will be shown.
        cmd = "qstat -a | grep %s | grep -v \' C \' | grep %s | wc -l " % (self._user_name, grep_pattern[0:15]) #Total number of jobs
        cmd_Q = "qstat -a | grep %s | grep -v \' C \' | grep \' Q \' | grep %s | wc -l " % (self._user_name, grep_pattern[0:15]) #Total number of jobs queued
        cmd_kill = "qstat -a | grep %s | grep -v \' C \' | grep %s | awk '{print $1}' | xarg qdel -W force " % (self._user_name, grep_pattern[0:15])
      #Checking number of job running for specific grep_pattern if any
      stat1,out1=commands.getstatusoutput(cmd)
      statQ,outQ=commands.getstatusoutput(cmd_Q)
      if (stat1 != 0 and statQ != 0) or not (out1.isdigit() and outQ.isdigit()):  # abnormal case, qstat query is not successful
        print 'qstat query error, will back off for %s seconds; \n return msg = %s' % (nsecs_backoff,out1)
        time.sleep(nsecs_backoff)
        # print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      else:  # Normal case, query is successful
        njobs_tot=int(out1)
        njobs_que=int(outQ)


        if njobs_tot > 0:
          icnt += 1
          if icnt == 1:
            print "Wait On All Jobs to Finish (%s)..." % grep_pattern[0:15]
            #Start timer for first time
            start_time=timer()

          print("checking if there are any finished josb to stack!!")
          #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          time.sleep(5) # This is very important! If you dont have a sleep here, then you end up barraging the head node 
          # with qstat requests and you end up screwing up the cluster for yourself and everyone else!!!!!
          #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          #Checking if maximum waiting time is reached. In case kill all related jobs and restart the bad ones outside
          #Timer restarts when a job is queued
          if(njobs_que > 0):
            start_time=timer()
          if(self.max_time!=None and start_time!=None):
            if(self.max_time<(timer()-start_time)):
              stat_kill,out_kill=commands.getstatusoutput(cmd_kill)
              print "Maximum waiting time of %s seconds reached! Restarting unfinished jobs!"%(max_time)
              #Restarting timer
              start_time=timer()
        else:
          break
    return




  def SubmitJob(self, fn_script):
    #Submit the job script to pbs system by looking at if there are enough running jobs
    num_queues = len(self._queues_info)
    i_queue = 0
    icnt = 0
    # Should have priority to submit to the queue that has no waiting jobs in the first round of scanning, therefore icnt=0 is treated with different logic.
    while True:
      queue_name, queue_cap = self._queues_info[i_queue]
      priority_Q_num = min(self._priroty_num_Q_jobs[i_queue],queue_cap)
      jobR = 0; jobQ = 0; jobC = 0

      #Running qstat until success for ntries
      ntries=30;
      itry=1
      cmd_template = "qstat -a | grep %s | grep \' %s \' | grep %s | wc -l "
      while True:
        # Check how many jobs are in this queue.
        cmd1 = cmd_template % (self._user_name, "R", queue_name)
        stat1,out1=commands.getstatusoutput(cmd1)
        cmd2 = cmd_template % (self._user_name, "Q", queue_name)
        stat2,out2=commands.getstatusoutput(cmd2)
        # Complex command to handle long job names
        cmd3 = "qstat -fu %s | grep -o 'Job_Name = %s-*\|job_state = C\|queue = %s' | grep -Pzo 'Job_Name = %s-\\njob_state = C\\nqueue = %s' | grep 'job_state = C' | wc -l" % (self._user_name, self.param_reader.prefix, queue_name, self.param_reader.prefix, queue_name)
        stat3,out3=commands.getstatusoutput(cmd3)
        if(stat1!=0 or not out1.isdigit()):
          sepbase.msg("qstat query failed, msg=%d,%s\nRetrying!" % (stat1,out1))
        elif(stat2!=0 or not out2.isdigit()):
          sepbase.msg("qstat query failed, msg=%d,%s\nRetrying!" % (stat2,out2))
        elif(stat3!=0 or not out3.isdigit()):
          sepbase.msg("qstat query failed, msg=%d,%s\nRetrying!" % (stat3,out3))
        elif(itry==ntries or stat1==0 and stat2==0 and stat3==0 and out1.isdigit() and out2.isdigit() and out3.isdigit()):
          #Queries successful exit loop
          break
        # os.system("sleep 3")  # Wait until retrying to query again
        os.system("sleep 30")  # Wait until retrying to query again
        itry+=1

      #Convert outputs to integer
      jobR = int(out1); jobQ = int(out2); jobC = int(out3)
      #jobC = 0  # Just ignore error jobs at this moment.
      if icnt == 0:
        print "jobs status in the queue <%s>, R/ Q:C, %d/ %d:%d" % (queue_name, jobR, jobQ, jobC)
      njob_pending = jobQ #+ jobC
      njob_total = njob_pending + jobR
      should_sumbit = False

      # YANGS WAY
      # if (njob_total < self._total_jobs_cap[i_queue] and
      #     (njob_pending<priority_Q_num or
      #      (njob_pending < queue_cap and icnt>=1))):
      #---------------------------------

      # TJS WAY
      if (icnt != 2):
        ttt='queue_name = %s njob_pending =  %d; queue_cap = %d' % (queue_name,njob_pending,queue_cap)
        print(ttt)

      if ( ((queue_name=='sep' and jobR<23) or (queue_name!='sep') ) and (njob_total < self._total_jobs_cap[i_queue] and (njob_pending<priority_Q_num or (njob_pending <= queue_cap))) ):
      #---------------------------------
        itry=1
        cmd1 = "qsub -q %s %s" % (queue_name, fn_script)
        while True:
          assert (itry<=ntries), "Error! Cannot submit job. Tried %s times."%(ntries)
          print line_no(), ("submitting job %s to Queue:%s" %
                          (fn_script, queue_name))
          stat1, out1 = commands.getstatusoutput(cmd1)
          if stat1 != 0:
            sepbase.msg("submit job failed, msg=%d,%s\nRetrying!" % (stat1,out1))
          elif(stat1 == 0):
            #Queries successful exit loop
            break
          # os.system("sleep 1")  # Wait until retrying to submit again
          # os.system("sleep 30")  # Wait until retrying to submit again
          itry+=1
        # os.system("sleep 1.5")  # Stagger the job start-off time a bit.
        break
      else:
        # if not last queue, then try next queue
        if i_queue != num_queues-1:
          i_queue += 1
        else:  # back-off for a while before retrying
          icnt += 1
          if icnt == 2:  # Do not print multiple times of the same waiting msg
            print "Waiting on the pbs queue..."
          # if icnt == 1:
            # os.system("sleep 5")
          else:
            os.system("sleep 10")  # Sleep a while (secs) before do the query again.
          # Then start polling the first queue again.
          i_queue = 0
    return




  # def WaitOnAllJobsFinishSSH(self):
  #   '''The function will block our script until all related jobs (that can be found under the current user and matches the grep_pattern) have been finished.'''
  #   #Variable to print only once
  #   print_info=True
  #   while True:
  #     if(len(self.processes)==0): break
  #     if(print_info):
  #       print "Waiting for all jobs to be finished ..."
  #       print_info=False
  #     #Verifying running processes
  #     self.check_processes()
  #     os.system("sleep 5")
  #   return

  # def SubmitJobSSH(self, fn_script):
  #   #Change permission of the file (make it an executable)
  #   assert(os.system("chmod 744 %s"%(fn_script))==0), "Error cannot change permission to %s"%(fn_script)
  #   assert(os.system("sed -i \'/PBS/d\' %s"%(fn_script))==0), "Error cannot change file %s"%(fn_script)
  #   assert(os.system("sed -i \'/HOSTNAME/d\' %s"%(fn_script))==0), "Error cannot change file %s"%(fn_script)
  #   #cmd to submit through ssh
  #   sub_ssh_cmd="ssh %s \'%s\'"
  #   #variable to check print only once
  #   print_info=True
  #   #check if nodelist is different than None
  #   assert(self.nodelist!=None), "nodelist variable must be different than None to run through ssh!!!"
  #   #loop for submission
  #   while True:
  #     if (len(self.nodelist)>0):
  #       #If internal node list contain name submit job
  #       cmd=sub_ssh_cmd%(self.nodelist[0],fn_script)
  #       #creating logfile name
  #       logfile=open(fn_script.replace(".sh",".log"),"w")
  #       p=subprocess.Popen(cmd,shell=True,stdout=logfile,stderr=subprocess.STDOUT,universal_newlines=True)
  #       node_used=self.nodelist[0];
  #       #Removing used nodes from node pool
  #       self.nodelist.remove(node_used)
  #       starting_time=timer()
  #       job_name=fn_script
  #       self.processes.append([p,job_name,node_used,starting_time,logfile])
  #       print "Submitting job %s to node %s; total number of job running: %s"%(job_name,node_used,len(self.processes))
  #       os.system("sleep 1")
  #       break
  #     else:
  #       if(print_info):
  #         print "All nodes busy. Waiting to submit job %s ..."%(fn_script)
  #         print_info=False
  #       #Otherwise wait and check running processes
  #       os.system("sleep 5")
  #       self.check_processes()
  #   return

  def check_processes(self):
    """Function to check if processes are running and if have exceeded maximum running time
       It also moves available nodes to self.nodelist"""
    for process in self.processes:
      p=process[0]
      if(p.poll()!=None):
        #If process has finished, free node and make node avaiable
        self.nodelist.append(process[2])
        self.processes.remove(process)
      else:
        #flush log files
        logfile=process[4]
        logfile.flush()
        #Check maximum time if present
        if (self.max_time!=None):
          starting_time=process[3]
          #Killing the process if maximum time is exceeded (bad node?)
          if(self.max_time<(timer()-starting_time)):
            print "Job %s exceeded maximum running time. Killing the process!"%(process[1])
            p.kill()
            self.processes.remove(process)
    return

# Second constructor for PbsSubmitter
def SubmitterFromParamReader(job_param_reader):
  param_reader = job_param_reader
  return PbsSubmitter(zip(param_reader.queues, param_reader.queues_cap), param_reader.total_jobs_cap, None, param_reader.priority_num_Q_jobs,param_reader)


def SplitFullFilePath(abs_file_path):
  '''Split a filename into path, name, extension (without .dot).'''
  fpath, fname = os.path.split(abs_file_path)
  fbasename, fext = os.path.splitext(fname)
  return (fpath,fbasename,fext)
