#!/usr/local/bin/python
import subprocess
import sys
import os
import sepbase
from pbs_util import JobParamReader
from subprocess import Popen, PIPE



class JobParamReader:
    '''Parse the common parameters'''
    def __init__(self, dict_args):
        self.dict_args = dict_args
        return



class RBF_LIN_INVERSION(object):
    def __init__(self, param_reader,rbfiter):
        #----------------------------------------------------------------
        self.dict_args = param_reader.dict_args
        self.pathtemprbf = self.dict_args['pathtemprbf']
        self.gensolve_F = "%s/forward_RBF_lin.txt" % (self.pathtemprbf)
        self.gensolve_A = "%s/adjoint_RBF_lin.txt" % (self.pathtemprbf)
        self.solverpath = self.dict_args['solverpath']
        self.T_BIN = self.dict_args['T_BIN']
        self.genpar = self.dict_args['genpar']
        self.rbfiter = rbfiter
        self.rbfcoord = self.dict_args['rbfcoord']
        self.rbftable = self.dict_args['rbftable']
        self.inversionname = self.dict_args['inversionname']
        return



    def RBF_LIN_solver(self, debug, phi_match, outputpath):
        # -------------------------------------------------------------------------------------
        # Write the LINEARIZED FORWARD .txt file
        text_file = open(self.gensolve_F, "w")
        cmdF = "RUN: %s/APPLY_RBF3d.x rbftable=%s rbfcoord=%s verbose=1 adjoint=0 par=%s < input.H > output.H" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.genpar)
        text_file.write("%s" % cmdF)
        text_file.close()

        # -------------------------------------------------------------------------------------
        # Write the LINEARIZED ADJOINT .txt file
        text_file = open(self.gensolve_A, "w")
        cmdA = "RUN: %s/APPLY_RBF3d.x rbftable=%s rbfcoord=%s verbose=1 adjoint=1 par=%s < input.H > output.H" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.genpar)
        text_file.write("%s" % cmdA)
        text_file.close()

        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdF)
            print('-----------------------------------------------------------------')
            print(cmdA)

        # Make a zero initial model
        cmd = "Window3d n2=1 < %s > %s/initmodelLIN.H; Solver_ops file1=%s/initmodelLIN.H op=zero;" % (self.rbfcoord, self.pathtemprbf,self.pathtemprbf)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making an initial model (all zeros)"
            print(cmd)
        subprocess.call(cmd, shell=True)
        sys.stdout.flush()

        # Run the generic solver with those files
        # if(not os.path.isfile(outputpath)):

        cmd1 = "%s/python_solver/generic_linear_prob.py  fwd_cmd_file=%s  adj_cmd_file=%s iteration_movies=obj,model,gradient,residual " % (
            self.solverpath, self.gensolve_F, self.gensolve_A)
        cmd2 = "debug=no data=%s init_model=%s/initmodelLIN.H niter=%s inv_model=%s suffix=%s dotprod=0 tolobjrel=0.0005" % (
            phi_match, self.pathtemprbf, self.rbfiter, outputpath, self.inversionname)
        cmd = "%s %s" % (cmd1, cmd2)
        if (debug):
            print(
                '-----------------------------------------------------------------')
            print " LAUNCHING GENERIC LINEAR PYTHON SOLVER FOR RBF FITTING TO A SURFACE"
            print(cmd)
            sys.stdout.flush()
        subprocess.call(cmd, shell=True)
            
        # else:
        #     print "------------------------------------------------------------"
        #     print "Inverted result already exists: Skipping RBF fitting inversion"
        sys.stdout.flush()

        return


#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = JobParamReader(eq_args_from_cmdline)
    dict_args = param_reader.dict_args
    phi_match = dict_args['phi_match']
    outputpath = dict_args['outputpath']
    rbfiter = dict_args['rbfiter']


    print("=================================================================\n")
    debug = True
    # INITIALIZING THE MAIN FUNCTION SPACE and RBF SOLVER
    mainRBF = RBF_LIN_INVERSION(param_reader,rbfiter)
    mainRBF.RBF_LIN_solver(debug, phi_match, outputpath)






