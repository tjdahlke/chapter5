from modeled_data3d import *
import subprocess
import os.path
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math


class SOURCE_INVERSION(object):
    def __init__(self, param_reader,prefix):
        #----------------------------------------------------------------
        self.dict_args = param_reader.dict_args
        self.vel_path = self.dict_args['vel_path']
        self.pathtempsou = self.dict_args['pathtemp']
        self.gensolve_F = "%s/forward_souPBS.txt" % (self.pathtempsou)
        self.gensolve_A = "%s/adjoint_souPBS.txt" % (self.pathtempsou)
        self.solverpath = self.dict_args['solverpath']
        self.T_BIN = self.dict_args['T_BIN']
        self.nfiles = int(self.dict_args['nfiles'])  # Same as number of shots
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.genpar = self.dict_args['genpar']
        self.pbs_template = self.dict_args['pbs_template']
        self.prefix = prefix
        self.nfiles_perjob = int(self.dict_args['nfiles_perjob'])
        self.souInv_iter = self.dict_args['souInv_iter']
        self.queues = self.dict_args['queues']
        self.queues_cap = self.dict_args['queues_cap']
        self.pypath = self.dict_args['pypath']
        self.singularity = self.dict_args['singularity']
        self.souhead = self.dict_args['souhead']
        self.recpos = self.dict_args['recposSI']
        self.abcs = self.dict_args['abcs']
        self.rand_path = self.dict_args['rand_path']
        self.pad = self.dict_args['pad']
        self.randpad = self.dict_args['pad']
        self.ntsi = self.dict_args['ntsi']
        return



    def modeling_op(self, debug, data):

        # Forward wave propagation
        text_fileF = open(self.gensolve_F, "w")
        args1f = "nt=%s pad=%s shot_divide=1 obs_prefix=' ' rand_path=%s pbs_template=%s genpar=%s vel_path=%s T_BIN=%s adjoint=0 model=input.H data=output.H writesrcwave=0 singularity='%s' inversion=1" % (
            self.ntsi,self.pad,self.rand_path,self.pbs_template, self.genpar, self.vel_path, self.T_BIN, self.singularity)
        args3f = "path_out=%s abcs=%s recpos=%s souhead=%s nfiles_perjob=%s  file_prefix='souInv' prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s queues=%s queues_cap=%s" % (
            self.path_out, self.abcs, self.recpos, data, self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg, self.queues, self.queues_cap)
        cmdF = "RUN: python %s/modeled_data3d.py jobtype='sourceinversion' %s %s >> source_modeling_forward.log" % (
            self.pypath, args1f, args3f)
        text_fileF.write("%s" % cmdF)
        text_fileF.close()

        # Adjoint source modeling
        text_fileA = open(self.gensolve_A, "w")
        args1a = "nt=%s pad=%s shot_divide=1 obs_prefix=' ' rand_path=%s pbs_template=%s genpar=%s vel_path=%s T_BIN=%s adjoint=1 model=output.H data=input.H writesrcwave=0 singularity='%s' inversion=1" % (
            self.ntsi,self.pad,self.rand_path,self.pbs_template, self.genpar, self.vel_path, self.T_BIN, self.singularity)
        args3a = "path_out=%s abcs=%s  recpos=%s souhead=%s nfiles_perjob=%s file_prefix='souInv' prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s queues=%s queues_cap=%s" % (
            self.path_out, self.abcs, self.recpos, data, self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg, self.queues, self.queues_cap)
        cmdA = "RUN: python %s/modeled_data3d.py jobtype='sourceinversion' %s %s >> source_modeling_adjoint.log" % (
            self.pypath, args1a, args3a)
        text_fileA.write("%s" % cmdA)
        text_fileA.close()

        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdF)
        return



    def souInvMAIN(self, param_reader, debug, model, data):
        #-------------------------------------------------------------------------------------
        # Write the FORWARD and ADJOINT .txt files for the generic solver
        self.modeling_op(debug,data)

        # Make input data
        cmd = "Cp %s %s/inputdata.H;" % (data, self.pathtempsou)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making input data"
            print(cmd)
        subprocess.call(cmd, shell=True)

        # Make a zero initial model
        initM="%s/initmodel.H" % (self.pathtempsou)
        cmd = "Window3d n2=1 synch=1 < %s > %s hff=%s@@ ; echo 'hff=-1 o2=0.0 d2=1.0' >> %s; Solver_ops file1=%s op=zero;" % (data,initM,initM,initM,initM)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making a zero initial model"
            print(cmd)
        subprocess.call(cmd, shell=True)

        if(not os.path.isfile(model)):
            # CG SOLVE
            cmd1 = "python %s/python_solver/generic_linear_prob.py  fwd_cmd_file=%s  adj_cmd_file=%s iteration_movies=obj,model" % (
                self.solverpath, self.gensolve_F, self.gensolve_A)
            # Running all the movies (gradient, residual) will sometimes fail for some reason depending on what version of Cat3d is being used I think
            cmd2 = "data=%s/inputdata.H init_model=%s  niter=%s  inv_model=%s suffix=%s debug=no dotprod=0" % (
                self.pathtempsou, initM, self.souInv_iter, model, self.prefix)
            cmd = "%s %s" % (cmd1, cmd2)  # tolobjrel=0.05
            if (debug):
                print(
                    '-----------------------------------------------------------------')
                print " LAUNCHING GENERIC PYTHON SOLVER FOR SOURCE INVERSION"
                print(cmd)
            subprocess.call(cmd, shell=True)
        else:
            print "------------------------------------------------------------"
            print "Inverted result already exists: Skipping Source Inversion"

        return


if __name__ == '__main__':
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)
    dict_args = param_reader.dict_args
    model = dict_args['model']
    data = dict_args['data']
    start_time = time.time()

    #------- RUN THE SOURCE INVERSION ----------------
    souInv=SOURCE_INVERSION(param_reader,'sourceInv')
    souInv.souInvMAIN(param_reader, True, model, data)

    elapsed_time = (time.time() - start_time)/60.0
    print("ELAPSED TIME FOR MODELED DATA (minutes) : ")
    print(elapsed_time)
